<?php

use Illuminate\Database\Seeder;
use App\SubField;

class SubFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_fields')->delete();

        SubField::create([
            'name' => 'Bangunan & Pembinaan',
            'status' => 'active',
            'field_id' => '2'
        ]);

        SubField::create([
            'name' => 'Teknologi Maklumat & Komunikasi',
            'status' => 'active',
            'field_id' => '7'
        ]);

        SubField::create([
            'name' => 'Pengurusan Bisnes',
            'status' => 'active',
            'field_id' => '5'
        ]);

    }
}
