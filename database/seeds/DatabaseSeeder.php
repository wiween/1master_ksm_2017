<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(LookupsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(StateTableSeeder::class);
        //$this->call(GalleryTableSeeder::class);
        $this->call(SliderTableSeeder::class);
       // $this->call(CoursesTableSeeder::class);
        $this->call(OrganizationSeeder::class);
        $this->call(FieldTableSeeder::class);
        $this->call(SubFieldTableSeeder::class);

    }
}
