<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        // Super Admin
        $superAdmin = new \App\User();
        $superAdmin->name = 'Mr. Super Admin';
        $superAdmin->email = 'super.admin@gmail.com';
        $superAdmin->password = bcrypt('haha1234');
        $superAdmin->ic_number = '80080808888';
        $superAdmin->phone_number = '0198899776';
        $superAdmin->role = 'super_admin';
        $superAdmin->access_power = 10000;
        $superAdmin->avatar = '/images/user/super_admin.png';
        $superAdmin->status = 'active';
        $superAdmin->save();

        // Admin (Dasar / Pantau) :: KSM
        $admin = new \App\User();
        $admin->name = 'Mr. Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('haha1234');
        $admin->ic_number = '800707077777';
        $admin->phone_number = '0198899777';
        $admin->role = 'admin';
        $admin->access_power = 5000;
        $admin->status = 'active';
        $admin->save();

        // Admin Kementerian
        $admin = new \App\User();
        $admin->name = 'Mr. Admin Ministry';
        $admin->email = 'admin.ministy@gmail.com';
        $admin->password = bcrypt('haha1234');
        $admin->ic_number = '800707076666';
        $admin->phone_number = '0198899666';
        $admin->role = 'admin_ministry';
        $admin->access_power = 3000;
        $admin->status = 'active';
        $admin->save();

        // Admin Department
        $admin = new \App\User();
        $admin->name = 'Mr. Admin Department';
        $admin->email = 'admin.department@gmail.com';
        $admin->password = bcrypt('haha1234');
        $admin->ic_number = '800707075555';
        $admin->phone_number = '0198899555';
        $admin->role = 'admin_department';
        $admin->access_power = 2000;
        $admin->status = 'active';
        $admin->save();

        // Admin Branch
        $admin = new \App\User();
        $admin->name = 'Mr. Admin Branch';
        $admin->email = 'admin.branch@gmail.com';
        $admin->password = bcrypt('haha1234');
        $admin->ic_number = '800707074444';
        $admin->phone_number = '0198899444';
        $admin->role = 'admin_branch';
        $admin->access_power = 1000;
        $admin->status = 'active';
        $admin->save();

        // Examiner
        $examiner = new \App\User();
        $examiner->name = 'Mr. Examiner';
        $examiner->email = 'examiner@gmail.com';
        $examiner->password = bcrypt('haha1234');
        $examiner->ic_number = '800707072222';
        $examiner->phone_number = '0198899222';
        $examiner->role = 'examiner';
        $examiner->access_power = 200;
        $examiner->status = 'active';
        $examiner->save();

        // user
        $user = new \App\User();
        $user->name = 'Mr. User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('haha1234');
        $user->ic_number = '800707071111';
        $user->phone_number = '0198899111';
        $user->role = 'user';
        $user->access_power = 100;
        $user->status = 'active';
        $user->save();

    }
}
