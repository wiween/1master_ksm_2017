<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('courses')->delete();
        
        \DB::table('courses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'PEMBUAT PAKAIAN LELAKI',
                'field' => 'Tekstil & Pakaian.',
                'introduction' => NULL,
                'job' => NULL,
                'salary' => NULL,
                'learning' => NULL,
                'contact' => NULL,
            'duration' => 'Jangka Panjang (6-15 bulan)',
                'audience1' => '7',
                'audience2' => '',
                'audience3' => '',
                'audience4' => '',
                'status' => '4',
                'avatar' => '/images/course/default.jpg',
                'hit' => 0,
                'visit' => 0,
                'student' => 0,
                'deleted_at' => NULL,
                'created_at' => '2017-08-23 15:32:11',
                'updated_at' => '2017-08-23 15:32:11',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'PEKERJA AM TANAMAN',
                'field' => 'Pertanian',
                'ministry' => '1',
                'venue' => '10',
                'state' => '12',
                'introduction' => NULL,
                'job' => NULL,
                'salary' => NULL,
                'learning' => NULL,
                'fee' => 'free',
                'contact' => NULL,
            'duration' => 'Jangka Panjang (6-15 bulan)',
                'audience1' => '7',
                'audience2' => '',
                'audience3' => '',
                'audience4' => '',
                'status' => '4',
                'avatar' => '/images/course/default.jpg',
                'hit' => 0,
                'visit' => 0,
                'student' => 0,
                'deleted_at' => NULL,
                'created_at' => '2017-08-23 15:34:33',
                'updated_at' => '2017-08-23 15:34:33',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'PEMBUAT PAKAIAN LELAKI',
                'field' => 'Tekstil & Pakaian.',
                'ministry' => '1',
                'venue' => '11',
                'state' => '12',
                'introduction' => 'Membuat dan membaiki sut atau pakaian',
                'job' => NULL,
                'salary' => NULL,
                'learning' => 'Kursus ini menyediakan pelajar tentang jenis, klasifikasi dan alatan jahitan serta fabrik dan tekstil, memberi pendedahan tentang aliran bagi pakaian yang berkualiti serta bersesuaian dengan tema dan kegunaan pasaran tempatan dan antarabangsa mengikut fesyen terkini.',
                'fee' => 'free',
                'contact' => NULL,
            'duration' => 'Jangka Panjang (6-15 bulan)',
                'audience1' => '7',
                'audience2' => '',
                'audience3' => '',
                'audience4' => '',
                'status' => '4',
                'avatar' => '/images/course/default.jpg',
                'hit' => 0,
                'visit' => 0,
                'student' => 0,
                'deleted_at' => NULL,
                'created_at' => '2017-08-23 15:36:33',
                'updated_at' => '2017-08-23 15:36:33',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'TUKANG LEPA',
                'field' => 'Kejuruteraan',
                'ministry' => '1',
                'venue' => '11',
                'state' => '12',
                'introduction' => 'Memasang dan menyelenggara papan lepa pada bahagian dalam dan luar struktur',
                'job' => NULL,
                'salary' => NULL,
                'learning' => 'Tukang plaster memasang, menyelenggara, dan membaik pulih papan lepa pada bangunan, dan meletakkan lapisan perhiasan dan perlindungan daripada plaster, simen, dan bahan yang seumpamanya pada bahagian dalam dan luar struktur.',
                'fee' => 'free',
                'contact' => NULL,
            'duration' => 'Jangka Panjang (6-15 bulan)',
                'audience1' => '7',
                'audience2' => '',
                'audience3' => '',
                'audience4' => '',
                'status' => '4',
                'avatar' => '/images/course/default.jpg',
                'hit' => 0,
                'visit' => 0,
                'student' => 0,
                'deleted_at' => NULL,
                'created_at' => '2017-08-23 15:39:12',
                'updated_at' => '2017-08-23 15:39:12',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'TUKANG BATU BATA',
                'field' => 'Senibina',
                'ministry' => '1',
                'venue' => '11',
                'state' => '12',
                'introduction' => 'Memasang dan menyelenggara papan lepa pada bahagian dalam dan luar struktur',
                'job' => NULL,
                'salary' => NULL,
                'learning' => 'Tukang plaster memasang, menyelenggara, dan membaik pulih papan lepa pada bangunan, dan meletakkan lapisan perhiasan dan perlindungan daripada plaster, simen, dan bahan yang seumpamanya pada bahagian dalam dan luar struktur.',
                'fee' => 'free',
                'contact' => NULL,
            'duration' => 'Jangka Panjang (6-15 bulan)',
                'audience1' => '7',
                'audience2' => '',
                'audience3' => '',
                'audience4' => '',
                'status' => '4',
                'avatar' => '/images/course/default.jpg',
                'hit' => 1,
                'visit' => 0,
                'student' => 0,
                'deleted_at' => NULL,
                'created_at' => '2017-08-23 15:41:59',
                'updated_at' => '2017-08-23 15:47:40',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Electrical Maintenance',
                'field' => 'Teknologi Penyenggaraan Mekanikal',
                'ministry' => '1',
            'venue' => '12',
                'state' => '9',
                'introduction' => NULL,
                'job' => NULL,
                'salary' => NULL,
            'learning' => 'i) Identify circuit diagram
ii) Go through common circuit diagram function command
iii) Run through electrical system according to specified diagram                                                                                                                                                iv) Identify safety requirements on precheck procedures                                                                                                           
v) Check contact breaker                                                                                                                          
vi) Identify types of common failure                                                                                                                            
vii) Report, record and check activity"',
'fee' => '230.00',
'contact' => 'NAMA : Mahizan Bin Shaari
NO TEL : 04-9777400 EXT:147
EMEL : mahizans@yahoo.com"',
'duration' => '16 Jam',
'audience1' => '7',
'audience2' => '8',
'audience3' => '',
'audience4' => '',
'status' => '4',
'avatar' => '/images/course/default.jpg',
'hit' => 0,
'visit' => 0,
'student' => 0,
'deleted_at' => NULL,
'created_at' => '2017-08-23 15:46:36',
'updated_at' => '2017-08-23 15:46:36',
),
));
        
        
    }
}