<?php

use Illuminate\Database\Seeder;
use App\Field;

class FieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fields')->delete();

        Field::create([
            'name' => 'Admin',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Engineering',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Finance & Accounting',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Legal',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Marketing',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Sales',
            'status' => 'active'
        ]);

        Field::create([
            'name' => 'Computing & IT',
            'status' => 'active'
        ]);

    }
}
