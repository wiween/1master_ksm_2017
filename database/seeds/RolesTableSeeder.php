<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'name'      => 'user',
            'nick'      => 'Standard User',
            'access_power' => 100,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'examiner',
            'nick'      => 'Examiner',
            'access_power' => 200,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'admin_branch',
            'nick'      => 'Admin Branch',
            'access_power' => 1000,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'admin_department',
            'nick'      => 'Admin Department',
            'access_power' => 2000,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'admin_ministry',
            'nick'      => 'Admin Ministry',
            'access_power' => 3000,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'admin',
            'nick'      => 'Administrator',
            'access_power' => 5000,
            'status'    => 'active',
            'description' => ''
        ]);

        Role::create([
            'name'      => 'super_admin',
            'nick'      => 'Super Admin (BTM)',
            'access_power' => 10000,
            'status'    => 'active',
            'description' => ''
        ]);
    }
}
