<?php

use Illuminate\Database\Seeder;
use App\State;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        State::create([
            'state_id' => '01',
            'name' => 'Johor',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '02',
            'name' => 'Kedah',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '03',
            'name' => 'Kelantan',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '04',
            'name' => 'Melaka',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '05',
            'name' => 'Negeri Sembilan',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '06',
            'name' => 'Pahang',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '07',
            'name' => 'Pulau Pinang',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '08',
            'name' => 'Perak',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '09',
            'name' => 'Perlis',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '10',
            'name' => 'Selangor',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '11',
            'name' => 'Terengganu',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '12',
            'name' => 'Sabah',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '13',
            'name' => 'Sarawak',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '14',
            'name' => 'WP Kuala Lumpur',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '15',
            'name' => 'WP Labuan',
            'status' => 'active'
        ]);

        State::create([
            'state_id' => '16',
            'name' => 'WP Putrajaya',
            'status' => 'active'
        ]);
    }
}
