<?php

use Illuminate\Database\Seeder;
use App\Gallery;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galleries')->delete();

        Gallery::create([
            'image'  => '/images/gallery/default.jpg',
            'remark'  => 'testing',
            'status'  => 'active',
            'organization_id'  => '1'
        ]);
    }
}
