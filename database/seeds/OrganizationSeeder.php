<?php

use Illuminate\Database\Seeder;
use App\Organization;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('organizations')->delete();

        Organization::create([
            'name'      => 'Ministry of Human Resources',
            'initial'      => 'MOHR',
            'type'    => 'ministry',
            'parent_id' => '',
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'Ministry of Youth And Sport',
            'initial'      => 'MOYS',
            'type'    => 'ministry',
            'parent_id' => '',
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'MInistry of Higher Education',
            'initial'      => 'MOHE',
            'type'    => 'ministry',
            'parent_id' => '',
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'MInistry of Higher Education',
            'initial'      => 'MOHE',
            'type'    => 'ministry',
            'parent_id' => '',
            'status' => 'active'
        ]);

        $mohr = Organization::where('type', 'ministry')->where('name', 'Ministry of Human Resources')->first();
        $moys = Organization::where('type', 'ministry')->where('name', 'Ministry of Youth And Sport')->first();

        Organization::create([
            'name'      => 'Manpower Department',
            'initial'      => 'JTM',
            'type'    => 'department',
            'parent_id' => $mohr->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'JPK',
            'initial'      => 'JPK',
            'type'    => 'department',
            'parent_id' => $mohr->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'IKBN',
            'initial'      => 'IKBN',
            'type'    => 'department',
            'parent_id' => $moys->id,
            'status' => 'active'
        ]);

        $jtm = Organization::where('type', 'department')->where('name', 'Manpower Department')->first();
        $ikbn = Organization::where('type', 'department')->where('name', 'IKBN')->first();

        Organization::create([
            'name'      => 'ILPKL',
            'initial'      => 'ILPKL',
            'type'    => 'agency',
            'parent_id' => $jtm->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'ILPKT',
            'initial'      => 'ILPKT',
            'type'    => 'agency',
            'parent_id' => $jtm->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'PUSAT LATIHAN KEMAHIRAN KOTA KINABALU',
            'initial'      => 'PLKKK',
            'type'    => 'agency',
            'parent_id' => $jtm->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'PUSAT LATIHAN KEMAHIRAN KENINGAU',
            'initial'      => 'PLKKENINGAU',
            'type'    => 'agency',
            'parent_id' => $jtm->id,
            'status' => 'active'
        ]);

        Organization::create([
            'name'      => 'Institut Latihan Perindustrian (ILP) Kangar',
            'initial'      => 'ILPKANGAR',
            'type'    => 'agency',
            'parent_id' => $jtm->id,
            'status' => 'active'
        ]);
    }
}
