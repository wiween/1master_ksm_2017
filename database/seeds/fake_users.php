<?php

use Illuminate\Database\Seeder;

class fake_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $roles = ['user', 'admin', 'super_admin', 'admin_minister', 'admin_department', 'examiner'];

        for ($i=0; $i<1000; $i++) {
            $user = new \App\User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->password = bcrypt('haha1234');
            $user->ic_number = $faker->numerify('############');
            $user->phone_number = $faker->phoneNumber;
            $user->role = $roles[rand(0,5)];
            $user->access_power = 100;
            $user->status = 'active';
            $user->save();
        }
    }
}
