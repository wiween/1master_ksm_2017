<?php

use Illuminate\Database\Seeder;
use App\Slider;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sliders')->delete();

        Slider::create([
            'sliderphoto'  => '/images/frontend/home/slider/banner_adtec1.png',
            'title'  => 'Where The Learning Begin',
            'subtitle'  => ' Knowledgeable and Trained Certified Skill Personal.',
            'status'  => 'active'
        ]);

          Slider::create([
            'sliderphoto'  => '/images/frontend/home/slider/banner_adtec2.png',
            'title'  => 'Create Talent',
            'subtitle'  => ' 1Malaysia Skill Training Enhancement for the Rakyat (1Master)',
            'status'  => 'active'
          ]);

        Slider::create([
            'sliderphoto'  => '/images/frontend/home/slider/banner_adtec3.png',
            'title'  => 'High Demand in Industry',
            'subtitle'  => 'Become skill professional to meet the ever growing demand by various industry.',
            'status'  => 'active'
        ]);

        Slider::create([
            'sliderphoto'  => '/images/frontend/home/slider/banner_adtec4.png',
            'title'  => 'Pragmatic Knowledge',
            'subtitle'  => 'Empower Yourself to the Next Level with Highly Sought Skills in the Market',
            'status'  => 'active'
        ]);
    }
}
