<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropNOSSSectorsFramesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('n_o_s_s_sectors');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('n_o_s_s_sectors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('noss_id');
            $table->string('name');
            $table->string('status')->default('active');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }
}
