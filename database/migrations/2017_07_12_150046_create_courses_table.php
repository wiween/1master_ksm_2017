<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            //$table->string('field')->default('-');
            $table->text('introduction')->nullable();
            $table->string('job')->nullable();
            $table->string('salary')->nullable();
            $table->text('syllabus')->nullable();
            $table->text('contact')->nullable();
            $table->text('duration')->nullable();
            $table->string('audience1')->default('-');
            $table->string('audience2')->default('-')->nullable();
            $table->string('audience3')->default('-')->nullable();
            $table->string('audience4')->default('-')->nullable();
            //$table->string('status')->default('active');
            $table->integer('status')->unsigned()->index();
            $table->foreign('status')->references('id')->on('lookups')->onDelete('restrict');
            $table->string('image')->default('/images/course/default.jpg');
            $table->integer('hit')->default(0);
            $table->integer('organization_id')->unsigned()->index();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('restrict');
            $table->integer('sub_field_id')->unsigned()->index()->default(1);
            $table->foreign('sub_field_id')->references('id')->on('sub_fields')->onDelete('restrict');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
