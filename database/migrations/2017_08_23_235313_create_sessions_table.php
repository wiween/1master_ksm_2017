<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year');
            $table->string('month');
            $table->string('state');
            $table->string('venue');
            $table->float('fee');
            $table->integer('man_age1518')->default(0);
            $table->integer('man_age1925')->default(0);
            $table->integer('man_age2640')->default(0);
            $table->integer('man_age41')->default(0);
            $table->integer('woman_age1518')->default(0);
            $table->integer('woman_age1925')->default(0);
            $table->integer('woman_age2640')->default(0);
            $table->integer('woman_age41')->default(0);
            $table->integer('students')->default(0);
            $table->integer('graduates')->default(0);
            $table->integer('school_leavers')->default(0);
            $table->integer('existing_employee')->default(0);
            $table->integer('malay')->default(0);
            $table->integer('chinese')->default(0);
            $table->integer('indian')->default(0);
            $table->integer('dayak')->default(0);
            $table->integer('others')->default(0);
            $table->integer('sme')->default(0);
            $table->integer('big_company')->default(0);
            $table->integer('total_participant')->default(0);
            $table->date('visited_at')->nullable();
            $table->string('visited_by')->nullable();
            $table->integer('course_id')->unsigned()->index();
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('restrict');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
