<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Testimonial
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Testimonial whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Testimonial whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Testimonial whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Testimonial extends Model
{
    //
}
