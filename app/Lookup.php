<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Lookup
 *
 * @property int $id
 * @property string $name
 * @property string $key
 * @property string $value
 * @property string $description
 * @property string $status
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereValue($value)
 * @mixin \Eloquent
 * @property string $remember_token
 * @method static \Illuminate\Database\Query\Builder|\App\Lookup whereRememberToken($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $course
 */
class Lookup extends Model
{
    public function course() {
        return $this->hasMany(Course::class); //cara php 7
    }

    public static function getValue($id)
    {
        $lookup = Lookup::find($id);
        $value = $lookup->value;

        return $value;
    }
}
