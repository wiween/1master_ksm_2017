<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Slider
 *
 * @property int $id
 * @property string $sliderphoto
 * @property string $title
 * @property string $subtitle
 * @property string $status
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereSliderphoto($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereSubtitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slider extends Model
{
    //
    use SoftDeletes;
}
