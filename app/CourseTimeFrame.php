<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CourseTimeFrame
 *
 * @property int $id
 * @property string $name
 * @property string $nick
 * @property string $description
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseTimeFrame whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CourseTimeFrame extends Model
{
    //
}
