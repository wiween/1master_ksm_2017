<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Gallery
 *
 * @property int $id
 * @property string $avatar
 * @property string $remark
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $organization_id
 * @property string $image
 * @property string $agency
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereAgency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery whereOrganizationId($value)
 * @property-read \App\Organization $organization
 */
class Gallery extends Model
{
    //
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
