<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Faq
 *
 * @property int $id
 * @property string $first_name
 * @property string $email
 * @property string $message
 * @property string $answer
 * @property string $category
 * @property string $status
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq extends Model
{
    public $table = "faqs";
    protected $dates = ['date_start','date_end'];
    use SoftDeletes;
}
