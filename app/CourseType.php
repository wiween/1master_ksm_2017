<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CourseType
 *
 * @property int $id
 * @property string $name
 * @property string $nick
 * @property string $description
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CourseType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CourseType extends Model
{
    //
}
