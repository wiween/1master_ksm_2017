<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Organization
 *
 * @property int $id
 * @property string $name
 * @property string $initial
 * @property string $type
 * @property string $parent_id
 * @property string $status
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereInitial($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @property-read \App\Target $target
 * @property-read \App\Visit $visit
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gallery[] $gallery
 */
class Organization extends Model
{
    use SoftDeletes;

    // each audit trails record, belong to ONE user only
    public function course() {
        return $this->hasMany(Course::class); //cara php 7
    }

    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public static function getValue($id)
    {
        $agency = Organization::find($id);
        $value = $agency->parent_id;

        return $value;
    }

    public static function getValue1($id)
    {
        $organization = Organization::find($id);
        $value = $organization->name;

        return $value;
    }

    // each audit trails record, belong to ONE user only
    public function visit() {
        return $this->belongsTo('App\Visit');
    }

    public function target() {
        return $this->hasMany('App\Target');
    }

    public static function getDepartment($agency_id)
    {
        $agency = Organization::findOrFail($agency_id);
        $departmentId = $agency->parent_id;
        $department = Organization::findOrFail($departmentId);

        return $department;
    }

    public static function getMinistry($departmentId)
    {
        //$agency = Organization::findOrFail($agency_id);
       // $departmentId = $agency->parent_id;
        //$department = Organization::findOrFail($departmentId);
        $department = Organization::findOrFail($departmentId);
        $ministry_id = $department->parent_id;
        $ministry = Organization::findOrFail($ministry_id);

        return $ministry;
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }
}
