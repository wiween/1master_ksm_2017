<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\NOSSSector
 *
 * @property int $id
 * @property string $noss_id
 * @property string $name
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereNossId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\NOSSSector whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NOSSSector extends Model
{
    //
}
