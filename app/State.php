<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\State
 *
 * @property int $id
 * @property string $state_id
 * @property string $name
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\State whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class State extends Model
{
    public static function getValue($id)
    {
        $state = State::find($id);
        $value = $state->name;

        return $value;
    }
}
