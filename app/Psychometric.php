<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Psychometric
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $field
 * @property string $category
 * @property string $score
 * @property string $status
 * @property int $user_id
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereField($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereUserId($value)
 * @property-read \App\User $user
 * @property int $uploaded_by
 * @method static \Illuminate\Database\Query\Builder|\App\Psychometric whereUploadedBy($value)
 */
class Psychometric extends Model
{
    protected $dates = ['enrolled_at'];


    public function user() {
        return $this->belongsTo('App\User');
    }
}
