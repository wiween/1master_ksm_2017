<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\MonthlyReport
 *
 * @property int $id
 * @property string $course_name
 * @property string $year
 * @property string $month
 * @property string $state
 * @property string $sector
 * @property string $fee
 * @property \App\Agency $agency
 * @property \App\Department $department
 * @property int $course_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Course $course
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereAgency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereCourseName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereDepartment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereFee($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereSector($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereYear($value)
 * @mixin \Eloquent
 * @property int $totalparticipant
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereTotalparticipant($value)
 * @property int $man_age1518
 * @property int $man_age1925
 * @property int $man_age2640
 * @property int $man_age41
 * @property int $woman_age1518
 * @property int $woman_age1925
 * @property int $woman_age2640
 * @property int $woman_age41
 * @property int $students
 * @property int $graduates
 * @property int $school_leavers
 * @property int $existing_employee
 * @property int $malay
 * @property int $chinese
 * @property int $indian
 * @property int $dayak
 * @property int $others
 * @property int $sme
 * @property int $big_company
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereBigCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereChinese($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereDayak($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereExistingEmployee($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereGraduates($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereIndian($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereMalay($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereManAge1518($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereManAge1925($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereManAge2640($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereManAge41($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereOthers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereSchoolLeavers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereSme($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereStudents($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereWomanAge1518($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereWomanAge1925($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereWomanAge2640($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereWomanAge41($value)
 * @property string $venue
 * @property int $total_participant
 * @property string $visited_at
 * @property string $visited_by
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereTotalParticipant($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereVenue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereVisitedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Session whereVisitedBy($value)
 */
class Session extends Model
{
    //
    use SoftDeletes;

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public static function getValue($id)
    {
        $course = Session::find($id);
        $value = $course->course_id;

        return $value;
    }

    public static function getValue1($id)
    {
        $venue = Session::find($id);
        $value = $venue->venue;

        return $value;
    }

    public static function getValue2($id)
    {
        $state = Session::find($id);
        $value = $state->state;

        return $value;
    }
}
