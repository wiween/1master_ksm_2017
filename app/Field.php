<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Field
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Field extends Model
{
    public static function getValue($id)
    {
        $field = Field::find($id);
        $value = $field->name;

        return $value;
    }
}
