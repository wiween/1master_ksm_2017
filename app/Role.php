<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string $nick
 * @property int $access_power
 * @property string $status
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereAccessPower($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $deleted_at
 * @property string $remember_token
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereRememberToken($value)
 */
class Role extends Model
{
    //
}
