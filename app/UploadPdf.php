<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\UploadPdf
 *
 * @property int $id
 * @property string $filepdf
 * @property string $uploaded_by
 * @property string $remark
 * @property int $user_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereFilepdf($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereUploadedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UploadPdf whereUserId($value)
 * @mixin \Eloquent
 */
class UploadPdf extends Model
{
    //
    use SoftDeletes;

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
