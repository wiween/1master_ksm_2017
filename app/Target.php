<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Target
 *
 * @property int $id
 * @property string $year
 * @property int $visit
 * @property int $participant
 * @property \App\Department $department
 * @property \App\Agency $agency
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereAgency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereDepartment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereParticipant($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereVisit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereYear($value)
 * @mixin \Eloquent
 * @property int $agency_id
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereAgencyId($value)
 * @property int $organization_id
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Target whereOrganizationId($value)
 */
class Target extends Model
{
    //
    public $table = "targets";
    use SoftDeletes;

    public function organization() {
        return $this->belongsTo(Organization::class);
    }


}
