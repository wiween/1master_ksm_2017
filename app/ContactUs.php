<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\ContactUs
 *
 * @property int $id
 * @property string $first_name
 * @property string $email
 * @property int $phone
 * @property string $subject
 * @property string $message
 * @property string $token_number
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereTokenNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactUs whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactUs extends Model
{
    //
    public $table = "contact_uses";
    use SoftDeletes;
}
