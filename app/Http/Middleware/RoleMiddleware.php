<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $routeRole)
    {
        if (Auth::check()) {
            $userRole = Auth::user()->role;

            if ($routeRole == 'user') {
                if ($userRole == 'user' || $userRole == 'examiner' || $userRole == 'admin_branch' || $userRole == 'admin_department'|| $userRole == 'admin_ministry'|| $userRole == 'admin'|| $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'examiner') {
                if ($userRole == 'examiner' || $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'admin_branch') {
                if ($userRole == 'admin_branch' || $userRole == 'admin_department'|| $userRole == 'admin_ministry'|| $userRole == 'admin'|| $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'admin_department') {
                if ($userRole == 'admin_department'|| $userRole == 'admin_ministry'|| $userRole == 'admin'|| $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'admin_ministry') {
                if ($userRole == 'admin_ministry'|| $userRole == 'admin'|| $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'admin') {
                if ($userRole == 'admin'|| $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'super_admin') {
                if ($userRole == 'super_admin') {
                    return $next($request);
                }
            }

            abort(403);
//            dd('If you see this page, check your role middleware.');


        } else {
            return redirect('login');
        }

    }
}
