<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PsychometricRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ic_number' => 'required',
            'score' => 'required',
            'category' => 'required',
            'field' => 'required',
            //
        ];
    }
}
