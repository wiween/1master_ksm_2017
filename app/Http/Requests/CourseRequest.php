<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'sub_field' => 'required',
            'agency' => 'required',
            //'introduction' => 'required',
            //'job' => 'required',
            //'salary' => 'required',
            //'learning' => 'required',
            //'contact' => 'required',
            'duration' => 'required',
            'audience1' => 'required',
            //'audience2' => 'required',
            //'audience3' => 'required',
            //'audience4' => 'required',
            'status' => 'required',
            'image' => 'file|mimes:jpeg,jpg,png',
        ];
    }
}
