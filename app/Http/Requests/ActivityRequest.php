<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time' => 'required',
            'venue' => 'required',
            'audience' => 'required',
            'ministry' => 'required',
            'officiated_by' => 'required',
            'description' => 'required',
            'status' => 'required',
            'image' => 'required|file|mimes:jpeg,jpg,png'
        ];
    }
}
