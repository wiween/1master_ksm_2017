<?php

namespace App\Http\Controllers\Frontend;

use App\Faq;
use App\Http\Requests\FaqRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;


class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $faqses = Faq::where('status', 'publish')->get();;
        return view('frontend.faq.index', compact('faqses'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request)
    {
        //
        //echo "masuk";
        $faqses = new Faq();
        $faqses->first_name = $request->input('first_name');
        $faqses->email = $request->input('email');
        $faqses->message = $request->input('message');

        // data to be sent to user via email to admin
        $data['first_name']  = $faqses->first_name;
        $data['email']  = $faqses->email;
        $data['message']  = $faqses->message;

        if ($faqses->save()) {
            return redirect('profile/faq')->with('successMessage', 'Message has been successfully sent');
        } else {
            return back()->with('errorMessage', 'Unable to sent message. Contact admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        //
    }
}
