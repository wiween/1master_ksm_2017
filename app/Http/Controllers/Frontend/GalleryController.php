<?php

namespace App\Http\Controllers\Frontend;

use App\Agency;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::where('status','active')->orderBy('id', 'desc')->paginate(12);
        $agencyIds = Gallery::distinct()->get(['organization_id']);
        $agencies = Organization::whereIn('id', $agencyIds)->get();  // select * from organizations where ID in [8,9]
//        $galleries = Gallery::orderBy('id', 'desc')->get();   // select * from contactus
        return view('frontend.gallery.index', compact('galleries', 'agencies'));
    }

    public function filter(Request $request)
    {
        $agency_id = $request->input('agency');
        $galleries = Gallery::where('organization_id', $agency_id)->paginate(12);
        $agencyIds = Gallery::distinct()->get(['organization_id']);
        $agencies = Organization::whereIn('id', $agencyIds)->get();  // select * from organizations where ID in [8,9]

        return view('frontend.gallery.index', compact('galleries', 'agencies'));
    }
}