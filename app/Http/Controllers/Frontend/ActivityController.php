<?php

namespace App\Http\Controllers\Frontend;

use App\Lookup;
use App\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Activity;
use App\Audience;
Use App\Organization;

class ActivityController extends Controller
{
    public function index()
    {
        $activities = Activity::where('status','publish')->paginate(3);
        //$ministry = Ministry::where('status','active')->get();
        $audiences = Lookup::where('status','active')->get();
        return view('frontend.activity.index',compact('activities','audiences','user'));
    }

    public function search(Request $request)
    {
        $audiences = Lookup::where('status','active')->get();
        $query = $request->input('query');
        $activities = Activity::where('name','like','%'.$query.'%')->paginate(3);
        $request->flash();

        return view('frontend.activity.index',compact('audiences','activities'));
    }

    public function show($id)
    {
        //Select * from users where id = 1 Limit 1
        $activity = Activity::findOrFail($id);
        $activity->hit = $activity->hit +1;
        $activity->save();
        return view('frontend.activity.index_more', compact('activity'));
    }

//    public function indexFilter(Request $request)
//    {
//        $audience = $request->get('audience');
//        $audiences = Course::where('status','active')->where('timeframe','short')->get();
//        $countdowns = Course::where('status','active')->where('timeframe','short')->orderBy('date_start','desc')->latest()->get();
//
//        if ($courses = Course::where('status','active')->where('timeframe','short')->where('noss_sector',$noss_sector)->orWhere('audience',$audience)->get()) {
//            return view('frontend.course.short', compact('courses', 'noss_sectors', 'audiences','countdowns'));
//        }
//        else {
//            $courses = Course::where('status','active')->where('timeframe','short')->get();
//            return view('frontend.course.short', compact('courses', 'noss_sectors', 'audiences','countdowns'));
//        }

    //  }

}
