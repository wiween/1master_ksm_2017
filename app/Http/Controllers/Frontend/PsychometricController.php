<?php

namespace App\Http\Controllers\Frontend;

use App\Psychometric;
use App\UploadPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use App\User;
use App\Lookup;
use App\Role;
use App\Http\Requests\UserChangePasswordRequest;
use App\Http\Requests\PsychometricProfileEditRequest;


class PsychometricController extends Controller
{
    public function register()
    {
        return view('frontend.psychometric.register');
    }

    public function profile()
    {
        $user = Auth::user();           // current user yang tengah login punya record
        $lastChar = substr($user->ic_number, -1);

        if ($user->avatar == '/images/user/default.png') {
            if ($lastChar % 2 > 0) {
                $avatar = '/images/user/man.png';
            } else {
                $avatar = '/images/user/woman.png';
            }
        } else {
            $avatar = $user->avatar;
        }

        $isEnroll = $user->enrolled_at;

        if ($isEnroll == null) {
            $enrollstatus = 'never';
        } else {
            //tgk pdf dulu
            $resultpdf = UploadPdf::where('user_id', $user->id)->count();

            if ($resultpdf > 0) {
                 //yg excel
                $result = Psychometric::where('user_id', $user->id)->where('status', 'active')->count();

                if ($result > 0) {
                    $enrollstatus = 'has_result_excel';
                    $psychometrics = Psychometric::where('user_id', $user->id)->where('status', 'active')->get();
                    $highestScore = Psychometric::where('user_id', $user->id)->where('status', 'active')->max('score');
                } else {
                    $enrollstatus = 'has_result';

                }
            } else {
                $enrollstatus = 'enrolling';
            }

        }

        $uploadpdf = UploadPdf::where('user_id', $user->id)->first();
        return view('frontend.psychometric.profile', compact('user', 'avatar',
            'enrollstatus', 'psychometrics', 'highestScore', 'uploadpdf'));
    }

    public function enroll()
    {
        $user = Auth::user();
        // data to be sent to user via email
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['phone'] = $user->phone;
        $data['ic_number'] = $user->ic_number;

        //store
//        $psychometric = new Psychometric();
//        $psychometric->user_id = $user->id;
//        $psychometric->enrolled_at = Carbon::now();
        $user->enrolled_at = Carbon::now();


        //sendmail
        Mail::send('emails.psychometric_test', $data, function ($message) use ($data) {
            $message->from('admin.1master@mohr.gov.my', "Admin 1Master");
            $message->subject("Assestment Tool For Hay Group From KSM 1Master");
            $message->to('adminhrdf@getnada.com', "Admin HRDF");
            // $message->to($data['email']);
        });

        if ($user->save()) {
            return redirect('psychometric/profile')->with('successMessage', 'Your application has been successfully
            submitted to HRDF. They will email you the psychometric test link.');
        } else {
            return back()->with('errorMessage', 'Unable to send enroll. Contact +603 8888 5000');
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        $roles = Role::where('status', 'active')->get();
        return view('backend.user.edit', compact('user', 'statuses', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
//        $user->password = $request->input('password');
        $user->ic_number = $request->input('ic_number');
        $user->phone_number = $request->input('phone_number');
        $user->role = $request->input('role');
        $user->access_power = 100;
        $user->remark = $request->input('remark');
        $user->status = $request->input('status');
        // Untuk upload gambar avatar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/user/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath . $fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        if ($user->save()) {
            return redirect('/psychometric/profile' . $id)->with('successMessage', 'User has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new user into database. Contact admin');
        }
    }

    public function changePassword()
    {
        return view('frontend.psychometric.change_password');
    }

    public function updatePassword(UserChangePasswordRequest $request)
    {
        $user = Auth::user();
        $oldPassword = $request->input('old_password');
        $password = $request->input('password');

        // check old password sama tak dengan yang dlm db
        $genuine = Hash::check($oldPassword, $user->password);
        // kalau sama, dia akan return true

        if ($genuine == false) {
            return back()->withInput()->with('errorMessage', 'Miss match old password');
        } else {
            // store new password into database
            $user->password = bcrypt($password);

            if ($user->save()) {
                return back()->with('successMessage', 'Password has been changed successfully.');
            } else {
                return back()->withInput()->with('errorMessage', 'Unable to change password.');
            }
        }


    }

    public function editProfile()
    {
//        $user = User::findOrFail($id);
        $user = Auth::user();
        return view('Frontend.psychometric.edit-profile', compact('user', 'statuses', 'roles'));
    }

    public function updateProfile(PsychometricProfileEditRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->ic_number = $request->input('ic_number');
        $user->phone_number = $request->input('phone_number');

        // Untuk upload gambar avatar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/user/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath . $fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        if ($user->save()) {
            return redirect('/psychometric/profile')->with('successMessage', 'Your profile has been updated.');
        } else {
            return back()->with('errorMessage', 'Unable to update your profile. Contact admin');
        }
    }


}
