<?php

namespace App\Http\Controllers\Frontend;

use App\ContactUs;
use App\Http\Requests\ContactUsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;



class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.contact_us.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUsRequest $request)
    {
        //
        //echo "masuk";

        $contactus = new ContactUs();
        $contactus->first_name = $request->input('first_name');
        $contactus->email = $request->input('email');
        $contactus->phone = $request->input('phone');
        $contactus->subject = $request->input('subject');
        $contactus->message = $request->input('message');

        // generate 8 random character
        $randomToken = str_random(8);
        $contactus->token_number = $randomToken;

        // data to be sent to user via email to admin
        $data['token_number']  = $contactus->token_number;
        $data['first_name']  = $contactus->first_name;
        $data['email']  = $contactus->email;
        $data['phone']  = $contactus->phone;
        $data['subject']  = $contactus->subject;
        $data['message']  = $contactus->message;

        /*Mail::send('emails.contact_us', $data, function($message) use ($data)
        {
            $message->from($data['email']);
            $message->subject("Contact Us - 1Master");
            $message->to('norazwin@mohr.gov.my', "Admin 1Master");;
        });
*/

        if ($contactus->save()) {
            return redirect('profile/contact-us')->with('successMessage', 'Message has been successfully sent');
        } else {
            return back()->with('errorMessage', 'Unable to sent message. Contact admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function show(ContactUs $contactUs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUs $contactUs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactUs $contactUs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactUs $contactUs)
    {
        //
    }
}
