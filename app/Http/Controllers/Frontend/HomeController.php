<?php

namespace App\Http\Controllers\Frontend;

use App\Activity;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;
use App\Slider;

class HomeController extends Controller
{
    public function index()
    {
        $totalCourse = Course::where('status', 'active')->count();
        $totalActivity = Course::where('status', 'active')->count();
        //$totalVisit = Course::where('status', 'active')->sum('visit');
        //$totalStudent = Course::where('status', 'active')->sum('visit');
        $activities = Activity::where('status', 'Published')->orderBy('id', 'desc')->take(6)->get();
        $galleries = Gallery::where('status', 'active')->orderBy('id', 'desc')->take(24)->get();
        $sliders = Slider::where('status', 'active')->orderBy('id', 'desc')->get();

        return view('frontend.home.index', compact('totalActivity', 'totalCourse',
            'totalStudent', 'totalVisit', 'activities','galleries','sliders', 'mostPopularActivities', 'mostPopularCourses'));
    }

    public function about()
    {
        return view('frontend.home.about');
    }

    public function search()
    {
        return view('frontend.home.search');
    }
}
