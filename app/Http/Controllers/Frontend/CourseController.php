<?php

namespace App\Http\Controllers\Frontend;

use App\Audience;
use App\Course;
use App\CourseTimeFrame;
use App\CourseType;
use App\Field;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Lookup;
use App\Ministry;
use App\NOSSSector;
use App\Organization;
use App\Session;
use App\State;
use App\SubField;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CourseController extends Controller
{
    public function short()
    {
        $courses = Course::where('status','4')->paginate(3);
        $fields = Field::where('status','active')->get();
        $subFields = SubField::where('status','active')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->orderBy('value','asc')->get();
        //$countdowns = Course::where('status','active')->where('timeframe','short')->orderBy('date_start','desc')->latest()->get();

        return view('frontend.course.short', compact('courses','fields','audiences','subFields'));
    }

    //for frontend page short search
    public function shortSearch(Request $request)
    {
        $fields = Field::where('status','active')->get();
        $subFields = SubField::where('status','active')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->orderBy('value','asc')->get();
        $query = $request->input('query');

        $courses = Course::where('name', 'like', '%'.$query.'%')->paginate(3);
        $request->flash();

        return view('frontend.course.short', compact('courses', 'fields', 'audiences','subFields'));
    }

    //for frontend page short_more
    public function show($id)
    {
        //Select * from users where id = 1 Limit 1
        $course = Course::findOrFail($id);
        $course->hit = $course->hit +1;
        $course->save();
        return view('frontend.course.short_more', compact('course'));
    }

    public function shortFilter(Request $request)
    {
        $subField = $request->get('subField');
        //return $subField;
        $audience = $request->get('audience');
        $fields = Field::where('status','active')->get();
        $subFields = SubField::where('status','active')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->orderBy('value','asc')->get();
        //$countdowns = Course::where('status','active')->where('timeframe','short')->orderBy('date_start','desc')->latest()->get();

        if ($courses = Course::where('status','4')->where('sub_field_id',$subField)->orWhere('audience1',$audience)->orWhere('audience2',$audience)->orWhere('audience3',$audience)->orWhere('audience4',$audience)->paginate(3)) {
            //return $courses;
            return view('frontend.course.short', compact('courses', 'fields', 'audiences','subFields'));
        }
        else {
            $courses = Course::where('status','4')->paginate(3);
            return view('frontend.course.short', compact('courses', 'fields', 'audiences', 'subFields'));
        }

    }

    public function long()
    {
        $courses = Course::where('status','active')->where('timeframe','long')->get();
        $noss_sectors = Course::where('status','active')->where('timeframe','long')->get();
        $audiences = Course::where('status','active')->where('timeframe','long')->get();
        $countdowns = Course::where('status','active')->where('timeframe','long')->orderBy('date_start','desc')->latest()->get();

        return view('frontend.course.long', compact('courses','noss_sectors','audiences','countdowns'));
    }

    //for frontend page long_more
    public function longMore($id)
    {
        //Select * from users where id = 1 Limit 1
        $course = Course::findOrFail($id);
        $course->hit = $course->hit +1;
        $course->save();
        return view('frontend.course.long_more', compact('course'));
    }

    public function longFilter(Request $request)
    {
        $noss_sector = $request->get('noss_sector');
        $audience = $request->get('audience');
        $noss_sectors = Course::where('status','active')->where('timeframe','long')->get();
        $audiences = Course::where('status','active')->where('timeframe','long')->get();
        $countdowns = Course::where('status','active')->where('timeframe','long')->orderBy('date_start','desc')->latest()->get();

        if ($courses = Course::where('status','active')->where('timeframe','long')->where('noss_sector',$noss_sector)->orWhere('audience',$audience)->get()) {
            return view('frontend.course.short', compact('courses', 'noss_sectors', 'audiences','countdowns'));
        }
        else {
            $courses = Course::where('status','active')->where('timeframe','long')->get();
            return view('frontend.course.long', compact('courses', 'noss_sectors', 'audiences','countdowns'));
        }

    }

    public function venue()
    {
        $courses = Course::where('status','4')->paginate(3);
        $states = State::where('status','active')->get();
        $venues = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        //$countdowns = Course::where('status','active')->orderBy('date_start','desc')->latest()->get();
        //$counter = Course::get('date_start');
        //$mytimes = Carbon::now();
        //$counters = $mytimes->diff($counter);
        //$courses = Course::findOrFail($state);
        //$courses = Course::where('state', $states)->get();
        //$states = State::where('status','active')->get();
        //echo $courses;
        //echo $state;
        //echo $mytimes;
        return view('frontend.course.venue', compact('courses','states','venues'));
    }

    //for frontend page venue search
    public function venueSearch(Request $request)
    {
        $states = State::where('status','active')->get();
        $venues = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        $query = $request->input('query');

        $courses = Course::where('name', 'like', '%'.$query.'%')->paginate(3);
        $request->flash();

        return view('frontend.course.venue', compact('courses', 'states', 'venues'));
    }

    //for frontend page long_more
    public function venueMore($id)
    {
        //Select * from users where id = 1 Limit 1
        $course = Course::findOrFail($id);
        $course->hit = $course->hit +1;
        $course->save();
        return view('frontend.course.venue_more', compact('course'));
    }

    /**
     * @param $state
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function filter(Request $request)
    {
        //$state = $request->get('state');
        $venue = $request->get('venue');
        //$course_id = Session::where('state',$state)->value('course_id');
        $states = State::where('status','active')->get();
        $venues = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        //$countdowns = Course::where('status','active')->orderBy('date_start','desc')->latest()->get();
        //$mytimes = Carbon::now();
        //$counters = $mytimes->diff($countdowns);

        //$courses = Course::all();
        //return $course_id;
        //$states = Course::all();
        //$venues = Course::all();
        //$courses = Course::findOrFail($state);
        if ($courses = Course::where('status','4')->where('organization_id',$venue)->paginate(3)) {
            //echo $state;
            //echo $venue;
            //echo $courses;
            //$courses = Course::where('state', '=', $state)->orWhere('venue', '=',  $venue)->orWhere('name', 'LIKE', '%' . $search . '%')->get()
            return view('frontend.course.venue', compact('courses', 'states', 'venues'));
        }
        else {
            $courses = Course::where('status','active')->paginate(3);
            return view('frontend.course.venue', compact('courses', 'states', 'venues'));
        }
        //$courses = Course::where('venue', $venue)->get();
        //$states = State::where('status', 'active')->get();
        //echo $courses;
        //echo $state;
        //echo $venue;
        //return view('frontend.course.venue', compact('courses','states','venues'));
    }

    public function sponsorship()
    {
        return view('frontend.course.sponsorship');
    }
}
