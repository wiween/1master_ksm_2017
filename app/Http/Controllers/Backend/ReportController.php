<?php

namespace App\Http\Controllers\Backend;

use App\Organization;
use App\Session;
use App\State;
use App\SubField;
use App\Target;
use App\Visit;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use ConsoleTVs\Charts\Facades\Charts;
use Carbon\Carbon;
use App\Course;
use App\Field;


class ReportController extends Controller
{
    // UTK CHART
    public function chartAllUsers()
    {
        $currentYear = Carbon::now()->year;


        /*$participantTargets = Target::where('year', $currentYear)->get()->participant;
        return $participantAchievement = Session::select('total_participant', DB::raw('sum (total_participant) as total'))->groupBy('course_id')->get();
        $participantLegend = Organization::pluck('name', 'id')->toArray();

        $chart = Charts::multi('bar', 'highcharts')
            // Setup the chart settings
            ->title("Participant : Target & Achievement")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)// Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Target', $participantTargets)
           // ->dataset('Participant', $participantAchievement)
            // Setup what the values mean
            ->labels(['Target', 'T', 'Three']);*/

        $targets =  Target::where('year', $currentYear)->pluck('visit');
        $visitAchievement = Visit::where('year', $currentYear)->groupBy('organization_id')->sum('hit');
        $visitLegend = Organization::pluck('name', 'id')->toArray();


//        $chart2 = Charts::multi('bar', 'highcharts')
//            // Setup the chart settings
//            ->title("Visit : Target & Achievement")
//            // A dimension5 of 0 means it will take 100% of the space
//            ->dimensions(0, 400)// Width x Height
//            // This defines a preset of colors already done:)
//            ->template("material")
//            // You could always set them manually
//            // ->colors(['#2196F3', '#F44336', '#FFC107'])
//            // Setup the diferent datasets (this is a multi chart)
//            ->dataset('Target', [ $targets->visit ])
//            ->dataset('Achievement', $visitAchievement)
//            // Setup what the values mean
//            ->labels($visitLegend);


       // $sessions = Session::select('state', DB::raw('count(*) as total'))->groupBy('state')->get();

        //foreach ($sessions as $session)
//        {
//            $stateName = State::where('state_id', (int)$session->state)->first()->name;
//            $session->state = $stateName;
//        }

        $legend = State::pluck('name', 'id')->toArray();

        //return $sessions;

        $chartSession = Charts::database(Session::all(), 'bar', 'highcharts')
            ->title('Course Session By State')
            ->dimensions(1000, 500)
            ->responsive(false)
            ->elementLabel("Total Course Session")
            ->groupBy('state', null, $legend);

        //field
        /* $fields = Field::all();
        foreach ($fields as $field)
        {
            $subField = SubField::where('year', $currentYear)->sum('man_age1518');
        }



      /*$pieChaield = Charts::create('pie', 'highcharts')
          ->title('Courses by Field')
          ->dimensions(1000, 500)
          ->responsive(false)
          ->labels(['15-18', '19-25', '26-40', '>41'])
          ->values([$totalField]);*/

        //Total Age Man
        $totalAgeMan1518 = Session::where('year', $currentYear)->sum('man_age1518');
        $totalAgeMan1925 = Session::where('year', $currentYear)->sum('man_age1925');
        $totalAgeMan2640 = Session::where('year', $currentYear)->sum('man_age2640');
        $totalAgeMan41 = Session::where('year', $currentYear)->sum('man_age41');
        $totalAllMan = $totalAgeMan41 + $totalAgeMan2640 + $totalAgeMan1925 + $totalAgeMan1518;

        $pieChartAgeMan = Charts::create('pie', 'highcharts')
            ->title('Courses by Age - Man ')
            ->dimensions(500, 300)
            ->responsive(false)
            ->labels(['15-18', '19-25', '26-40', '>41'])
            ->values([$totalAgeMan1518, $totalAgeMan1925, $totalAgeMan2640, $totalAgeMan41]);

        $totalAgeWoman1518 = Session::where('year', $currentYear)->sum('woman_age1518');
        $totalAgeWoman1925 = Session::where('year', $currentYear)->sum('woman_age1925');
        $totalAgeWoman2640 = Session::where('year', $currentYear)->sum('woman_age2640');
        $totalAgeWoman41 = Session::where('year', $currentYear)->sum('woman_age41');
        $totalAllWoman = $totalAgeWoman41 + $totalAgeWoman2640 + $totalAgeWoman1925 + $totalAgeWoman1518;

        $pieChartAgeWoman = Charts::create('pie', 'highcharts')
            ->title('Course by Age - Woman')
            ->labels(['15-18', '19-25', '26-40', '>41'])
            ->values([$totalAgeWoman1518, $totalAgeWoman1925, $totalAgeWoman2640, $totalAgeWoman41])
            ->dimensions(500, 300)
            ->responsive(false);
        //->groupBy('role');

        //total student - kotak atas
        $totalStudents = Session::where('year', $currentYear)->sum('students');
        $totalGraduates = Session::where('year', $currentYear)->sum('graduates');
        $totalExistingEmployees = Session::where('year', $currentYear)->sum('existing_employee');
        $totalSchoolLeavers = Session::where('year', $currentYear)->sum('school_leavers');
        $totalSME = Session::where('year', $currentYear)->sum('sme');
        $totalBigCompany = Session::where('year', $currentYear)->sum('big_company');

        //table
        $totalMalay = Session::where('year', $currentYear)->sum('malay');
        $totalChinese = Session::where('year', $currentYear)->sum('chinese');
        $totalIndian = Session::where('year', $currentYear)->sum('indian');
        $totalDayak = Session::where('year', $currentYear)->sum('dayak');
        $totalOthers = Session::where('year', $currentYear)->sum('others');
        $totalEthnic = $totalMalay + $totalChinese + $totalIndian + $totalDayak + $totalOthers;


        return view('backend.chart.index', compact('chartSession', 'chart', 'chart2',
            'pieChartField', 'pieChartAgeMan', 'pieChartAgeWoman', 'totalStudents',
            'totalExistingEmployees', 'totalGraduates', 'totalSchoolLeavers',
            'totalSME', 'totalBigCompany', 'totalMalay', 'totalChinese', 'totalIndian', 'totalDayak', 'totalOthers', 'totalEthnic'));
    }

    public function indexPdf()
    {
        return view('backend.report.index');
    }

    public function reportPdf()
    {
        $users = User::whereMonth('created_at', '08')->orderBy('id')->take(10)->get();
        $pdf = PDF::loadView('backend.report.report', compact('users'));
        //return $pdf->download('report.pdf');
        return $pdf->stream('report.pdf');
    }

}
