<?php

namespace App\Http\Controllers\Backend;

use App\Activity;
use App\Audience;
use App\Http\Requests\ActivityRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lookup;
use App\Ministry;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
Use App\Organization;
Use Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $activities = Activity::all();   // select * from activities

        return view('backend.activity.index', compact('activities'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Lookup::where('name', 'publish_status')->orderBy('value', 'asc')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $user = Auth::User()->id;
//        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
//        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.activity.create',compact('audiences', 'statuses','ministries','user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityRequest $request)
    {
        $activity = new Activity();
        $activity->name = $request->input('name');
        $activity->date_start = $request->input('date_start');
        $activity->date_end = $request->input('date_end');
        $activity->time = $request->input('time');
        $activity->venue = $request->input('venue');
        $activity->officiated_by = $request->input('officiated_by');
        $activity->audience = $request->input('audience');
        $activity->organization_id = $request->input('ministry');
        $activity->participant = $request->input('participant');
        $activity->description = $request->input('description');
        $activity->remark = $request->input('remark');
        if ((Auth::user()->id)  == 1 || (Auth::user()->id)  == 2)
        {
            $activity->status = $request->input('status');
        }
        else
        {
            $activity->status = 'Unpublished';
        }
        // Untuk upload gambar avatar
        if (isset($request->image)) {
            if ($request->file('image')->isValid())
            {
                $destinationPath = "images/activity/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('image')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $activity->image = '/' . $destinationPath . $fileName;
            }
        }

        if ($activity->save()) {
            return redirect('admin/activity/')->with('successMessage', 'Activity has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new activity into database. Contact admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // SELECT * FROM users WHERE id = 1 LIMIT 1
        $activity = Activity::findOrFail($id);
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        //$organizations = Organization::all();
        //$venue = Activity::where('venue','id')->where;
//        $departments = Department::where('status', 'active')->orderBy('value', 'asc')->get();
//        // SELECT * FROM lookups WHERE 'name' = 'user_status' AND 'status' = 'active' ORDERBY ASC
//        $agencies = Agency::where('name', 'agencies_name')->where('status', 'active')->orderBy('value','asc')->get();
        //$agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.activity.show', compact('activity','ministries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::findOrFail($id);
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
//        $departments = Department::where('status', 'active')->orderBy('value', 'asc')->get();
//        // SELECT * FROM lookups WHERE 'name' = 'user_status' AND 'status' = 'active' ORDERBY ASC
//        $agencies = Agency::where('name', 'agencies_name')->where('status', 'active')->orderBy('value','asc')->get();
        $statuses = Lookup::where('name', 'publish_status')->orderBy('value', 'asc')->get();
        $audiences = Lookup::where('status','active')->get();
        return view('backend.activity.edit', compact('activity', 'statuses', 'audiences','ministries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);
        $activity->name = $request->input('name');
        $activity->date_start = $request->input('date_start');
        $activity->date_end = $request->input('date_end');
        $activity->time = $request->input('time');
        $activity->venue = $request->input('venue');
        $activity->audience = $request->input('audience');
        $activity->status = $request->input('status');
        $activity->description = $request->input('description');
        $activity->remark = $request->input('remark');

        // Untuk upload gambar avatar
        if (isset($request->image)) {
            if ($request->file('image')->isValid()) {
                $destinationPath = "images/activity/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('image')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $activity->image = '/' . $destinationPath . $fileName;
            }
        }

        if ($activity->save()) {
            return redirect('admin/activity/show/'.$id)->with('successMessage', 'Activity has been successfully Updated');
        } else {
            return back()->with('errorMessage', 'Unable to create new activity into database. Contact admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::findOrFail($id);

        if($activity->delete()) {
            return back()->with('successMessage', 'The activity has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the activity from database');
        }
    }

    public function setFlag($flag, $id)
    {
        $activity = Activity::findOrFail($id);
        $activity->status = $flag;

        if ($activity->save()) {
            return redirect('admin/activity')->with('successMessage', 'Status has been set.');
        } else {
            return back()->with('errorMessage', 'Unable to set status. Contact admin');
        }
    }

    public function ajaxLoadDept($ministryId)
    {
        //
        $departments = Organization::where('parent_id', $ministryId)->
        where('type','department')->
        where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.ajax.getDepartment', compact('departments'));
    }

//    public function ajaxLoadAgency($agencyId)
//    {
//        //
//        $agencies = Organization::where('parent_id',$agencyId )->
//        where('type','agency')->where('status', 'active')->
//        orderBy('name', 'asc')->get();
//
//        return view('backend.ajax.getAgency', compact('agencies'));
//    }
}
