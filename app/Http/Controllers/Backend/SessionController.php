<?php

namespace App\Http\Controllers\Backend;

use App\Course;
use App\Session;
use App\Organization;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sessions = Session::all();  // select * from target

        return view('backend.session.index', compact('sessions'));
    }

    public function monthlyReport()
    {
        //
        $monthlyReports = Session::where('total_participant','<>','0')->get();
        //$monthlyReports = Session::all();   // select * from target

        return view('backend.monthlyreport.index', compact('monthlyReports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $states = State::where('status','active')->get();
        $years = range(2000, 2050);
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $currentMonth = Carbon::now()->format('F');
        $courses = Course::where('status', '4')->orderBy('name', 'asc')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.session.create', compact('departments','agencies','courses','ministries','years', 'months', 'currentMonth', 'states'));
    }

    public function monthlyReportCreate()
    {
        //
        $states = State::where('status','active')->get();
        //$courses = [];
        //$sessions1 = Session::all();
        $sessions = Session::all();
        $years = range(2000, 2050);
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $currentMonth = Carbon::now()->format('F');
        $courses = Course::where('status', '4')->orderBy('name', 'asc')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.monthlyreport.create', compact('departments','agencies','courses','ministries','years', 'months', 'currentMonth', 'states','sessions','sessions1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $session = new Session();
        $session->course_id = $request->input('course');
        $session->year = $request->input('year');
        $session->month = $request->input('month');
        $session->state = $request->input('state');
        $session->venue = $request->input('venue');
        $session->fee = $request->input('fee');

        if ($session->save()) {
            return redirect('admin/session')->with('successMessage', 'Session has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new session into database. Contact admin');
        }
    }

    public function monthlyReportStore(Request $request)
    {
        //
        $sessionId = $request->input('session');
        $total_participant = $request->input('total_participant');
        $man_age1518 = $request->input('man_age1518');
        $woman_age1518 = $request->input('woman_age1518');
        $man_age1925 = $request->input('man_age1925');
        $woman_age1925 = $request->input('woman_age1925');
        $man_age2640 = $request->input('man_age2640');
        $woman_age2640 = $request->input('woman_age2640');
        $man_age41 = $request->input('man_age41');
        $woman_age41 = $request->input('woman_age41');
        $students = $request->input('students');
        $graduates = $request->input('graduates');
        $school_leavers = $request->input('school_leavers');
        $existing_employee = $request->input('existing_employee');
        $malay = $request->input('malay');
        $chinese = $request->input('chinese');
        $indian = $request->input('indian');
        $dayak = $request->input('dayak');
        $others = $request->input('others');

        $monthlyReport = Session::findOrFail($sessionId);
        $monthlyReport->total_participant = $request->input('total_participant');
        $monthlyReport->man_age1518 = $request->input('man_age1518');
        $monthlyReport->woman_age1518 = $request->input('woman_age1518');
        $monthlyReport->man_age1925 = $request->input('man_age1925');
        $monthlyReport->woman_age1925 = $request->input('woman_age1925');
        $monthlyReport->man_age2640 = $request->input('man_age2640');
        $monthlyReport->woman_age2640 = $request->input('woman_age2640');
        $monthlyReport->man_age41 = $request->input('man_age41');
        $monthlyReport->woman_age41 = $request->input('woman_age41');
        $monthlyReport->students = $request->input('students');
        $monthlyReport->graduates = $request->input('graduates');
        $monthlyReport->school_leavers = $request->input('school_leavers');
        $monthlyReport->existing_employee = $request->input('existing_employee');
        $monthlyReport->malay = $request->input('malay');
        $monthlyReport->chinese = $request->input('chinese');
        $monthlyReport->indian = $request->input('indian');
        $monthlyReport->dayak = $request->input('dayak');
        $monthlyReport->others = $request->input('others');
        $monthlyReport->sme = $request->input('sme');
        $monthlyReport->big_company = $request->input('big_company');
        $request->flash();

        if ($total_participant <> $man_age1518+$woman_age1518+$man_age1925+$woman_age1925+$man_age2640+$woman_age2640+$man_age41+$woman_age41) {
            return back()->with('errorMessage', 'Total of Number of Participants by Gender is not equal to Total Participants.');
        } elseif ($total_participant <> $students+$graduates+$school_leavers+$existing_employee) {
            return back()->with('errorMessage', 'Total of Number of Participants by Participant Category is not equal to Total Participants.');
        } elseif ($total_participant <> $malay+$chinese+$indian+$dayak+$others) {
            return back()->with('errorMessage', 'Total of Number of Participants by Race is not equal to Total Participants.');
        } else {
            if ($monthlyReport->save()) {
                return redirect('admin/monthlyreport')->with('successMessage', 'Monthly Report has been successfully created');
            } else {
                return back()->with('errorMessage', 'Unable to create new monthly report into database. Contact admin');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Session  $monthlyreport
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $session = Session::findOrFail($id);
        return view('backend.session.show', compact('session'));
    }

    public function monthlyReportShow($id)
    {
        //
        $monthlyReport = Session::findOrFail($id);
        return view('backend.monthlyreport.show', compact('monthlyReport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Session  $monthlyreport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $session = Session::findOrFail($id);
        $states = State::where('status','active')->get();
        $years = range(2000, 2050);
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $currentMonth = Carbon::now()->format('F');
        $courses = Course::where('status', '4')->orderBy('name', 'asc')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.session.edit', compact('states','years','months','currentMonth','courses','session'));
    }

    public function monthlyReportEdit($id)
    {
        //
        $session = Session::findOrFail($id);
        $states = State::where('status','active')->get();
        //$courses = [];
        //$sessions1 = Session::all();
        $sessions = Session::all();
        $years = range(2000, 2050);
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $currentMonth = Carbon::now()->format('F');
        $courses = Course::where('status', '4')->orderBy('name', 'asc')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.monthlyreport.edit', compact('departments','agencies','courses','ministries','years', 'months', 'currentMonth', 'states','sessions','session'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Session  $monthlyreport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $session = Session::findOrFail($id);
        $session->course_id = $request->input('course');
        $session->year = $request->input('year');
        $session->month = $request->input('month');
        $session->state = $request->input('state');
        $session->venue = $request->input('venue');
        $session->fee = $request->input('fee');

        if ($session->save()) {
            return redirect('admin/session/show/'.$id)->with('successMessage', 'Session has been successfully updated');
        } else {
            return back()->with('errorMessage', 'Unable to update session into database. Contact admin');
        }
    }

    public function monthlyReportUpdate(Request $request, $id)
    {
        //
        $total_participant = $request->input('total_participant');
        $man_age1518 = $request->input('man_age1518');
        $woman_age1518 = $request->input('woman_age1518');
        $man_age1925 = $request->input('man_age1925');
        $woman_age1925 = $request->input('woman_age1925');
        $man_age2640 = $request->input('man_age2640');
        $woman_age2640 = $request->input('woman_age2640');
        $man_age41 = $request->input('man_age41');
        $woman_age41 = $request->input('woman_age41');
        $students = $request->input('students');
        $graduates = $request->input('graduates');
        $school_leavers = $request->input('school_leavers');
        $existing_employee = $request->input('existing_employee');
        $malay = $request->input('malay');
        $chinese = $request->input('chinese');
        $indian = $request->input('indian');
        $dayak = $request->input('dayak');
        $others = $request->input('others');

        $monthlyReport = Session::findOrFail($id);
        $monthlyReport->total_participant = $request->input('total_participant');
        $monthlyReport->man_age1518 = $request->input('man_age1518');
        $monthlyReport->woman_age1518 = $request->input('woman_age1518');
        $monthlyReport->man_age1925 = $request->input('man_age1925');
        $monthlyReport->woman_age1925 = $request->input('woman_age1925');
        $monthlyReport->man_age2640 = $request->input('man_age2640');
        $monthlyReport->woman_age2640 = $request->input('woman_age2640');
        $monthlyReport->man_age41 = $request->input('man_age41');
        $monthlyReport->woman_age41 = $request->input('woman_age41');
        $monthlyReport->students = $request->input('students');
        $monthlyReport->graduates = $request->input('graduates');
        $monthlyReport->school_leavers = $request->input('school_leavers');
        $monthlyReport->existing_employee = $request->input('existing_employee');
        $monthlyReport->malay = $request->input('malay');
        $monthlyReport->chinese = $request->input('chinese');
        $monthlyReport->indian = $request->input('indian');
        $monthlyReport->dayak = $request->input('dayak');
        $monthlyReport->others = $request->input('others');
        $monthlyReport->sme = $request->input('sme');
        $monthlyReport->big_company = $request->input('big_company');
        $request->flash();

        if ($total_participant <> $man_age1518+$woman_age1518+$man_age1925+$woman_age1925+$man_age2640+$woman_age2640+$man_age41+$woman_age41) {
            return back()->with('errorMessage', 'Total of Number of Participants by Gender is not equal to Total Participants.');
        } elseif ($total_participant <> $students+$graduates+$school_leavers+$existing_employee) {
            return back()->with('errorMessage', 'Total of Number of Participants by Participant Category is not equal to Total Participants.');
        } elseif ($total_participant <> $malay+$chinese+$indian+$dayak+$others) {
            return back()->with('errorMessage', 'Total of Number of Participants by Race is not equal to Total Participants.');
        } else {
            if ($monthlyReport->save()) {
                return redirect('admin/monthlyreport/show/'.$id)->with('successMessage', 'Monthly Report has been successfully updated');
            } else {
                return back()->with('errorMessage', 'Unable to update monthly report into database. Contact admin');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Session  $monthlyreport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $session = Session::findOrFail($id);

        if($session->delete()) {
            return back()->with('successMessage', 'The session has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the session from database');
        }
    }

    public function monthlyReportDestroy($id)
    {
        //
        $monthlyReport = Session::findOrFail($id);

        if($monthlyReport->delete()) {
            return back()->with('successMessage', 'The monthly report has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the monthly report from database');
        }
    }


//    public function ajaxLoadDept($ministryId)
//    {
//        //
//        $departments = Organization::where('parent_id', $ministryId)->
//        where('type','department')->
//        where('status', 'active')->orderBy('name', 'asc')->get();
//        return view('backend.ajax.getDepartment', compact('departments'));
//    }
//
//    public function ajaxLoadAgency($agencyId)
//    {
//        //
//        $agencies = Organization::where('parent_id',$agencyId )->
//        where('type','agency')->where('status', 'active')->
//        orderBy('name', 'asc')->get();
//
//        return view('backend.ajax.getAgency', compact('agencies'));
//    }

    public function ajaxLoadCourse($courseId)
    {
        //
        $courses = Course::where('id', $courseId)->
        orderBy('created_at', 'desc')->get();

        return view('backend.ajax.getCourse', compact('courses'));
    }

    public function ajaxLoadSession($sessionId)
    {
        //
        $sessions = Session::where('course_id', $sessionId)->
        orderBy('created_at', 'desc')->get();

        return view('backend.ajax.getSession', compact('sessions'));
    }
}
