<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DashboardController extends Controller
{
    // display dashboard or home
    // 1master.dev/dashboard
    public function index()
    {

        $role = Auth::user()->role;

        if ($role == 'user')
        {
            return redirect('/psychometric/profile');
        }
        else
        {
            return view('backend.dashboard.index');
        }
       
    }

    public function home()
    {
        return view('backend.dashboard.index');

    }

}
