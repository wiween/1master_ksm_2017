<?php

namespace App\Http\Controllers\Backend;

use App\Audience;
use App\Course;
use App\CourseTimeFrame;
use App\CourseType;
use App\Field;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Lookup;
use App\NOSSSector;
use App\Organization;
use App\State;
use App\SubField;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$ministries = Organization::all();
        $courses = Course::all();
        //$courses = Course::raw("SELECT * FROM courses c, organizations o WHERE c.ministry = o.id")->get();
        //$ministries = Organization::where('id','=','courses.ministry')->get();

        return view('backend.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$timeframes = CourseTimeFrame::where('status','active')->get();
        //$course_types = CourseType::where('status','active')->get();
        $states = State::where('status','active')->get();
        $fields = Field::where('status','active')->get();
        $sub_fields = SubField::where('status','active')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = [];
        $agencies = [];
//        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
//        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        //$noss_sectors = NOSSSector::where('status','active')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->orderBy('value','asc')->get();
        $statuses = Lookup::where('name','publish_status')->where('status','active')->orderBy('value','asc')->get();

        return view('backend.course.create', compact('states','ministries','audiences','statuses','departments','agencies','fields','sub_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $course = new Course();
        $course->name = $request->input('name');
        $course->sub_field_id = $request->input('sub_field');
        $course->organization_id = $request->input('agency');
        $course->introduction = $request->input('introduction');
        $course->job = $request->input('job');
        $course->salary = $request->input('salary');
        $course->syllabus= $request->input('syllabus');
        $course->contact = $request->input('contact');
        $course->duration = $request->input('duration');
        $course->audience1 = $request->input('audience1');
        $course->audience2 = $request->input('audience2');
        $course->audience3 = $request->input('audience3');
        $course->audience4 = $request->input('audience4');
        $course->status = $request->input('status');

        //untuk upload gambar image
        if (isset($request->image)){
            if ($request->file('image')->isValid()){
                $destinationPath = "images/course/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32).'.'.$extension;
                $request->file('image')->move($destinationPath, $fileName);

                //standardize the image dimension(optional)
                Image::make($destinationPath.$fileName)->fit(270, 230)->save();

                $course->image = '/'.$destinationPath.$fileName;
            }
        }

        if($course->save()){
            return redirect('admin/course')->with('successMessage', 'Course has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new course into database. Contact Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Select * from users where id = 1 Limit 1
        $course = Course::findOrFail($id);
        return view('backend.course.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        //$avatar = $course->avatar;
        $fields = Field::where('status','active')->get();
        $sub_fields = SubField::where('status','active')->get();
        $statuses = Lookup::where('name','publish_status')->where('status','active')->orderBy('value','asc')->get();
        //$noss_sectors = NOSSSector::where('status','active')->get();
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        $states = State::where('status','active')->get();
        //$course_types = CourseType::where('status','active')->get();
        //$timeframes = CourseTimeFrame::where('status','active')->get();
        $audiences = Lookup::where('name','audiences')->where('status', 'active')->orderBy('value','asc')->get();
        return view('backend.course.edit', compact('course','statuses','ministries','states','audiences','departments','agencies','fields','sub_fields'));
        //return redirect('backend.course_management.edit')-> withInput(['course']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $course->name = $request->input('name');
        $course->sub_field_id = $request->input('sub_field');
        $course->organization_id = $request->input('agency');
        $course->introduction = $request->input('introduction');
        $course->job = $request->input('job');
        $course->salary = $request->input('salary');
        $course->syllabus= $request->input('syllabus');
        $course->contact = $request->input('contact');
        $course->duration = $request->input('duration');
        $course->audience1 = $request->input('audience1');
        $course->audience2 = $request->input('audience2');
        $course->audience3 = $request->input('audience3');
        $course->audience4 = $request->input('audience4');
        $course->status = $request->input('status');

        //untuk upload gambar image
        if (isset($request->image)){
            if ($request->file('image')->isValid()){
                $destinationPath = "images/course/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32).'.'.$extension;
                $request->file('image')->move($destinationPath, $fileName);

                //standardize the image dimension(optional)
                Image::make($destinationPath.$fileName)->fit(270, 230)->save();

                $course->image = '/'.$destinationPath.$fileName;
            }
        }

        if($course->save()){
            //return redirect('course_management/show/'.$id)->with('successMessage', 'Course has been successfully edited');
            return redirect()->action('Backend\CourseController@edit',['id' => $course->id])->with('successMessage', 'Course has been successfully edited');
        } else {
            return back()->with('errorMessage', 'Unable to update course into database. Contact Admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);

        if($course->delete()) {
            return back()->with('successMessage', 'The course has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the course from database');
        }
    }

    public function ajaxLoadDept($ministryId)
    {
        //
        $departments = Organization::where('parent_id', $ministryId)->
        where('type','department')->
        where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.ajax.getDepartment', compact('departments'));
    }

    public function ajaxLoadAgency($agencyId)
    {
        //
        $agencies = Organization::where('parent_id',$agencyId )->
        where('type','agency')->where('status', 'active')->
        orderBy('name', 'asc')->get();

        return view('backend.ajax.getAgency', compact('agencies'));
    }

    public function ajaxLoadSubField($subFieldId)
    {
        //
        $sub_fields = SubField::where('field_id',$subFieldId )->where('status', 'active')->
        orderBy('name', 'asc')->get();

        return view('backend.ajax.getSubField', compact('sub_fields'));
    }
}
