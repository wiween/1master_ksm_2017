<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqEditRequest;
use App\Faq;
use App\Lookup;
use App\Category;

class FaqController extends Controller
{
    public function index()
    {
        $faqses = Faq::orderBy('id', 'desc')->get();
        return view('backend.faq.index', compact('faqses'));
    }

    public function show($id)
    {
        $faqses = Faq::findOrFail($id);
        return view('backend.faq.show', compact('faqses'));
    }

    public function edit($id)
    {
        $faqses = Faq::findOrFail($id);
        $statuses = Lookup::where('name', 'publish_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        $categories = Category::where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.faq.edit', compact('faqses', 'statuses', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqEditRequest $request, $id)
    {
        //
        $faq = Faq::findOrFail($id);
        $faq->answer = $request->input('answer');
        $faq->status = $request->input('status');
        $faq->category = $request->input('category');

        if ($faq->save()) {
            return redirect('admin/faq')->with('successMessage', 'faq has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new faq into database. Contact admin');
        }
    }

    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        if($faq->delete()) {
            return back()->with('successMessage', 'The FAQ feedback has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete FAQ feedback from database');
        }

    }

    public function setFlag($flag, $id)
    {
        $faq = Faq::findOrFail($id);
        $faq->status = $flag;

        if ($faq->save()) {
            return redirect('admin/faq')->with('successMessage', 'Status has been set.');
        } else {
            return back()->with('errorMessage', 'Unable to set status. Contact admin');
        }
    }


}

