<?php

namespace App\Http\Controllers\Backend;

use App\CourseTimeFrame;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseTimeFrameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseTimeFrame  $courseTimeFrame
     * @return \Illuminate\Http\Response
     */
    public function show(CourseTimeFrame $courseTimeFrame)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseTimeFrame  $courseTimeFrame
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseTimeFrame $courseTimeFrame)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseTimeFrame  $courseTimeFrame
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseTimeFrame $courseTimeFrame)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseTimeFrame  $courseTimeFrame
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseTimeFrame $courseTimeFrame)
    {
        //
    }
}
