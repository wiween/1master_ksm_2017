<?php

namespace App\Http\Controllers\Backend;

use App\Visit;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $visits = Visit::all();   // select * from target
        return view('backend.visit.index', compact('visits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //$organization = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $years= range(1980, 2050);
        $months= ['January', 'February', 'Mac', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $currentMonth=Carbon::now()->format('F');
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        //$agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.visit.create', compact('ministries','months', 'years', 'currentMonth', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $visit = new Visit();
        $visit->year = $request->input('year');
        $visit->month = $request->input('month');
        //$visit->organization_id = $request->input('ministry');
        $visit->organization_id = $request->input('department');
       // $visit->organization_id = $request->input('agency');
        $visit->hit = $request->input('hit');


        if ($visit->save()) {
            return redirect('admin/visit')->with('successMessage', 'Visit has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create visit into database. Contact admin');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function show(Visit $visit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $visit = Visit::findOrFail($id);
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type', 'department')->where('status', 'active')->orderBy('name', 'asc')->get();
        // SELECT * FROM lookups WHERE 'name' = 'user_status' AND 'status' = 'active' ORDERBY ASC
       // $agencies = Organization::where('type', 'agencies_name')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.visit.edit', compact('visit', 'ministries','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $visit = Visit::findOrFail($id);
        $visit->year = $request->input('year');
        $visit->month = $request->input('month');
        //$visit->organization_id = $request->input('ministry');
        $visit->organization_id = $request->input('department');
       // $visit->organization_id = $request->input('agency');
        $visit->hit = $request->input('hit');

        if ($visit->save()) {
            return redirect('admin/visit')->with('successMessage', 'visit has been successfully update');
        } else {
            return back()->with('errorMessage', 'Unable to create into database. Contact admin');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $visit = Visit::findOrFail($id);

        if($visit->delete()) {
            return back()->with('successMessage', 'The visit has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete from database');
        }
    }

    public function ajaxLoadDept($ministryId)
    {
        //
        $departments = Organization::where('parent_id', $ministryId)->
        where('type','department')->
        where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.ajax.getDepartment', compact('departments'));
    }


}
