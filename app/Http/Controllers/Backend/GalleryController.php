<?php

namespace App\Http\Controllers\Backend;

use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use App\Lookup;
use App\Http\Requests\GalleryRequest;
use Intervention\Image\Facades\Image;
use App\Auth;


class GalleryController extends Controller
{
    //
    public function index()
    {
        $galleries = Gallery::orderBy('id', 'desc')->get();   // select * from contactus

        return view('backend.gallery.index', compact('galleries'));
    }

    public function create()
    {
        //
        $statuses = Lookup::where('name', 'user_status')->orderBy('value', 'asc')->get();
        //return view('backend.gallery.create',compact( 'statuses'));

//        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
//        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.gallery.create', compact('statuses', 'agencies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new Gallery();
        $gallery->remark = $request->input('remark');
        $gallery->status = $request->input('status');
//        $gallery->ministry = $request->input('ministry');
//        $gallery->department = $request->input('department');
        $gallery->organization_id = $request->input('agency');
        //untuk upload gambar avatar
        if (isset($request->image)){
            if ($request->file('image')->isValid()){
                $destinationPath = "images/gallery/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32).'.'.$extension;
                $request->file('image')->move($destinationPath, $fileName);

                //standardize the image dimension(optional)
                Image::make($destinationPath.$fileName)->fit(540, 440)->save();

                $gallery->image = '/'.$destinationPath.$fileName;
            }
        }

        if($gallery->save()){
            return redirect('admin/gallery')->with('successMessage', 'Gallery has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new course into database. Contact Admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);

//        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
//        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
        $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        $statuses = Lookup::where('name', 'user_status')->orderBy('value', 'asc')->get();
        return view('backend.gallery.edit', compact('gallery', 'statuses', 'agencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->status = $request->input('status');
        $gallery->remark = $request->input('remark');
//        $gallery->ministry = $request->input('ministry');
//        $gallery->department = $request->input('department');
        $gallery->organization_id = $request->input('agency');

        // Untuk upload gambar avatar
        if (isset($request->image)) {
            if ($request->file('image')->isValid()) {
                $destinationPath = "images/gallery/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('image')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(540, 440)->save();

                $gallery->image = '/' . $destinationPath . $fileName;
            }
        }

        if ($gallery->save()) {
            return redirect('admin/gallery')->with('successMessage', 'gallery has been successfully update');
        } else {
            return back()->with('errorMessage', 'Unable to create new gallery into database. Contact admin');
        }
    }

    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('backend.gallery.show', compact('gallery'));
    }


    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        if($gallery->delete()) {
            return back()->with('successMessage', 'The photo gallery has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the contact us feedback from database');
        }

    }

//    public function confirmDestroy(Request $request, $id)
//    {
//        $user = Auth::user();
//    }

    public function setFlag($flag, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->status = $flag;

        if ($gallery->save()) {
            return redirect('admin/gallery')->with('successMessage', 'Status has been set.');
        } else {
            return back()->with('errorMessage', 'Unable to set status. Contact admin');
        }
    }

//    public function ajaxLoadDept($ministryId)
//    {
//        //
//        $departments = Organization::where('parent_id', $ministryId)->
//        where('type','department')->
//        where('status', 'active')->orderBy('name', 'asc')->get();
//        return view('backend.ajax.getDepartment', compact('departments'));
//    }

    public function ajaxLoadAgency($agencyId)
    {
        //
        $agencies = Organization::where('parent_id',$agencyId )->
        where('type','agency')->where('status', 'active')->
        orderBy('name', 'asc')->get();

        return view('backend.ajax.getAgency', compact('agencies'));
    }
}
