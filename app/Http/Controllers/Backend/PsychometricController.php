<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\UploadExcelRequest;
use App\Http\Requests\UploadPdfRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Psychometric;
use App\Http\Requests\PsychometricRequest;
use Excel;
use Auth;
use App\Http\Requests\PsychometricEditRequest;

class PsychometricController extends Controller
{
    //
    public function index()
    {
        $psychometrics = Psychometric::all();   // select * from psychometrics
        $users = User::whereNotNull('enrolled_at')->get();
        return view('backend.psychometric.index', compact('psychometrics', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereNotNull('enrolled_at')->get();
        return view('backend.psychometric.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PsychometricRequest $request)
    {
        $psychometric = new Psychometric();
        $psychometric->user_id = $request->input('ic_number');
        $psychometric->field = $request->input('field');
        $psychometric->score = $request->input('score');
        $psychometric->category = $request->input('category');
        $psychometric->uploaded_by = Auth::id();
        if ($psychometric->save()) {
            return redirect('admin/psychometric/form')->with('successMessage', 'psychometric has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new psychometric into database. Contact administrator');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // SELECT * FROM psychometrics WHERE id = 1 LIMIT 1
        $psychometric = psychometric::findOrFail($id);
        return view('backend.psychometric.show', compact('psychometric'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $psychometric = psychometric::findOrFail($id);
        $psychoList = ['finance', 'engineering', 'admin', 'legal'];
        $psychoCategory = ['high', 'medium', 'high'];
        return view('backend.psychometric.edit', compact('psychometric', 'psychoList', 'psychoCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PsychometricEditRequest $request, $id)
    {
        $psychometric = psychometric::findOrFail($id);
        $psychometric->field = $request->input('field');
        $psychometric->category = $request->input('category');
        $psychometric->score = $request->input('score');

        if ($psychometric->save()) {
            return redirect('admin/psychometric/form/show/' . $id)->with('successMessage', 'psychometric result has been successfully updated');
        } else {
            return back()->with('errorMessage', 'Unable to update new psychometric record into database. Contact administrator');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $psychometric = psychometric::findOrFail($id);

        if ($psychometric->delete()) {
            return back()->with('successMessage', 'The psychometric has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the psychometric from database');
        }
    }

    public function uploadPDF()
    {
        $psychometrics = Psychometric::all();
        $users = User::whereNotIn('id', $psychometrics->pluck('id'))->whereNotNull('enrolled_at')->get();

        return view('backend.psychometric.pdf.create', compact('users'));
    }


    public function uploadExcel()
    {
        return view('backend.psychometric.upload-excel');
    }

    public function storeExcel(UploadExcelRequest $request)
    {
        //$psychometric->remark = $request->input('remark');
        // Untuk upload excel
        if (isset($request->fileexcel)) {
            if ($request->file('fileexcel')->isValid()) {
                $path = $request->file('fileexcel')->getRealPath();
                $records = Excel::load($path)->get();

                if ($records->count()) {
                    foreach ($records as $record) {
                        $psychometric = new Psychometric();
                        $ic_number = $record->ic_number;
                        $user = User::where('ic_number', $ic_number)->first();
                        $psychometric->user_id = $user->id;
                        $psychometric->field = $record->field;
                        $psychometric->category = $record->category;
                        $psychometric->score = $record->score;
                        $psychometric->uploaded_by = Auth::id();
                        $psychometric->save();

                    }
                }

                return redirect('admin/psychometric/upload-excel')->with('successMessage', 'File has been successfully uploaded');

            }
        }

//        if ($psychometric->save()) {
//            return redirect('admin/psychometric/upload-excel')->with('successMessage', 'File has been successfully uploaded');
//        } else {
//            return back()->with('errorMessage', 'Unable to upload file. Contact admin');
//        }

    }

}
