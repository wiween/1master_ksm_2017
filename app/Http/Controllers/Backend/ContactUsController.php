<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactUs;

class ContactUsController extends Controller
{
    //
    public function index()
    {
        $contactuses = ContactUs::orderBy('id', 'desc')->get();;   // select * from contactus

        return view('backend.contact_us.index', compact('contactuses'));
    }

    public function show($id)
    {
        $contactus = ContactUs::findOrFail($id);
        return view('backend.contact_us.show', compact('contactus'));
    }


    public function destroy($id)
    {
        $contactus = ContactUs::findOrFail($id);
        if($contactus->delete()) {
            return back()->with('successMessage', 'The contact us feedback has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the contact us feedback from database');
        }

    }

    public function setFlag($flag, $id)
    {
        $contactus = ContactUs::findOrFail($id);
        $contactus->status = $flag;

        if ($contactus->save()) {
            return redirect('admin/contact-us')->with('successMessage', 'Status has been set.');
        } else {
            return back()->with('errorMessage', 'Unable to set status. Contact admin');
        }
    }
}
