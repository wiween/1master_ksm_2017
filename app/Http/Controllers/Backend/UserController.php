<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\UserChangePasswordRequest;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use App\Lookup;
use App\Role;
use App\User;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();   // select * from users

        return view('backend.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        // SELECT * FROM lookups WHERE 'name' = 'user_status' AND 'status' = 'active' ORDERBY ASC
        $roles = Role::where('status', 'active')->get();
        // SELECT * FROM roles WHERE 'status' = 'active'
        return view('backend.user.create', compact('statuses', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->ic_number = $request->input('ic_number');
        $user->phone_number = $request->input('phone_number');
        $user->role = $request->input('role');
        $user->access_power = 100;
        $user->remark = $request->input('remark');
        $user->status = $request->input('status');
        // Untuk upload gambar avatar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/user/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        if ($user->save()) {
            return redirect('admin/user')->with('successMessage', 'User has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new user into database. Contact admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // SELECT * FROM users WHERE id = 1 LIMIT 1
        $user = User::findOrFail($id);
        return view('backend.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        $roles = Role::where('status', 'active')->get();
        return view('backend.user.edit', compact('user', 'statuses', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
//        $user->password = $request->input('password');
        $user->ic_number = $request->input('ic_number');
        $user->phone_number = $request->input('phone_number');
        $user->role = $request->input('role');
        $user->access_power = 100;
        $user->remark = $request->input('remark');
        $user->status = $request->input('status');
        // Untuk upload gambar avatar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/user/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        if ($user->save()) {
            return redirect('admin/user/show/'.$id)->with('successMessage', 'User has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new user into database. Contact admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if($user->delete()) {
            return back()->with('successMessage', 'The user has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the user from database');
        }
    }

    public function profile()
    {
        $user = Auth::user();           // current user yang tengah login punya record
        return view('backend.user.profile', compact('user'));
    }

    public function editProfile()
    {
//        $user = User::findOrFail($id);
        $user = Auth::user();
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        $roles = Role::where('status', 'active')->get();
        return view('backend.user.edit', compact('user', 'statuses', 'roles'));
    }

    public function updateProfile(UserEditRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
//        $user->password = $request->input('password');
        $user->ic_number = $request->input('ic_number');
        $user->phone_number = $request->input('phone_number');
        $user->role = $request->input('role');
        $user->access_power = 100;
        $user->remark = $request->input('remark');
        $user->status = $request->input('status');
        // Untuk upload gambar avatar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/user/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        if ($user->save()) {
            return redirect('user/profile')->with('successMessage', 'Your profile has been updated.');
        } else {
            return back()->with('errorMessage', 'Unable to update your profile. Contact admin');
        }
    }

    public function changePassword()
    {
        return view('backend.user.change_password');
    }

    public function updatePassword(UserChangePasswordRequest $request)
    {
        $user = Auth::user();
        $oldPassword = $request->input('old_password');
        $password = $request->input('password');

        // check old password sama tak dengan yang dlm db
        $genuine = Hash::check($oldPassword, $user->password);
        // kalau sama, dia akan return true

        if ($genuine == false) {
            return back()->withInput()->with('errorMessage', 'Miss match old password');
        } else {
            // store new password into database
            $user->password = bcrypt($password);

            if ($user->save()) {
                return back()->with('successMessage', 'Password has been changed successfully.');
            } else {
                return back()->withInput()->with('errorMessage', 'Unable to change password.');
            }
        }
    }

    public function resetPassword($id)
    {
        $user = User::findOrFail($id);

        // generate 8 random character
        $randomPassword = str_random(8);
        // change user's password dengan randompassword di atas
        $user->password = bcrypt($randomPassword);

        // data to be sent to user via email
        $data['new_password']  = $randomPassword;
        $data['name']  = $user->name;
        $data['email']  = $user->email;

        Mail::send('emails.reset_password', $data, function($message) use ($data)
        {
            $message->from('admin.1master@mohr.gov.my', "Admin 1Master");
            $message->subject("New Password - 1Master");
            $message->to($data['email']);
        });

        if ($user->save()) {
            return redirect('user')->with('successMessage', 'The user password has been reset and an email 
            has been dispatched to the user');
        } else {
            return back()->with('errorMessage', 'Unable to reset password. Contact admin');
        }
    }


}




















