<?php

namespace App\Http\Controllers\Backend;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lookup;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sliders = Slider::orderBy('id', 'desc')->get();   // select * from contactus
        return view('backend.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $statuses = Lookup::where('name', 'user_status')->orderBy('value', 'asc')->get();
        return view('backend.slider.create',compact( 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $slider = new slider();
        $slider->title = $request->input('title');
        $slider->subtitle = $request->input('subtitle');
        $slider->status = $request->input('status');
        //untuk upload gambar avatar
        if (isset($request->sliderphoto)){
            if ($request->file('sliderphoto')->isValid()){
                $destinationPath = "images/frontend/home/slider/";
                $extension = $request->file('sliderphoto')->getClientOriginalExtension();
                $fileName = str_random(32).'.'.$extension;
                $request->file('sliderphoto')->move($destinationPath, $fileName);

                //standardize the image dimension(optional)
                Image::make($destinationPath.$fileName)->fit(1920, 720)->save();

                $slider->sliderphoto = '/'.$destinationPath.$fileName;
            }
        }

        if($slider->save()){
            return redirect('admin/slider')->with('successMessage', 'Course has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new course into database. Contact Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $slider = slider::findOrFail($id);
        $statuses = Lookup::where('name', 'user_status')->orderBy('value', 'asc')->get();
        return view('backend.slider.show', compact('slider', 'statuses'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $slider = slider::findOrFail($id);
        $statuses = Lookup::where('name', 'user_status')->orderBy('value', 'asc')->get();
        return view('backend.slider.edit', compact('slider', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $slider = slider::findOrFail($id);
        $slider->status = $request->input('status');
        $slider->title = $request->input('title');
        $slider->subtitle = $request->input('subtitle');

        // Untuk upload gambar avatar
        if (isset($request->sliderphoto)) {
            if ($request->file('sliderphoto')->isValid()) {
                $destinationPath = "images/frontend/home/slider/";
                $extension = $request->file('sliderphoto')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('sliderphoto')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(1920, 720)->save();

                $slider->sliderphoto = '/' . $destinationPath . $fileName;
            }
        }

        if ($slider->save()) {
            return redirect('admin/slider')->with('successMessage', 'slider has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new slider into database. Contact admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $slider = Slider::findOrFail($id);
        if($slider->delete()) {
            return back()->with('successMessage', 'The slider has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the slider from database');
        }
    }

    public function setFlag($flag, $id)
    {
        $slider = Slider::findOrFail($id);
        $slider->status = $flag;

        if ($slider->save()) {
            return redirect('admin/slider')->with('successMessage', 'Status has been set.');
        } else {
            return back()->with('errorMessage', 'Unable to set status. Contact admin');
        }
    }
}
