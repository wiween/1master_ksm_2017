<?php

namespace App\Http\Controllers\Backend;

use App\Organization;
use App\Target;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $targets = Target::all();   // select * from target
        return view('backend.target.index', compact('targets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $years= range(1980, 2050);
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type','department')->where('status', 'active')->orderBy('name', 'asc')->get();
       // $agencies = Organization::where('type','agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.target.create', compact('ministries','departments','agencies', 'years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target = new Target();
        //$target->ministry = $request->input('ministry');
        $target->organization_id = $request->input('department');
       // $target->organization_id = $request->input('agency');
        $target->year = $request->input('year');
        $target->visit = $request->input('visit');
        $target->participant = $request->input('participant');

        if ($target->save()) {
            return redirect('admin/target')->with('successMessage', 'Target has been successfully created');
        } else {
            return back()->with('errorMessage', 'Unable to create new user into database. Contact admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function show(Target $target)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $target = Target::findOrFail($id);
        $ministries = Organization::where('type','ministry')->where('status', 'active')->orderBy('name', 'asc')->get();
        $departments = Organization::where('type', 'department')->where('status', 'active')->orderBy('name', 'asc')->get();
        // SELECT * FROM lookups WHERE 'name' = 'user_status' AND 'status' = 'active' ORDERBY ASC
        $agencies = Organization::where('type', 'agency')->where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.target.edit', compact('target', 'ministries', 'departments','agencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $target = Target::findOrFail($id);
        //$target->ministry = $request->input('ministry');
        //$target->department = $request->input('department');
        $target->organization_id = $request->input('department');
        $target->year = $request->input('year');
        $target->visit = $request->input('visit');
        $target->participant = $request->input('participant');

        if ($target->save()) {
            return redirect('admin/target')->with('successMessage', 'Target has been successfully update');
        } else {
            return back()->with('errorMessage', 'Unable to create new user into database. Contact admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $target = Target::findOrFail($id);

        if($target->delete()) {
            return back()->with('successMessage', 'The target has been successfully deleted');
        } else {
            return back()->with('errorMessage', 'Unable to delete the user from database');
        }
    }

    public function ajaxLoadDept($ministryId)
    {
        //
        $departments = Organization::where('parent_id', $ministryId)->
        where('type','department')->
        where('status', 'active')->orderBy('name', 'asc')->get();
        return view('backend.ajax.getDepartment', compact('departments'));
    }

    public function ajaxLoadAgency($agencyId)
    {
        //
        $agencies = Organization::where('parent_id',$agencyId )->
        where('type','agency')->where('status', 'active')->
        orderBy('name', 'asc')->get();

        return view('backend.ajax.getAgency', compact('agencies'));
    }
}
