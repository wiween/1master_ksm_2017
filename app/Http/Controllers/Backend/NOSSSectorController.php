<?php

namespace App\Http\Controllers\Backend;

use App\NOSSSector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lookup;

class NOSSSectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        return view('backend.lookups_management.field', compact('statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $statuses = Lookup::where('name', 'user_status')->where('status', 'active')->orderBy('value', 'asc')->get();
        return view('backend.lookups_management.createnoss', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NOSSSector  $nOSSSector
     * @return \Illuminate\Http\Response
     */
    public function show(NOSSSector $nOSSSector)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NOSSSector  $nOSSSector
     * @return \Illuminate\Http\Response
     */
    public function edit(NOSSSector $nOSSSector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NOSSSector  $nOSSSector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NOSSSector $nOSSSector)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NOSSSector  $nOSSSector
     * @return \Illuminate\Http\Response
     */
    public function destroy(NOSSSector $nOSSSector)
    {
        //
    }
}
