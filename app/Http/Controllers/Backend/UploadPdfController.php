<?php

namespace App\Http\Controllers\Backend;

use App\UploadPdf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadPdfRequest;
use Illuminate\Support\Facades\Auth;

class UploadPdfController extends Controller
{
    //
    public function storePDF(UploadPdfRequest $request)
    {
        $uploadpdf = new UploadPdf();
        $uploadpdf->user_id = $request->input('user_id');
        $uploadpdf->remark = $request->input('remark');

        $uploadpdf->uploaded_by = Auth::id();

        // Untuk upload gambar avatar
        if (isset($request->filepdf)) {
            if ($request->file('filepdf')->isValid()) {
                $destinationPath = "uploadpdf/";
                $extension = $request->file('filepdf')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('filepdf')->move($destinationPath, $fileName);

                $uploadpdf->filepdf = '/' . $destinationPath . $fileName;
            }
        }

        if ($uploadpdf->save()) {
            return redirect('admin/psychometric/pdf')->with('successMessage', 'File has been successfully uploaded');
        } else {
            return back()->with('errorMessage', 'Unable to upload file. Contact admin');
        }

    }

    public function showPdf()
    {
        $uploads = UploadPdf::all();
        return view('backend.psychometric.pdf.index', compact('uploads'));
    }


}
