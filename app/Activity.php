<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Activity
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property string $time
 * @property string $venue
 * @property string $description
 * @property string $audience
 * @property string $ministry
 * @property string $avatar
 * @property int $participant
 * @property string $status
 * @property string $dirasmi_oleh
 * @property string $remark
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereAudience($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereDateEnd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereDirasmiOleh($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereMinistry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereParticipant($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereVenue($value)
 * @mixin \Eloquent
 * @property string $officiated_by
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereOfficiatedBy($value)
 * @property int $hit
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereHit($value)
 * @property string $image
 * @property int $organization_id
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereOrganizationId($value)
 */
class Activity extends Model
{
    //
    protected $dates = ['date_start','date_end'];

    public static function mostPopularActivity($limit)
    {
        $mostPopularActivities = Activity::where('status', 'published')->orderBy('hit', 'desc')->take($limit)->get();
        return $mostPopularActivities;
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
