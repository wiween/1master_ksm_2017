<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Visit
 *
 * @property int $id
 * @property int $organization_id
 * @property string $year
 * @property string $month
 * @property int $hit
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereHit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Visit whereYear($value)
 * @mixin \Eloquent
 */
class Visit extends Model
{
    //
    //public $table = "visits";
   // use SoftDeletes;

    public function organization() {
        return $this->belongsTo(Organization::class);
    }


}
