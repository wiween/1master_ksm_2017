<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SubField
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $field_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereFieldId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SubField whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubField extends Model
{
    public static function getValue($id)
    {
        $sub_field = SubField::find($id);
        $value = $sub_field->name;

        return $value;
    }

    public static function getValue1($id)
    {
        $sub_field = SubField::find($id);
        $value = $sub_field->field_id;

        return $value;
    }

}
