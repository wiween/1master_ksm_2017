<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property int $id
 * @property string $name
 * @property string $noss_sector
 * @property string $noss_subsector
 * @property string $ministry
 * @property string $venue
 * @property string $state
 * @property string $avatar
 * @property string $course_type
 * @property string $timeframe
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property string $fee
 * @property string $audience
 * @property string $age
 * @property int $total_participant
 * @property string $description
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAudience($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereCourseType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDateEnd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereFee($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereMinistry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereNossSector($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereNossSubsector($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereTimeframe($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereTotalParticipant($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereVenue($value)
 * @mixin \Eloquent
 * @property string $field
 * @property string $introduction
 * @property string $job
 * @property string $salary
 * @property string $learning
 * @property string $contact
 * @property string $duration
 * @property string $audience1
 * @property string $audience2
 * @property string $audience3
 * @property string $audience4
 * @property int $visit
 * @property int $student
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAudience1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAudience2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAudience3($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereAudience4($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereContact($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDuration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereField($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereIntroduction($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereJob($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereLearning($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereSalary($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereStudent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereVisit($value)
 * @property int $hit
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereHit($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Session[] $monthlyreport
 * @property string $syllabus
 * @property string $image
 * @property int $organization_id
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereSyllabus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Session[] $session
 * @property int $sub_field_id
 * @property-read \App\Lookup $lookup
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereSubFieldId($value)
 */
class Course extends Model
{
    //
    protected $dates = ['date_start','date_end'];

    public static function mostPopularCourse($limit)
    {
        $mostPopularCourses = Course::where('status', 'published')->orderBy('hit', 'desc')->take($limit)->get();
        return $mostPopularCourses;
    }

    public function session()
    {
        return $this->hasMany('App\Session');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function lookup()
    {
        return $this->belongsTo(Lookup::class);
    }

    public static function getValue($id)
    {
        $course = Course::find($id);
        $value = $course->name;

        return $value;
    }

    public static function getValue1($id)
    {
        $course = Course::find($id);
        $value = $course->image;

        return $value;
    }

    public static function getValue2($id)
    {
        $course = Course::find($id);
        $value = $course->organization_id;

        return $value;
    }
}
