<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Audience
 *
 * @property int $id
 * @property string $name
 * @property string $nick
 * @property string $status
 * @property string $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audience whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Audience extends Model
{
    //
}
