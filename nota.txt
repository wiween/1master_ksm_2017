
NAMING CONVENTION
*****************
table : users
model : User
controller : UserController
request : UserRequest
seeder : UserTableSeeder

table : audit_trails
model : AuditTrail
controller : AuditTrailController
request : AuditTrailRequest
seeder : AuditTrailTableSeeder


COMMAND ARTISAN
****************
php artisan make:model AuditTrail -mcr
    php artisan make:controller UserController
    php artisan make:controller UserController -r
php artisan db:seed
php artisan migrate
    php artisan migrate:refresh
    php artisan migrate:refresh --seed
php artisan ide-helper:models
php artisan make:middleware AuditTrailMiddleware



COMMAND COMPOSER
*****************
composer dump-autoload
composer update
composer install
composer require NamaPakej


AUDIT TRAIL:
*************

id as PK
ip (varchar aka string)
url (varchar)
flag (varchar) :: default value is --> normal
note (text) :: nullable
user_id (int, unsigned) :: FK kepada table user, column ID

nak pakai softdelete


