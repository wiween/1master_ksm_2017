<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// *************  FRONT END  ***********
// handle login, logout
Auth::routes();
Route::get('/', 'Frontend\HomeController@index');
Route::get('profile/about-us', 'Frontend\HomeController@about');
Route::get('profile/faq', 'Frontend\FaqController@index');
//Route::get('profile/search', 'Frontend\HomeController@search');
Route::get('activity', 'Frontend\ActivityController@index');
Route::get('activity/show/{id}', 'Frontend\ActivityController@show');
Route::get('activity/gallery', 'Frontend\GalleryController@index');
Route::post('activity/gallery', 'Frontend\GalleryController@filter');
Route::post('activity/search', 'Frontend\ActivityController@search');

//course - simon
Route::get('course/short', 'Frontend\CourseController@short');
Route::get('course/show/{id}', 'Frontend\CourseController@show');
Route::post('course/short', 'Frontend\CourseController@shortFilter');
Route::post('course/short/search', 'Frontend\CourseController@shortSearch');
Route::get('course/long', 'Frontend\CourseController@long');
Route::get('course/long_more/{id}', 'Frontend\CourseController@longMore');
Route::post('course/long', 'Frontend\CourseController@longFilter');
Route::get('course/venue', 'Frontend\CourseController@venue');
Route::post('course/venue/search', 'Frontend\CourseController@venueSearch');
Route::get('course/venue_more/{id}', 'Frontend\CourseController@venueMore');
Route::post('course/venue', 'Frontend\CourseController@filter');
Route::get('course/sponsorship', 'Frontend\CourseController@sponsorship');

//testimonial
Route::get('testimonial', 'Frontend\TestimonialController@index');

//psychometric
Route::get('psychometric', 'Frontend\PsychometricController@register');



//search - awin
//Route::post('profile/search/{search}', 'Frontend\SearchResultController@show');

//contact us - awin
Route::get('profile/contact-us', 'Frontend\ContactUsController@index');
Route::post('profile/contact-us', 'Frontend\ContactUsController@store');
Route::post('/contact-us', 'Frontend\ContactUsController@store');

// FRONTEND but for AUTHENTICATED USER only
Route::group(['middleware' => ['audit', 'role:user']], function () {
    // Psychometric
    Route::get('psychometric/profile', 'Frontend\PsychometricController@profile');
//    Route::get('/psychometric/psychometric_test', 'Frontend\PsychometricController@psychometricTest');
    Route::get('/psychometric/edit-profile', 'Frontend\PsychometricController@editProfile');
    Route::post('/psychometric/edit-profile', 'Frontend\PsychometricController@updateProfile');
    Route::get('/psychometric/changePassword', 'Frontend\PsychometricController@changePassword');
    Route::post('/psychometric/changePassword', 'Frontend\PsychometricController@updatePassword');
    Route::get('/psychometric/enroll', 'Frontend\PsychometricController@enroll');
});



// *************  BACKEND  *************
// USER and above
Route::get('logout', 'Auth\LoginController@logout');
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:user']], function () {
    // Dashboard
    Route::get('/dashboard', 'Backend\DashboardController@index');
    // User
    Route::get('/user/profile', 'Backend\UserController@profile');
    Route::get('/user/edit-profile', 'Backend\UserController@editProfile');
    Route::post('/user/edit-profile', 'Backend\UserController@updateProfile');
    Route::get('/user/change-password', 'Backend\UserController@changePassword');
    Route::post('/user/change-password', 'Backend\UserController@updatePassword');

    Route::get('/user/chart/all', 'Backend\UserController@chartAllUsers');
});

// EXAMINER
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:examiner']], function () {
    // psychometric Result
    Route::get('/psychometric/form', 'Backend\PsychometricController@index');
    Route::get('/psychometric/form/create', 'Backend\PsychometricController@create');
    Route::post('/psychometric/form/create', 'Backend\PsychometricController@store');
    Route::get('/psychometric/form/show/{id}', 'Backend\PsychometricController@show');
    Route::get('/psychometric/form/edit/{id}', 'Backend\PsychometricController@edit');
    Route::post('/psychometric/form/edit/{id}', 'Backend\PsychometricController@update');
    Route::get('/psychometric/form/destroy/{id}', 'Backend\PsychometricController@destroy');
    Route::get('/psychometric/pdf', 'Backend\UploadPdfController@showPdf');
    Route::get('/psychometric/pdf/create', 'Backend\PsychometricController@uploadPDF');
    Route::post('/psychometric/pdf/create', 'Backend\UploadPdfController@storePDF');
    Route::get('/psychometric/upload-excel', 'Backend\PsychometricController@uploadexcel');
    Route::post('/psychometric/upload-excel', 'Backend\PsychometricController@storeExcel');

});

// ADMIN BRANCH
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:admin_branch']], function () {

});

// ADMIN DEPARTMENT
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:admin_department']], function () {

});

// ADMIN MINISTRY
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:admin_ministry']], function () {

    // Activity
    Route::get('/activity', 'Backend\ActivityController@index');
    Route::get('/activity/create', 'Backend\ActivityController@create');
    Route::post('/activity/create', 'Backend\ActivityController@store');
    Route::get('/activity/show/{id}', 'Backend\ActivityController@show');
    Route::get('/activity/edit/{id}', 'Backend\ActivityController@edit');
    Route::post('/activity/edit/{id}', 'Backend\ActivityController@update');
    Route::get('/activity/destroy/{id}', 'Backend\ActivityController@destroy');
    Route::get('/activity/set/{flag}/{id}', 'Backend\ActivityController@setFlag');
    Route::get('/activity/ajax/get-dept/{ministryId}', 'Backend\ActivityController@ajaxLoadDept');


    // course
    Route::get('/course', 'Backend\CourseController@index');
    Route::get('/course/create', 'Backend\CourseController@create');
    Route::post('/course/create', 'Backend\CourseController@store');
    Route::get('/course/show/{id}', 'Backend\CourseController@show');
    Route::get('/course/edit/{id}', 'Backend\CourseController@edit');
    Route::post('/course/edit/{id}', 'Backend\CourseController@update');
    Route::get('/course/destroy/{id}', 'Backend\CourseController@destroy');
    Route::get('/course/ajax/get-dept/{ministryId}', 'Backend\CourseController@ajaxLoadDept');
    Route::get('/course/ajax/get-agency/{agencyId}', 'Backend\CourseController@ajaxLoadAgency');
    Route::get('/course/ajax/get-sub-field/{subFieldId}', 'Backend\CourseController@ajaxLoadSubField');

    // session
    Route::get('/session', 'Backend\SessionController@index');
    Route::get('/session/create', 'Backend\SessionController@create');
    Route::post('/session/create', 'Backend\SessionController@store');
    Route::get('/session/show/{id}', 'Backend\SessionController@show');
    Route::get('/session/edit/{id}', 'Backend\SessionController@edit');
    Route::post('/session/edit/{id}', 'Backend\SessionController@update');
    Route::get('/session/destroy/{id}', 'Backend\SessionController@destroy');
    Route::get('/session/ajax/get-dept/{ministryId}', 'Backend\SessionController@ajaxLoadDept');
    Route::get('/session/ajax/get-agency/{agencyId}', 'Backend\SessionController@ajaxLoadAgency');

// monthlyreport
    Route::get('/monthlyreport', 'Backend\SessionController@monthlyReport');
    Route::get('/monthlyreport/create', 'Backend\SessionController@monthlyReportCreate');
    Route::post('/monthlyreport/create', 'Backend\SessionController@monthlyReportStore');
    Route::get('/monthlyreport/show/{id}', 'Backend\SessionController@monthlyReportShow');
    Route::get('/monthlyreport/edit/{id}', 'Backend\SessionController@monthlyReportEdit');
    Route::post('/monthlyreport/edit/{id}', 'Backend\SessionController@monthlyReportUpdate');
    Route::get('/monthlyreport/destroy/{id}', 'Backend\SessionController@monthlyReportDestroy');
    //Route::get('/monthlyreport/ajax/get-dept/{ministryId}', 'Backend\MonthlyReportController@ajaxLoadDept');
    //Route::get('/monthlyreport/ajax/get-agency/{agencyId}', 'Backend\MonthlyReportController@ajaxLoadAgency');
    Route::get('/monthlyreport/ajax/get-course/{courseId}', 'Backend\SessionController@ajaxLoadCourse');
    Route::get('/monthlyreport/ajax/get-session/{sessionId}', 'Backend\SessionController@ajaxLoadSession');

    //contact us
    Route::get('/contact-us', 'Backend\ContactUsController@index');
    Route::get('/contact-us/set/{flag}/{id}', 'Backend\ContactUsController@setFlag');
    Route::get('/contact-us/show/{id}', 'Backend\ContactUsController@show');
    Route::get('/contact-us/destroy/{id}', 'Backend\ContactUsController@destroy');

    //gallery
    Route::get('/gallery', 'Backend\GalleryController@index');
    Route::get('/gallery/create', 'Backend\GalleryController@create');
    Route::post('/gallery/create', 'Backend\GalleryController@store');
    Route::get('/gallery/edit/{id}', 'Backend\GalleryController@edit');
    Route::post('/gallery/edit/{id}', 'Backend\GalleryController@update');
    Route::get('/gallery/set/{flag}/{id}', 'Backend\GalleryController@setFlag');
    Route::get('/gallery/show/{id}', 'Backend\GalleryController@show');
    Route::get('/gallery/destroy/{id}', 'Backend\GalleryController@destroy');

    //slider
    Route::get('/slider', 'Backend\SliderController@index');
    Route::get('/slider/create', 'Backend\SliderController@create');
    Route::post('/slider/create', 'Backend\SliderController@store');
    Route::get('/slider/edit/{id}', 'Backend\SliderController@edit');
    Route::post('/slider/edit/{id}', 'Backend\SliderController@update');
    Route::get('/slider/set/{flag}/{id}', 'Backend\SliderController@setFlag');
    Route::get('/slider/show/{id}', 'Backend\GalleryController@show');
    Route::get('/slider/destroy/{id}', 'Backend\GalleryController@destroy');

    //target
    Route::get('/target', 'Backend\TargetController@index');
    Route::get('/target/create', 'Backend\TargetController@create');
    Route::post('/target/create', 'Backend\TargetController@store');
    Route::get('/target/edit/{id}', 'Backend\TargetController@edit');
    Route::post('/target/edit/{id}', 'Backend\TargetController@update');
    Route::get('/target/destroy/{id}', 'Backend\TargetController@destroy');
    Route::get('/target/ajax/get-dept/{ministryId}', 'Backend\TargetController@ajaxLoadDept');
    Route::get('/target/ajax/get-agency/{agencyId}', 'Backend\TargetController@ajaxLoadAgency');

    //visit
    Route::get('/visit', 'Backend\VisitController@index');
    Route::get('/visit/create', 'Backend\VisitController@create');
    Route::post('/visit/create', 'Backend\VisitController@store');
    Route::get('/visit/edit/{id}', 'Backend\VisitController@edit');
    Route::post('/visit/edit/{id}', 'Backend\VisitController@update');
    Route::get('/visit/destroy/{id}', 'Backend\VisitController@destroy');

    // faq
    Route::get('/faq', 'Backend\FaqController@index');
    Route::get('/faq/create', 'Backend\FaqController@create');
    Route::post('/faq/create', 'Backend\FaqController@store');
    Route::get('/faq/show/{id}', 'Backend\FaqController@show');
    Route::get('/faq/edit/{id}', 'Backend\FaqController@edit');
    Route::post('/faq/edit/{id}', 'Backend\FaqController@update');
    Route::get('/faq/destroy/{id}', 'Backend\FaqController@destroy');
    Route::get('/faq/set/{flag}/{id}', 'Backend\FaqController@setFlag');
});

// ADMIN (PANTAU)
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:admin']], function () {

    // Lookup Management - Agency
    Route::get('/lookups/agency', 'Backend\AgencyController@index');
    Route::get('/lookups/agency/create', 'Backend\AgencyController@create');
    Route::post('/lookups/agency/create', 'Backend\AgencyController@store');
    Route::get('/lookups/agency/show/{id}', 'Backend\AgencyController@show');
    Route::get('/lookups/agency/edit/{id}', 'Backend\AgencyController@edit');
    Route::post('/lookups/agency/edit/{id}', 'Backend\AgencyController@update');
    Route::get('/lookups/agency/destroy/{id}', 'Backend\AgencyController@destroy');

    // Lookup Management - Department
    Route::get('/lookups/department', 'Backend\DepartmentController@index');
    Route::get('/lookups/department/create', 'Backend\DepartmentController@create');
    Route::post('/lookups/department/create', 'Backend\DepartmentController@store');
    Route::get('/lookups/department/show/{id}', 'Backend\DepartmentController@show');
    Route::get('/lookups/department/edit/{id}', 'Backend\DepartmentController@edit');
    Route::post('/lookups/department/edit/{id}', 'Backend\DepartmentController@update');
    Route::get('/lookups/department/destroy/{id}', 'Backend\DepartmentController@destroy');

    // Lookup Management - Role
    Route::get('/lookups/role', 'Backend\RoleController@index');
    Route::get('/lookups/role/create', 'Backend\RoleController@create');
    Route::post('/lookups/role/create', 'Backend\RoleController@store');
    Route::get('/lookups/role/show/{id}', 'Backend\RoleController@show');
    Route::get('/lookups/role/edit/{id}', 'Backend\RoleController@edit');
    Route::post('/lookups/role/edit/{id}', 'Backend\RoleController@update');
    Route::get('/lookups/role/destroy/{id}', 'Backend\RoleController@destroy');

    // Lookup Management - State
    Route::get('/lookups/state', 'Backend\StateController@index');
    Route::get('lookups//state/create', 'Backend\StateController@create');
    Route::post('/lookups/state/create', 'Backend\StateController@store');
    Route::get('/lookups/state/show/{id}', 'Backend\StateController@show');
    Route::get('/lookups/state/edit/{id}', 'Backend\StateController@edit');
    Route::post('/lookups/state/edit/{id}', 'Backend\StateController@update');
    Route::get('/lookups/state/destroy/{id}', 'Backend\StateController@destroy');

    // Lookup Management - bidang
    Route::get('/lookups/field', 'Backend\NOSSSectorController@index');
    Route::get('/lookups/field/create', 'Backend\NOSSSectorController@create');
    Route::post('/lookups/field/create', 'Backend\NOSSSectorController@store');
    Route::get('/lookups/field/show/{id}', 'Backend\NOSSSectorController@show');
    Route::get('/lookups/field/edit/{id}', 'Backend\NOSSSectorController@edit');
    Route::post('/lookups/field/edit/{id}', 'Backend\NOSSSectorController@update');
    Route::get('/lookups/field/destroy/{id}', 'Backend\NOSSSectorController@destroy');

    // Lookup Management - Ministry
    Route::get('/lookups/ministry', 'Backend\MinistryController@index');
    Route::get('/lookups/ministry/create', 'Backend\MinistryController@create');
    Route::post('/lookups/ministry/create', 'Backend\MinistryController@store');
    Route::get('/lookups/ministry/show/{id}', 'Backend\MinistryController@show');
    Route::get('/lookups/ministry/edit/{id}', 'Backend\MinistryController@edit');
    Route::post('/lookups/ministry/edit/{id}', 'Backend\MinistryController@update');
    Route::get('/lookups/ministry/destroy/{id}', 'Backend\MinistryController@destroy');

    // Lookup Management - Audience
    Route::get('/lookups/audience', 'Backend\AudienceController@index');
    Route::get('/lookups/audience/create', 'Backend\AudienceController@create');
    Route::post('/lookups/audience/create', 'Backend\AudienceController@store');
    Route::get('/lookups/audience/show/{id}', 'Backend\AudienceController@show');
    Route::get('/lookups/audience/edit/{id}', 'Backend\AudienceController@edit');
    Route::post('/lookups/audience/edit/{id}', 'Backend\AudienceController@update');
    Route::get('/lookups/audience/destroy/{id}', 'Backend\AudienceController@destroy');

    // Lookup Management - category
    Route::get('/lookups/category', 'Backend\CategoryController@index');
    Route::get('/lookups/category/create', 'Backend\CategoryController@create');
    Route::post('/lookups/category/create', 'Backend\CategoryController@store');
    Route::get('/lookups/category/show/{id}', 'Backend\CategoryController@show');
    Route::get('/lookups/category/edit/{id}', 'Backend\CategoryController@edit');
    Route::post('/lookups/category/edit/{id}', 'Backend\CategoryController@update');
    Route::get('/lookups/  category/destroy/{id}', 'Backend\CategoryController@destroy');

    // ONLY ADMIN AND SUPER ADMIN CAN DELETE
    Route::get('/session/destroy/{id}', 'Backend\SessionController@destroy');

    //chart
    Route::get('/report/chart', 'Backend\ReportController@chartAllUsers');
    Route::get('/report/statistik', 'Backend\ReportController@indexPdf');
    Route::get('/report/statistik/pdf', 'Backend\ReportController@reportPdf');
});

// SUPER ADMIN ONLY
Route::group(['prefix' => 'admin', 'middleware' => ['audit', 'role:super_admin']], function () {
    // User
    Route::get('/user', 'Backend\UserController@index');
    Route::get('/user/create', 'Backend\UserController@create');
    Route::post('/user/create', 'Backend\UserController@store');
    Route::get('/user/show/{id}', 'Backend\UserController@show');
    Route::get('/user/edit/{id}', 'Backend\UserController@edit');
    Route::post('/user/edit/{id}', 'Backend\UserController@update');
    Route::get('/user/reset-password/{id}', 'Backend\UserController@resetPassword');
    Route::get('/user/destroy/{id}', 'Backend\UserController@destroy');
    Route::get('/user/reset-password/{id}', 'Backend\UserController@resetPassword');

    // Audit
    Route::get('/audit-trail', 'Backend\AuditTrailController@index');
    Route::get('/audit-trail/set/{flag}/{id}', 'Backend\AuditTrailController@setFlag');
    Route::get('/audit-trail/show/{id}', 'Backend\AuditTrailController@show');
    Route::post('/audit-trail/update/{id}', 'Backend\AuditTrailController@update');
    Route::get('/audit-trail/destroy/{id}', 'Backend\AuditTrailController@destroy');




});







//Route::get('/home', 'HomeController@index')->name('home');






