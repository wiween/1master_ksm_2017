<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home - 1Master</title>

    <!-- PLUGINS CSS STYLE -->
    <link href="/themes/kidz/plugins/jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="/themes/kidz/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/kidz/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/themes/kidz/plugins/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/themes/kidz/plugins/selectbox/select_option1.css">
    <link rel="stylesheet" type="text/css" href="/themes/kidz/plugins/owl-carousel/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/themes/kidz/plugins/isotope/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/themes/kidz/plugins/isotope/isotope.css">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Dosis:400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- CUSTOM CSS -->
    <link href="/themes/kidz/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/themes/kidz/css/default.css" id="option_color">

    <!-- Icons -->
    {{--<link rel="shortcut icon" href="/themes/kidz/img/favicon.png">--}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    @include('partials.touchicon')
</head>

<body class="body-wrapper">


<div class="main-wrapper">
    <!-- HEADER -->
    <header id="pageTop" class="header-wrapper">
        <!-- COLOR BAR -->
        <div class="container-fluid color-bar top-fixed clearfix">
            <div class="row">
                <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
                <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
            </div>
        </div>

        <!-- TOP INFO BAR -->
        <div class="top-info-bar bg-color-7 hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <ul class="list-inline topList">
                            <li><i class="fa fa-envelope bg-color-1" aria-hidden="true"></i> <a href="mailto:1master@mohr.gov.my">1master@mohr.gov.my</a></li>
                            <li><i class="fa fa-phone bg-color-2" aria-hidden="true"></i> <a href="tel:+603 8888 5000">+603 8888 5000</a></li>
                            <!--<li><i class="fa fa-clock-o bg-color-6" aria-hidden="true"></i> Open: 9am - 6pm</li>-->
                        </ul>
                    </div>
                    <div class="col-sm-5">
                        <ul class="list-inline functionList">
                            <li><i class="fa fa-globe bg-color-4" aria-hidden="true"></i></li>
                            <li class="LanguageList">
                                <select name="guiest_id1" id="guiest_id1" class="select-drop">
                                    <option value="0"><i class="fa fa-globe bg-color-4" aria-hidden="true"></i> Language</option>
                                    <option value="1">English</option>
                                    <option value="2">B.Melayu</option>
                                </select>
                            </li>
                            @if (Auth::check())
                                <span>Hello {{ strtok(Auth::user()->name, " ") }} |
                                    @if (Auth::user()->role <> "user")
                                    <a href="/admin/dashboard/" class="color-1">
                                    <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        Dashboard </a> |
                                    @endif
                                    <a href="/logout" class="color-1">
                                    Logout <i class="fa fa-sign-out" aria-hidden="true"></i>
                                </a>
                                </span>
                                    @else
                                <li>
                                    <i class="fa fa-unlock-alt bg-color-5" aria-hidden="true"></i>
                                    <a href='/login' data-toggle="modal" >Login</a>
                                    <span>or</span><a href='/register' data-toggle="modal">Register</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- NAVBAR -->
        @include('partials.frontend_navbar')
    </header>

    @yield('content')

    <!-- FOOTER -->
    <footer>
        <!-- COLOR BAR -->
        <div class="container-fluid color-bar clearfix">
            <div class="row">
                <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
                <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
                <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
                <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
            </div>
        </div>
        <!-- FOOTER INFO AREA -->
        <div class="footerInfoArea full-width clearfix" style="background-image: url(/themes/kidz/img/footer/footer-bg-1.png);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="footerTitle">
                            <a href="index.html"><img src="/images/frontend/logo/1master_logo_blackwhite.png" style="width: 200px"></a>
                        </div>
                        <div class="footerInfo">
                            <p>1Malaysia Skill Training Enhancement for the Rakyat.</p>
                            <p>A program specially implemented under the National Blue Ocean Strategy (NBOS) to offer practical training in industry-demanded skills</p>
                        </div>
                        <hr>
                        <div>
                           <address style="color: #ffffcc">
                               <strong>1Master by Ministry of Human Resource, Malaysia</strong><br>
                              Persiaran Sultan Sallahuddin Abdul Aziz Shah, Presint 1, 62000 Putrajaya<br>
                                <a href="mailto:1master@mohr.gov.my">1master@mohr.gov.my</a><br>
                                03 8885 5000
                           </address>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="footerTitle">
                            <h4>Useful Links</h4>
                        </div>
                        <div class="footerInfo">
                            <ul class="list-unstyled footerList">
                                <li>
                                    <a target="_blank" href="http://www.skillsmalaysia.gov.my/">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>SkillMalaysia
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.kbs.gov.my/en/web-personalization-fix/79-skill.html">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>Youth Program
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.ciast.gov.my/">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>Ciast
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.talentcorp.com.my/">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>Talent Development
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.eknowledge.com.my/promotions/upskiling/index.html">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>Knowledgecom
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="footerTitle">
                            <h4>Popular Activity</h4>
                        </div>
                        <div class="footerInfo">
                            <ul class="list-unstyled postLink">
                                @foreach(\App\Activity::mostPopularActivity(4) as $activity)
                                <li>
                                    <div class="media">
                                        <a class="media-left" href="single-blog.html">
                                            <img class="media-object img-rounded border-color-1" src="{{ $activity->image }}" alt="Image">
                                        </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"><a href="#">{{ $activity->name }}</a></h5>
                                            <p>{{ $activity->date_start->format('d M Y') }}</p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity2.jpeg" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="single-blog-left-sidebar.html">Bootcamp By Talent</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity3.png" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="single-blog-left-sidebar.html">Career Fair 2017</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity3.png" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="single-blog-left-sidebar.html">Career Fair 2017</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="footerTitle">
                            <h4>Popular Course</h4>
                        </div>
                        <div class="footerInfo">
                            <ul class="list-unstyled postLink">
                                @foreach(\App\Course::mostPopularCourse(4) as $course)
                                <li>
                                    <div class="media">
                                        <a class="media-left" href="single-blog.html">
                                            <img class="media-object img-rounded border-color-1" src="{{ $course->avatar }}" alt="Image">
                                        </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"><a href="#">{{ $course->name }}</a></h5>
                                        </div>
                                    </div>
                                </li>
                                @endforeach

                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity3.png" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="#">Career Fair 2017</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity2.jpeg" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="#">Bootcamp By Talent</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="media">--}}
                                        {{--<a class="media-left" href="single-blog-left-sidebar.html">--}}
                                            {{--<img class="media-object img-rounded border-color-1" src="/images/frontend/activity/activity2.jpeg" alt="Image">--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h5 class="media-heading"><a href="#">Bootcamp By Talent</a></h5>--}}
                                            {{--<p>July 7 - 2016</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- COPY RIGHT -->
        <div class="copyRight clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-sm-push-7 col-xs-12">
                        <ul class="list-inline">
                            <li><a href="#" class="bg-color-1"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="bg-color-2"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="bg-color-3"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="bg-color-4"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="bg-color-5"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-7 col-sm-pull-5 col-xs-12">
                        <div class="copyRightText">
                            <p>© {{ date('Y') }} Copyright 1Master by <a href="https://www.mohr.gov.my">MOHR, Malaysia</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="scrolling">
    <a href="#pageTop" class="backToTop hidden-xs" id="backToTop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
</div>

<!-- LOGIN MODAL -->
<div class="modal fade customModal" id="loginModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default formPanel">
                <div class="panel-heading bg-color-1 border-color-1">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <form action="#" method="POST" role="form">
                        <div class="form-group formField">
                            <input type="text" class="form-control" placeholder="User name">
                        </div>
                        <div class="form-group formField">
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group formField">
                            <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Log in">
                        </div>
                        <div class="form-group formField">
                            <p class="help-block"><a href="#">Forgot password?</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CREATE ACCOUNT MODAL -->
<div class="modal fade customModal" id="createAccount" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default formPanel">
                <div class="panel-heading bg-color-1 border-color-1">
                    <h3 class="panel-title">Create  an account</h3>
                </div>
                <div class="panel-body">
                    <form action="#" method="POST" role="form">
                        <div class="form-group formField">
                            <input type="text" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group formField">
                            <input type="text" class="form-control" placeholder="User name">
                        </div>
                        <div class="form-group formField">
                            <input type="text" class="form-control" placeholder="Phone">
                        </div>
                        <div class="form-group formField">
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group formField">
                            <input type="password" class="form-control" placeholder="Re-Password">
                        </div>
                        <div class="form-group formField">
                            <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Register">
                        </div>
                        <div class="form-group formField">
                            <p class="help-block">Already have an account? <a href="#">Log in</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/themes/kidz/plugins/jquery-ui/jquery-ui.js"></script>
<script src="/themes/kidz/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/themes/kidz/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/themes/kidz/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/themes/kidz/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="/themes/kidz/plugins/owl-carousel/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="/themes/kidz/plugins/counter-up/jquery.counterup.min.js"></script>
<script src="/themes/kidz/plugins/isotope/isotope.min.js"></script>
<script src="/themes/kidz/plugins/isotope/jquery.fancybox.pack.js"></script>
<script src="/themes/kidz/plugins/isotope/isotope-triger.js"></script>
<script src="/themes/kidz/plugins/countdown/jquery.syotimer.js"></script>
<script src="/themes/kidz/plugins/velocity/velocity.min.js"></script>
<script src="/themes/kidz/plugins/smoothscroll/SmoothScroll.js"></script>
<script src="/themes/kidz/js/custom.js"></script>
</body>
</html>

