<nav id="menuBar" class="navbar navbar-default lightHeader" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/images/frontend/logo/1master_logo.png" alt="1Master" style="width: 350px; margin-top: -8px">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown singleDrop color-1 @if (Request::segment(1) == '') active @endif">
                    <a href="/" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-home bg-color-1" aria-hidden="true"></i> <span>Home</span>
                    </a>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'profile') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-ul bg-color-3" aria-hidden="true"></i>
                        <span>About Us</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li class=""><a href="/profile/about-us">1Master</a></li>
                        {{--<li class=""><a href="/profile/search">Search</a></li>--}}
                        <li class=""><a href="/profile/contact-us">Contact Us</a></li>
                        <li class=""><a href="/profile/faq">FAQ</a></li>
                    </ul>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'activity') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calendar bg-color-3" aria-hidden="true"></i>
                        <span>Activity</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class=""><a href="/activity">All Activities</a></li>
                        <li class=""><a href="/activity/gallery">Photo Gallery</a></li>
                    </ul>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'course') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calendar bg-color-3" aria-hidden="true"></i>
                        <span>Courses</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class=""><a href="/course/short">Short Courses</a></li>
                        <!--<li class=""><a href="/course/long">Long Courses</a></li>-->
                        <li class=""><a href="/course/venue">Course Venue</a></li>
                        <li class=""><a href="/course/sponsorship">Sponsorship</a></li>
                    </ul>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'audience') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-pencil-square-o bg-color-3" aria-hidden="true"></i>
                        <span>Audience</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class=""><a href="/#student">Student</a></li>
                        <li class=""><a href="/#employer">Employer</a></li>
                        <li class=""><a href="/#jobseeker">Working Adult</a></li>
                        <li class=""><a href="/#working-adult">Pensioner</a></li>
                    </ul>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'testimonial') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calendar bg-color-3" aria-hidden="true"></i>
                        <span>Testimonial</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class=""><a href="/testimonial">Success Story</a></li>
                    </ul>
                </li>
                <li class="dropdown singleDrop color-3 @if (Request::segment(1) == 'psychometric') active @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false"><i class="fa fa-gg bg-color-3" aria-hidden="true"></i>
                        <span>Psychometric</span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        @if (Auth::user())
                            <li class=""><a href="/psychometric/profile">My Profile</a></li>
                            <li class=""><a href="/psychometric/changePassword">Change Password</a></li>
                            <li class=""><a href="/logout">Logout</a></li>
                        @else
                            <li class=""><a href="/psychometric">Register</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
