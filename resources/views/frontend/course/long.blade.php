@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Long Course'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset>
                    <aside>
                            <div class="panel panel-default courseSidebar">
                            <div class="panel-heading bg-color-2 border-color-2">
                                <h3 class="panel-title">Filter By</h3>
                            </div>
                            <div class="panel-body">
                                {{--NOSS Sector--}}
                                <div class="lightDrop{{$errors->has('noss_sector') ? 'has-error' : ''}}">
                                    <select name="noss_sector" class="select-drop">
                                        <option selected>All Available Sector</option>
                                        @foreach ($noss_sectors as $noss_sector)
                                            <option @if (old('noss_sector')== $noss_sector->noss_sector) selected @endif value="{{$noss_sector->noss_sector}}">{{$noss_sector->noss_sector}}</option>
                                        @endforeach
                                    </select>
                                    @include('partials.error_block',['item'=> 'noss_sector'])
                                </div>

                                {{--Audience--}}
                                <div class="lightDrop{{$errors->has('audience') ? 'has-error' : ''}}">
                                    <select name="audience" class="select-drop">
                                        <option selected>Targeted Participant</option>
                                        @foreach ($audiences as $audience)
                                            <option @if (old('audience')== $audience->audience) selected @endif value="{{$audience->audience}}">{{str_replace('_', ' ', $audience->audience)}}</option>
                                        @endforeach
                                    </select>
                                    @include('partials.error_block',['item'=> 'audience'])
                                </div>

                                <div class="priceRange">
                                    <input class="btn btn-primary bg-color-2" type="submit" value="Filter">
                                </div>
                            </div>
                        </div>
                        <div class="coursesCounter">
                            @foreach($countdowns as $countdown)
                                <div class="counterInner">
                                    <h3 class="color-3">Next Course Is In</h3>
                                    <h3>{{$countdown->date_start->format('d M, Y')}}</h3>
                                    <h3>({{ $countdown->date_start->diffForHumans() }})</h3>
                                </div>
                            @endforeach
                        </div>
                    </aside>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-12">

                    @foreach($courses as $course)
                    <div class="media courseList couresListPage">
                        <a class="media-left border-color-1" href="/course/long_more/{{$course->id}}">
                            <img class="media-object" src="{{$course->avatar}}" alt="Image">
                            <!--<span class="sticker bg-color-1">$50</span>-->
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading"><a href="/course/long_more/{{$course->id}}" class="color-1">{{$course->name}} <h5 class="color-6">({{$course->ministry}})</h5></a></h3>
                            <ul class="list-inline">
                                <li><i class="fa fa-calendar-o" aria-hidden="true"></i>{{$course->age}} Years Old</li>
                                <li><i class="fa fa-clock-o" aria-hidden="true"></i>{{$course->date_start->format('d M, Y')}} to {{$course->date_end->format('d M, Y')}}</li>
                            </ul>
                            <p>Course Sector : {{$course->noss_sector}}</p>
                            <p class="text-capitalize">Targeted Participant : {{str_replace('_', ' ', $course->audience)}}</p>
                            <ul class="list-inline btn-yellow btnPart">
                                <!--<li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>-->
                                <li><a href="/course/long_more/{{$course->id}}" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                            </ul>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

            <div class="pagerArea text-center">
                <ul class="pager">
                    <li class="prev"><a href="#"><i class="fa fa-arrow-left" aria-hidden="true"></i>Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">15</a></li>
                    <li class="next"><a href="#">Next<i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

@endsection