@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Short Course'])

    <section class="mainContent full-width clearfix courseSingleSection">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12 block pull-right">
                    <div class="thumbnail thumbnailContent alt">
                        <div><img src="{{$course->image}}" alt="image" height="470" width="870"></div>
                        <!--<div class="sticker bg-color-1">$50</div>-->
                        <div class="caption border-color-1">
                            <h3 class="color-1">{{ $course->name }}</h3>
                            <h4 class="color-6">Introduction</h4>
                            <p>{{ $course->introduction }}</p>
                            <br/>
                            <h4 class="color-6">What You Will Learn</h4>
                            <p>{{ $course->syllabus }}</p>
                            <br/>
                            <h4 class="color-6">Contact Person</h4>
                            <p><textarea class="form-control" rows="3" name="contact" disabled="">{{old('introduction', $course->contact)}}</textarea>
                                @include('partials.error_block',['item'=> 'contact'])</p>
                        </div>
                    </div>
                    <div class="btnArea">
                        <a href="/course/short" class="btn btn-primary">Back</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                    <aside>
                        <div class="rightSidebar">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-color-1 border-color-1">
                                    <h3 class="panel-title">Course Information</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-2">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-2">Course Duration</h4>
                                                <p>{{$course->duration}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-3">
                                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-3">Potential Job</h4>
                                                <p>{{$course->job}}</p>
                                            </div>
                                        </li>
                                        <li class="media iconContet">
                                            <div class="media-left iconContent bg-color-4">
                                                <i class="fa fa-money" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-4">Potential Salary</h4>
                                                <p>{{$course->salary}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-5">
                                                <i class="fa fa-university" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-5">Course Organizer</h4>
                                                <p>{{$course->organization->name}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-6">
                                                <i class="fa fa-leanpub" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-6">Course Field</h4>
                                                <p>{{ \App\Field::getValue(\App\SubField::getValue1($course->sub_field_id))}}</p>
                                                <p>[{{\App\SubField::getValue($course->sub_field_id)}}]</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="btnPart">
                                <a href="#" class="btn btn-primary btn-block bg-color-6">Enroll now</a>
                            </div>-->
                            <!--<div class="panel panel-default">
                                <div class="panel-heading bg-color-5 border-color-5">
                                    <h3 class="panel-title">Related Course</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list blogListing">
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#"><img src="img/course-single/related-course-01.jpg" alt="image" class="img-rounded"></a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#">Mauris semper mass feugiat facilisis.</a></h4>
                                                <p>July 7, 2016</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#"><img src="img/course-single/related-course-02.jpg" alt="image" class="img-rounded"></a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#">Mauris semper mass feugiat facilisis.</a></h4>
                                                <p>July 7, 2016</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#"><img src="img/course-single/related-course-03.jpg" alt="image" class="img-rounded"></a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#">Mauris semper mass feugiat facilisis.</a></h4>
                                                <p>July 7, 2016</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#"><img src="img/course-single/related-course-04.jpg" alt="image" class="img-rounded"></a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#">Mauris semper mass feugiat facilisis.</a></h4>
                                                <p>July 7, 2016</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>-->
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection