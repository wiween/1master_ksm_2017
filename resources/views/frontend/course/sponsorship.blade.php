@extends('layouts.frontend')


@section('content')

   @include('partials.page_title',['title'=>'Sponsorship'])
    <!-- CATEGORY SECTION -->
    <section class="mainContent full-width clearfix productSection categorySection">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <div class="box bg-color-1">
                        <div class="box-img border-color-1 text-center">
                            <a href="http://www.ptpk.gov.my" target="_blank">
                                <img src="/themes/kidz/img/sponsorship/logotabung.png" alt="image" class="img-responsive">
                            </a>
                        </div>
                        <div class="box-info">
                            <h4><a href="http://www.ptpk.gov.my" target="_blank">PTPK</a></h4>
                            <p>Skills Development Fund Corporation</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="box bg-color-2">
                        <div class="box-img border-color-2 text-center">
                            <a href="http://www.mara.gov.my" target="_blank">
                                <img src="/themes/kidz/img/sponsorship/mara.png" alt="image" class="img-responsive">
                            </a>
                        </div>
                        <div class="box-info">
                            <h4><a href="http://www.mara.gov.my" target="_blank">MARA</a></h4>
                            <p>Majlis Amanah Rakyat, <br>Malaysia</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="box bg-color-3">
                        <div class="box-img border-color-3 text-center">
                            <a href="http://www.mohe.gov.my" target="_blank">
                                <img src="/themes/kidz/img/sponsorship/jata.png" alt="image" class="img-responsive">
                            </a>
                        </div>
                        <div class="box-info">
                            <h4><a href="http://www.mohe.gov.my" target="_blank">MOHE</a></h4>
                            <p>Ministry of High Education, Malaysia</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="box bg-color-4">
                        <div class="box-img border-color-4 text-center">
                            <a href="http://www.hrdf.com.my" target="_blank">
                                <img src="/themes/kidz/img/sponsorship/hrdf.png" alt="image" class="img-responsive">
                            </a>
                        </div>
                        <div class="box-info">
                            <h4><a href="http://www.hrdf.com.my" target="_blank">HRDF</a></h4>
                            <p>Human Resources Development Fund</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection