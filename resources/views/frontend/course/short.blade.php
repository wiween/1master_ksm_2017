@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Short Course'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix coursesSection">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12 pull-right">
                    @if($courses->isEmpty())
                        No Courses found
                    @else
                    @foreach($courses as $course)
                        <div class="media courseList couresListPage">
                            <a class="media-left border-color-1" href="/course/short_more/{{$course->id}}">
                                <img class="media-object" src="{{$course->image}}" alt="Image">
                                <!--<span class="sticker bg-color-1">$50</span>-->
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading"><a href="/course/show/{{$course->id}}" class="color-1">{{$course->name}} <h5 class="color-6">({{$course->organization->name}})</h5></a></h3>
                                <ul class="list-inline">
                                    <li><i class="fa fa-leanpub" aria-hidden="true"></i>{{\App\SubField::getValue($course->sub_field_id)}}</li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>{{$course->duration}}</li>
                                </ul>
                                <p>Course Field : {{ \App\Field::getValue(\App\SubField::getValue1($course->sub_field_id))}}</p>
                                <p class="text-capitalize">Targeted Participant :
                                    @if ($course->audience1 <> '' && $course->audience2 == '' && $course->audience3 == '' && $course->audience4 == '')
                                        {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}
                                    @elseif ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 == '' && $course->audience4 == '')
                                        {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}
                                    @elseif ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 <> '' && $course->audience4 == '')
                                        {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience3))}}
                                    @else ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 <> '' && $course->audience4 <> '')
                                        {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience3))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience4))}}
                                    @endif
                                </p>
                                <ul class="list-inline btn-yellow btnPart">
                                    <!--<li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>-->
                                    <li><a href="/course/show/{{$course->id}}" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                    @endif
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12 pull-left">
                    <form class="form-horizontal" role="form" method="POST" action="/course/short/search" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset>
                            <aside>
                                <div class="panel panel-default courseSidebar">
                                    <div class="panel-heading bg-color-1 border-color-1">
                                        <h3 class="panel-title">Search</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group">
                                            <input type="text" name="query" value="{{ old('query') }}" class="form-control" placeholder="Enter Course Name" aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2"><input class="btn btn-primary bg-color-1" type="submit" value="Search"></span>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </fieldset>
                    </form>

                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset>
                            <aside>

                                <div class="panel panel-default courseSidebar">
                                    <div class="panel-heading bg-color-2 border-color-2">
                                        <h3 class="panel-title">Filter By</h3>
                                    </div>
                                    <div class="panel-body">
                                        {{--NOSS Sector--}}
                                        <div class="lightDrop{{$errors->has('subField') ? 'has-error' : ''}}">
                                            <select name="subField" class="select-drop">
                                                <option selected>All Available Sector</option>
                                                @foreach ($subFields as $subField)
                                                    <option @if (old('subField')== $subField->id) selected
                                                            @endif value="{{$subField->id}}">{{$subField->name}}</option>
                                                @endforeach
                                            </select>
                                            @include('partials.error_block',['item'=> 'subField'])
                                        </div>

                                        {{--Audience--}}
                                        <div class="lightDrop{{$errors->has('audience') ? 'has-error' : ''}}">
                                            <select name="audience" class="select-drop">
                                                <option selected>Targeted Participant</option>
                                                @foreach ($audiences as $audience)
                                                    <option @if (old('audience') == $audience->id) selected
                                                            @endif value="{{$audience->id}}">{{$audience->value}}</option>
                                                @endforeach
                                            </select>
                                            @include('partials.error_block',['item'=> 'audience'])
                                        </div>

                                        <div class="priceRange">
                                            <!--<div class="price-slider-inner">
                              <span class="amount-wrapper">
                                Price:
                                <input type="text" id="price-amount-1" readonly>
                                <strong>-</strong>
                                <input type="text" id="price-amount-2" readonly>
                              </span>
                                                <div id="price-range"></div>
                                            </div>-->
                                            <input class="btn btn-primary bg-color-2" type="submit" value="Filter">
                                            <!-- <span class="priceLabel">Price: <strong>$12 - $30</strong></span> -->
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="coursesCounter">
                                    @foreach($countdowns as $countdown)
                                        <div class="counterInner">
                                            <h3 class="color-3">Next Course Is In</h3>
                                            <h3>{{$countdown->date_start->format('d M, Y')}}</h3>
                                            <h3>({{ $countdown->date_start->diffForHumans() }})</h3>
                                        <!--<div class="coursesCountStart clearfix">
                                            <div id="courseTimer" class="courseCountTimer">{{ $countdown->date_start->diffForHumans() }}</div>
                                        </div>-->
                                            <!--<a href="#" class="btn btn-primary">buy course</a>-->
                                        </div>
                                    @endforeach
                                </div>--}}
                            </aside>
                        </fieldset>
                    </form>
                </div>
            </div>

            <div class="pagerArea text-center">
                <ul class="pager">
                    {{ $courses->links() }}
                </ul>
            </div>

        </div>
    </section>

@endsection