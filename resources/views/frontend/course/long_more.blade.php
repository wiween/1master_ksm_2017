@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Long Course'])

    <section class="mainContent full-width clearfix courseSingleSection">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12 block pull-right">
                    <div class="thumbnail thumbnailContent alt">
                        <div><img src="{{$course->avatar}}" alt="image" height="470" width="870"></div>
                        <div class="caption border-color-1">
                            <h3 class="color-1">{{ $course->name }}</h3>
                            <p>{{ $course->description }}</p>
                        </div>
                    </div>
                    <div class="btnArea">
                        <a href="/course/long" class="btn btn-primary">Back</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                    <aside>
                        <div class="rightSidebar">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-color-1 border-color-1">
                                    <h3 class="panel-title">Course Information</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-2">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-2">Start Date</h4>
                                                <p>{{$course->date_start->format('d M, Y')}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-3">
                                                <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-3">Required Age</h4>
                                                <p>{{$course->age}} years old</p>
                                            </div>
                                        </li>
                                        <li class="media iconContet">
                                            <div class="media-left iconContent bg-color-4">
                                                <i class="fa fa-anchor" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-4">Available Seats</h4>
                                                <p>{{$course->total_participant}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-5">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-5">Course Venue</h4>
                                                <p>{{$course->venue}}, {{$course->state}}</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-6">
                                                <i class="fa fa-money" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-6">Course Fee</h4>
                                                <p>RM{{$course->fee}}/person</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection