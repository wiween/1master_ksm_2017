@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'All Activities'])
    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix coursesSection">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12 pull-right">
                    @if($activities->isEmpty())
                        No Activity found
                    @else
                        @foreach($activities as $activity)
                            <div class="col-md-4 col-sm-6 col-xs-12 block">
                                <div class="thumbnail thumbnailContent" href="/activity/index_more/{{$activity->id}}">
                                    <a href="course-single-left-sidebar.html"><img
                                                src="{{$activity->image}}" alt="image"
                                                class="img-responsive">
                                    </a>
                                    <div class="caption border-color-1">
                                        <h3 class="media-heading color-3">
                                            {{$activity->name}}</h3>
                                            <h5 class="color-6">({{$activity->organization->name}})</h5>
                                            <ul class="list-unstyled">
                                            <li><i class="fa fa-calendar-o"
                                                   aria-hidden="true"></i>{{ $activity->date_start->format('d M, Y') }}
                                                to {{ $activity->date_end->format('d M, Y') }}</li>
                                            <li><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $activity->time }}
                                            </li>
                                            <li><i class="fa fa-venus-mars"
                                                   aria-hidden="true"></i>{{ $activity->venue }}</li>

                                        </ul>
                                        <ul class="list-inline btn-yellow">
                                            <li><a href="/activity/show/{{$activity->id}}" class="btn btn-link">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>More</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="col-md-3 col-sm-5 col-xs-12 pull-left">

                        <form class="form-horizontal" role="form" method="POST" action="/activity/search" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <fieldset>
                                <aside>
                                    <div class="panel panel-default courseSidebar">
                                        <div class="panel-heading bg-color-1 border-color-1">
                                            <h3 class="panel-title">Search</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="input-group">
                                                <input type="text" name="query" value="{{ old('query') }}" class="form-control" placeholder="Enter Activity Name" aria-describedby="basic-addon2">
                                                <span class="input-group-addon" id="basic-addon2"><input class="btn btn-primary bg-color-1" type="submit" value="Search"></span>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </fieldset>
                        </form>


                    </div>
                </div>


            <div class="pagerArea text-center">
                <ul class="pagerArea">
                    {{ $activities->links() }}
                </ul>
            </div>

        </div>
    </section>

@endsection