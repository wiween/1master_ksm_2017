@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'All Activities'])

    <section class="mainContent full-width clearfix courseSingleSection">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12 block pull-right">
                    <div class="thumbnail thumbnailContent alt">
                        <div><img src="{{$activity->image}}" alt="image" height="470" width="870"></div>
                        <div class="caption border-color-1">
                            <h3 class="color-1">{{ $activity->name }}</h3>
                            <p>{{ $activity->description }}</p>
                        </div>
                    </div>
                    <div class="btnArea">
                        <a href="/activity" class="btn btn-primary">Back</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                    <aside>
                        <div class="rightSidebar">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-color-1 border-color-1">
                                    <h3 class="panel-title">Activity Information</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                        <!--start date-->
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-2">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-2">Start Date</h4>
                                                <p>{{$activity->date_start->format('d M, Y')}}</p>
                                            </div>
                                        </li>
                                        <!--target participant-->
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-3">
                                                <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-3">Audience</h4>
                                                <p>{{  \App\Lookup::getValue($activity->audience) }}</p>
                                            </div>
                                        </li>
                                        <!--venue-->
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-5">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-5">Activity Venue</h4>
                                                <p>{{$activity->venue}}, {{$activity->state}}</p>
                                            </div>
                                        </li>
                                        <!--participant-->
                                        <li class="media iconContet">
                                            <div class="media-left iconContent bg-color-4">
                                                <i class="fa fa-anchor" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-4">Available Seats</h4>
                                                <p>{{$activity->participant}}</p>
                                            </div>
                                        </li>
                                        <!--rasmi oleh-->
                                        <li class="media">
                                            <div class="media-left iconContent bg-color-5">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body iconContent">
                                                <h4 class="media-heading color-5">officiated by</h4>
                                                <p>{{$activity->officiated_by}}</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection