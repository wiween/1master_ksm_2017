@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Testimonial'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix testimonialArea">
        <div class="container">
           <div class="row testimonial-grid">
               <a href="#myModal5" data-toggle="modal">
                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent bg-color-1">
                        <span class="userSign border-color-1"><i class="fa fa-quote-left color-1" aria-hidden="true"></i></span>
                        <p><img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/5Z9aFfVBcsA/mqdefault.jpg" alt=""></p>
                        <h3><a href="#">Operations Director</a></h3>
                    </div>
            </div>
            </a>

                <div id="myModal5" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 class="media-heading"><a>Operations Director</a></h4>
                            </div>
                            <div class="modal-body">
                                <iframe id="cartoonVideo5" width="560" height="315" src="https://www.youtube.com/embed/5Z9aFfVBcsA" frameborder="0" allowfullscreen></iframe>
                                <br />
                                <p>Rusliza attributes her success to her passion in electronics. She started her career as a software engineer and has since risen up the corporate ladder.</p>
                            </div>
                        </div>
                    </div>
                </div>

               <a href="#myModal4" data-toggle="modal">
                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent  bg-color-2">
                        <span class="userSign border-color-2"><i class="fa fa-quote-left color-2" aria-hidden="true"></i></span>
                        <p><img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/YwXjAII7150/mqdefault.jpg" alt=""></p>
                        <h3><a href="#">Executive Housekeeper</a></h3>
                    </div>
                </div>
               </a>

                <div id="myModal4" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 class="media-heading"><a>Executive Housekeeper</a></h4>
                            </div>
                            <div class="modal-body">
                                <iframe id="cartoonVideo4" width="560" height="315" src="https://www.youtube.com/embed/YwXjAII7150" frameborder="0" allowfullscreen></iframe>
                                <br />
                                <p>Ruzaini started his career as a bellboy in the hotel industry. He got his break when he was offered a position at the front desk. Throughout the years he rose through various position in the hotel industry and is now a professional executive housekeeper.</p>
                            </div>
                        </div>
                    </div>
                </div>

               <a href="#myModal3" data-toggle="modal">
                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent  bg-color-3">
                        <span class="userSign border-color-3"><i class="fa fa-quote-left color-3" aria-hidden="true"></i></span>
                        <p><img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/YAIwlvkXwv4/mqdefault.jpg" alt=""></p>
                        <h3><a href="#">Head of Production</a></h3>
                    </div>
                </div>
               </a>

                <div id="myModal3" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 class="media-heading"><a>Head of Production</a></h4>
                            </div>
                            <div class="modal-body">
                                <iframe id="cartoonVideo3" width="560" height="315" src="https://www.youtube.com/embed/YAIwlvkXwv4" frameborder="0" allowfullscreen></iframe>
                                <br />
                                <p>Ramadan says with the right skill sets and attitude, one gets to eventually taste success. He advises to never take things for granted and every working session is an opportunity to presents the best in you.</p>
                            </div>
                        </div>
                    </div>
                </div>

               <a href="#myModal2" data-toggle="modal">
                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent  bg-color-4">
                        <span class="userSign border-color-4"><i class="fa fa-quote-left color-4" aria-hidden="true"></i></span>
                        <p><img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/790lkAJvUSQ/mqdefault.jpg" alt=""></p>
                        <h3><a href="#">3D Animation Lecturer</a></h3>
                    </div>
                </div>
               </a>

                <div id="myModal2" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 class="media-heading"><a>3D Animation Lecturer</a></h4>
                            </div>
                            <div class="modal-body">
                                <iframe id="cartoonVideo2" width="560" height="315" src="https://www.youtube.com/embed/790lkAJvUSQ" frameborder="0" allowfullscreen></iframe>
                                <br />
                                <p>Sheheide finds fulfilment in sharing his experience and knowledge in teaching young talents to excel in the field of animation. Every day he gets to meet different talented people from all walks of life.</p>
                            </div>
                        </div>
                    </div>
                </div>

               <a href="#myModal1" data-toggle="modal">
                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent  bg-color-5">
                        <span class="userSign border-color-5"><i class="fa fa-quote-left color-5" aria-hidden="true"></i></span>
                        <p><img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/tHm5BAmuuow/mqdefault.jpg" alt=""></p>
                        <h3><a href="#">Software Supervisor</a></h3>
                    </div>
                </div>
               </a>

                <div id="myModal1" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 class="media-heading"><a>Software Supervisor</a></h4>
                            </div>
                            <div class="modal-body">
                                <iframe id="cartoonVideo1" width="560" height="315" src="https://www.youtube.com/embed/tHm5BAmuuow" frameborder="0" allowfullscreen></iframe>
                                <br />
                                <p>Rofizan enjoys her job as a software supervisor. Over the years she followed her career pathway from a junior position to middle management.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-12">
                    <div class="testimonialContent  bg-color-6">
                        <span class="userSign border-color-6"><i class="fa fa-quote-left color-6" aria-hidden="true"></i></span>
                        <p>Many more testimonials to come.Please keep in touch with us...</p>
                        <h3>1Master's Team<br> <small>1Master</small></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection