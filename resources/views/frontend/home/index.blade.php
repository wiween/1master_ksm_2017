@extends('layouts.frontend')

@section('content')
    <!-- BANNER -->
    <section class="bannercontainer bannercontainerV1">
        <div class="fullscreenbanner-container">
            <div class="fullscreenbanner">
                <ul>
                    @foreach($sliders as $slider)
                        <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                            <img src="{{ $slider->sliderphoto }}" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                            <div class="slider-caption container">
                                <div class="tp-caption rs-caption-1 sft start"
                                     data-hoffset="0"
                                     data-y="200"
                                     data-speed="800"
                                     data-start="1000"
                                     data-easing="Back.easeInOut"
                                     data-endspeed="300">
                                   {{ $slider->title }}
                                </div>

                                <div class="tp-caption rs-caption-2 sft"
                                     data-hoffset="0"
                                     data-y="265"
                                     data-speed="1000"
                                     data-start="1500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="off">
                                    {{ $slider->subtitle }}
                                </div>
                            </div>
                        </li>

                        @endforeach

                    {{--<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">--}}
                        {{--<img src="/images/frontend/home/slider/banner_adtec2.png" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">--}}
                        {{--<div class="slider-caption container">--}}
                            {{--<div class="tp-caption rs-caption-1 sft start text-center"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-x="center"--}}
                                 {{--data-y="200"--}}
                                 {{--data-speed="800"--}}
                                 {{--data-start="1000"--}}
                                 {{--data-easing="Back.easeInOut"--}}
                                 {{--data-endspeed="300">--}}
                                {{--Talented Malaysian For Malaysia--}}
                            {{--</div>--}}

                            {{--<div class="tp-caption rs-caption-2 sft text-center"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-x="center"--}}
                                 {{--data-y="265"--}}
                                 {{--data-speed="1000"--}}
                                 {{--data-start="1500"--}}
                                 {{--data-easing="Power4.easeOut"--}}
                                 {{--data-endspeed="300"--}}
                                 {{--data-endeasing="Power1.easeIn"--}}
                                 {{--data-captionhidden="off">--}}
                                {{--1Malaysia Skill Training Enhancement for the Rakyat (1Master)--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li data-transition="fade" data-slotamount="5" data-masterspeed="700"  data-title="Slide 3">--}}
                        {{--<img src="/images/frontend/home/slider/banner_adtec3.png" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">--}}
                        {{--<div class="slider-caption container">--}}
                            {{--<div class="tp-caption rs-caption-1 sft start text-right"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-y="200"--}}
                                 {{--data-x="right"--}}
                                 {{--data-speed="800"--}}
                                 {{--data-start="1000"--}}
                                 {{--data-easing="Back.easeInOut"--}}
                                 {{--data-endspeed="300">--}}
                                {{--High Demand in Industry--}}
                            {{--</div>--}}

                            {{--<div class="tp-caption rs-caption-2 sft text-right"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-y="265"--}}
                                 {{--data-x="right"--}}
                                 {{--data-speed="1000"--}}
                                 {{--data-start="1500"--}}
                                 {{--data-easing="Power4.easeOut"--}}
                                 {{--data-endspeed="300"--}}
                                 {{--data-endeasing="Power1.easeIn"--}}
                                 {{--data-captionhidden="off">--}}
                                {{--Become skill professional to meet the ever growing demand by various industry.--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">--}}
                        {{--<img src="/images/frontend/home/slider/banner_adtec4.png" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">--}}
                        {{--<div class="slider-caption container">--}}
                            {{--<div class="tp-caption rs-caption-1 sft start text-center"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-x="center"--}}
                                 {{--data-y="200"--}}
                                 {{--data-speed="800"--}}
                                 {{--data-start="1000"--}}
                                 {{--data-easing="Back.easeInOut"--}}
                                 {{--data-endspeed="300">--}}
                                {{--Selamat Datang Boss--}}
                            {{--</div>--}}

                            {{--<div class="tp-caption rs-caption-2 sft text-center"--}}
                                 {{--data-hoffset="0"--}}
                                 {{--data-x="center"--}}
                                 {{--data-y="265"--}}
                                 {{--data-speed="1000"--}}
                                 {{--data-start="1500"--}}
                                 {{--data-easing="Power4.easeOut"--}}
                                 {{--data-endspeed="300"--}}
                                 {{--data-endeasing="Power1.easeIn"--}}
                                 {{--data-captionhidden="off">--}}
                                {{--Tuan Haji Hassan.--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </section>

    <!-- MAIN CONTENT -->
    <section class="clearfix linkSection hidden-xs">
        <div class="sectionLinkArea hidden-xs scrolling">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="#student" class="sectionLink bg-color-1" id="coursesLink">
                            <i class="fa fa-file-text-o linkIcon border-color-1" aria-hidden="true"></i>
                            <span class="linkText">Student</span>
                            <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#employer" class="sectionLink bg-color-2" id="teamLink">
                            <i class="fa fa-calendar-o linkIcon border-color-2" aria-hidden="true"></i>
                            <span class="linkText">Employer</span>
                            <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#working-adult" class="sectionLink bg-color-3" id="galleryLink">
                            <i class="fa fa-picture-o linkIcon border-color-3" aria-hidden="true"></i>
                            <span class="linkText">Pensioner</span>
                            <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#jobseeker" class="sectionLink bg-color-4" id="newsLink">
                            <i class="fa fa-files-o linkIcon border-color-4" aria-hidden="true"></i>
                            <span class="linkText">Working Adult</span>
                            <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FEATURE SECTION -->
    <section class="mainContent full-width clearfix featureSection">
        <div class="container">
            <div class="sectionTitle text-center">
                <h2>
                    <span class="shape shape-left bg-color-4"></span>
                    <span>Facts About 1Master</span>
                    <span class="shape shape-right bg-color-4"></span>
                </h2>
            </div>

            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-1">
                <i class="fa fa-graduation-cap bg-color-1" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-1">NBOS Initiative</h3>
                            <p>NBOS initiative coordinated by the Ministry of Human Resources</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-2">
                <i class="fa fa-leaf bg-color-2" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-2">Objective</h3>
                            <p>To rapidly enhance the skills of the workforce by focusing on skills training via Public-Private Partnership.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-3">
                <i class="fa fa-car bg-color-3" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-3">Public Private Parnership</h3>
                            <p>Engage with private sector to identify relevant skills required by the industry and tailor the training to fulfill these requirements.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-4">
                <i class="fa fa-cutlery bg-color-4" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-4">Collabration</h3>
                            <p>Collaboration with Ministry of Youth and Sports, Ministry of Higher Education, Ministry of Education and Ministry of Rural and Regional Development.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-5">
                <i class="fa fa-heart bg-color-5" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-5">Rapid Execution </h3>
                            <p>Rapidly train mass participants by offering flexible training duration of 1,2,3 or 6 months.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media featuresContent">
              <span class="media-left bg-color-6">
                <i class="fa fa-shield bg-color-6" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading color-6">Target Group </h3>
                            <p>comprised of:<br>
                                i.	existing employees;<br>
                                ii.	students / youths; and <br>
                                iii.retrenched workers.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- PROMOTION SECTION-->
    <section class="promotionWrapper" style="background-image: url(/themes/kidz/img/home/promotion-3.jpg);" id="working-adult">
        <div class="container">
            <div class="promotionInfo">
                <h2>Pensioner</h2>
                <p>Looking for a change or want to upskill yoursel to be promoted or start your own business, check out this :</p>
                <a href="http://www.hrdf.com.my" target="_blank" class="btn btn-primary"><i class="fa fa-sticky-note" aria-hidden="true"></i> Upskilling Program Under HRDF Fund</a>
                <a href="/course/short" class="btn btn-warning"><i class="fa fa-camera" aria-hidden="true"></i> Others Upskilling Program</a>
            </div>
        </div>
    </section>
    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix coursesSection" id="student">
        <div class="container">
            <div class="sectionTitle text-center">
                <h2>
                    <span class="shape shape-left bg-color-4"></span>
                    <span>Student </span>
                    <span class="shape shape-right bg-color-4"></span>
                </h2>
                <p>Students and school leavers who are keen to pursue technical training can click on one of these choices for more information .</p>
            </div>

            <div class="row">
                <div class="col-sm-4 col-xs-12 block">
                    <div class="thumbnail thumbnailContent">
                        <a href="/course/short"><img src="/themes/kidz/img/courses/course-min.jpg" alt="image" class="img-responsive"></a>
                        <div class="caption border-color-1">
                            <h3><a href="/course/short" class="color-1">Courses</a></h3>
                            <p>All courses organized by MOHR, MOT ....</p>
                            <!--<div class="row">
                                <div class="col-xs-6">-->
                                    <a href="/course/short" class="btn btn-default btn-block"><span style="color: #2980b9; margin-top: 20px"><i class="fa fa-calendar-o" aria-hidden="true"></i> Short Course</span> </a>
                                <!--</div>-->
                                <!--<div class="col-xs-6">
                                    <a href="/course/long" class="btn btn-default btn-block"><span style="color: #2980b9; margin-top: 20px">Long</span></a>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 block">
                    <div class="thumbnail thumbnailContent">
                        <a href="/course/venue"><img src="/themes/kidz/img/courses/training_center-min.jpg" alt="image" class="img-responsive"></a>
                        <div class="caption border-color-2">
                            <h3><a href="course-single-left-sidebar.html" class="color-2">Course Venue</a></h3>
                            <p>Where to upskilling learn</p>
                            <a href="/course/venue" class="btn btn-default btn-block"><span style="color: #2980b9; margin-top: 20px"><i class="fa fa-university" aria-hidden="true"></i> Detail</span> </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 block">
                    <div class="thumbnail thumbnailContent">
                        <a href="/course/sponsorship"><img src="/themes/kidz/img/courses/fund-min.jpg" alt="image" class="img-responsive"></a>
                        <div class="caption border-color-3">
                            <h3><a href="course-single-left-sidebar.html" class="color-3">Sponsor</a></h3>
                            <p>No fund? Don't worry, here are our sponsor :</p>
                            <a href="/course/sponsorship" class="btn btn-default btn-block"><span style="color: #2980b9; margin-top: 20px"><i class="fa fa-money" aria-hidden="true"></i> Sponsorship</span> </a>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <!--awin tambah -->
    <!-- COLOR SECTION -->
    <section class="colorSection full-width clearfix bg-color-4 servicesSection" id="employer">
        <div class="container">
            <div class="sectionTitle text-center alt">
                <h2>
                    <span class="shape shape-left bg-color-3"></span>
                    <span>Employer</span>
                    <span class="shape shape-right bg-color-3"></span>
                </h2>
                <p>Tired of your current job, looking for a change or want to upskill yoursel to be promoted or start your own business, check out the following:</p>
            </div>

            <div class="row">
                <a href="http://www.hrdf.com.my">
                    <div class="col-sm-4 col-xs-12">
                        <div class="media servicesContent">
                      <span class="media-left">
                        <i class="fa fa-shield bg-color-4" aria-hidden="true"></i>
                      </span>
                            <div class="media-body">
                                <h3 class="media-heading">Fund by HRDF</h3>
                                <p>All course HRDF Fund</p>
                            </div>
                        </div>
                    </div>
                </a>
                <div class="col-sm-4 hidden-xs">
                    <div class="text-center">
                        {{--<img src="/themes/kidz/img/home/services.png" alt="image">--}}
                    </div>
                </div>
                <a href="/course/short">
                    <div class="col-sm-4 col-xs-12">
                        <div class="media servicesContent">
                      <span class="media-left">
                        <i class="fa fa-car bg-color-4" aria-hidden="true"></i>
                      </span>
                            <div class="media-body">
                                <h3 class="media-heading">Other Programs</h3>
                                <p>Other upskilling program which is not under HRDF</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix eventSection" id="jobseeker">
        <div class="container">
            <div class="sectionTitle text-center">
                <h2>
                    <span class="shape shape-left bg-color-4"></span>
                    <span>Working Adult</span>
                    <span class="shape shape-right bg-color-4"></span>
                </h2>
                <p>If you are looking for a job, look no further find out what you want to be.</p>
            </div>

            <div class="row">
                <div class="col-sm-6 col-xs-12 block">
                    <div class="media eventContent bg-color-1">
                        <a class="media-left" href="single-event-left-sidebar.html">
                            <img class="media-object" src="/themes/kidz/img/jobseeker/hireme-min.jpg" alt="Image">
                            {{--<span class="sticker-round">1</span>--}}
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Fresh Graduate</h3>
                            {{--<span>Don't know where to find guide</span>--}}
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Program Kerjaya
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>JobsMalaysia
                                    </a>
                                </li>
                            </ul>
                            <!--<p>Don't know where to start? Here our guide</p>
                            <ul class="list-inline btn-yellow">
                                <li><a href="single-event-left-sidebar.html" class="btn btn-primary">read more</a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 block">
                    <div class="media eventContent bg-color-2">
                        <a class="media-left" href="single-event-left-sidebar.html">
                            <img class="media-object" src="/themes/kidz/img/jobseeker/freshgraduate-min.jpg" alt="Image">
                            {{--<span class="sticker-round">2</span>--}}
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Retrenched Workers</h3>
                            {{--<span>Don't know where to find guide</span>--}}
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>1MOC
                                    </a>
                                </li>

                                <li>
                                    <a href="http://www.jobmalaysia.gov.my">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>JobsMalaysia
                                    </a>
                                </li>

                            </ul>
                            <!--<p>All program special for retrench workers</p>
                            <ul class="list-inline btn-green">
                                <li><a href="single-event-left-sidebar.html" class="btn btn-primary">read more</a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 block">
                    <div class="media eventContent bg-color-3">
                        <a class="media-left" href="single-event-left-sidebar.html">
                            <img class="media-object" src="/themes/kidz/img/jobseeker/working_adult_new-min.jpg" alt="Image">
                            {{--<span class="sticker-round">3</span>--}}
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Existing Employee</h3>
                            {{--<span>Don't know where to find guide</span>--}}
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>JobsMalaysia
                                    </a>
                                </li>
                            </ul>
                            <!--<p>Tired doing....</p>
                            <ul class="list-inline btn-red">
                                <li><a href="single-event-left-sidebar.html" class="btn btn-primary">read more</a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 block">
                    <div class="media eventContent bg-color-4">
                        <a class="media-left" href="single-event-left-sidebar.html">
                            <img class="media-object" src="/themes/kidz/img/jobseeker/keyguide2-min.jpg" alt="Image">
                            {{--<span class="sticker-round">4</span>--}}
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Key Guide</h3>
                            {{--<span>Don't know where to find guide</span>--}}
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Wages Guide
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>Job Guide
                                    </a>
                                </li>
                            </ul>
                            <!--<p>Are you loss? Here are the guide </p>
                           ul class="list-inline btn-sky">
                               <li><a href="#" class="btn btn-primary">read more</a></li>
                           </ul>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- COLOR SECTION -->
    <section class="colorSection full-width clearfix bg-color-5 teamSection" id="ourTeam">
        <div class="container">
            <div class="sectionTitle text-center alt">
                <h2>
                    <span class="shape shape-left bg-color-3"></span>
                    <span>Our Partner</span>
                    <span class="shape shape-right bg-color-3"></span>
                </h2>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="owl-carousel teamSlider">
                        <div class="slide">
                            <div class="teamContent">
                                <div class="teamImage">
                                    <img src="/themes/kidz/img/partner/jata.jpg" alt="img" class="img-circle">
                                    <div class="maskingContent">
                                        <ul class="list-inline">
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="teamInfo">
                                    <h3><a href="teachers-details.html">MOHR</a></h3>
                                    <p>Kementerian Sumber Manusia</p>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="teamContent">
                                <div class="teamImage">
                                    <img src="/themes/kidz/img/partner/kbs.jpg" alt="img" class="img-circle">
                                    <div class="maskingContent">
                                        <ul class="list-inline">
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="teamInfo">
                                    <h3><a href="teachers-details.html">KBS</a></h3>
                                    <p>Kementerian Belia Dan Sukan</p>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="teamContent">
                                <div class="teamImage">
                                    <img src="/themes/kidz/img/partner/jata.jpg" alt="img" class="img-circle">
                                    <div class="maskingContent">
                                        <ul class="list-inline">
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="teamInfo">
                                    <h3><a href="teachers-details.html">KPT</a></h3>
                                    <p>Kementerian Pengajian Tinggi</p>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="teamContent">
                                <div class="teamImage">
                                    <img src="/themes/kidz/img/partner/mara.jpg" alt="img" class="img-circle">
                                    <div class="maskingContent">
                                        <ul class="list-inline">
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="teamInfo">
                                    <h3><a href="teachers-details.html">MARA</a></h3>
                                    <p>Majlis Amanah Rakyat</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix homeGallerySection" id="ourGallery">
        <div class="container">
            <div class="sectionTitle text-center">
                <h2>
                    <span class="shape shape-left bg-color-4"></span>
                    <span>Photo Gallery</span>
                    <span class="shape shape-right bg-color-4"></span>
                </h2>
            </div>

            <div class="row">
               <div class="col-xs-12">
                   <!-- <div class="filter-container isotopeFilters">
                      <ul class="list-inline filter">
                           <li class="active"><a href="#" data-filter="*">View All</a></li>
                           <!--a href="#" data-filter=".charity">Charity</a></li>
                           <li><a href="#" data-filter=".nature">nature</a></li>
                           <li><a href="#" data-filter=".children">children</a></li>
                        </ul>
                    </div>-->
                </div>
            </div>
            <div class="row isotopeContainer" id="container">
                @foreach($galleries as $gallery)
                    <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector charity">
                        <article class="">
                            <figure>
                                <img src="{{ $gallery->image }}" alt="image" class="img-rounded">
                                <div class="overlay-background">
                                    <div class="inner"></div>
                                </div>
                                <div class="overlay">
                                    <a class="fancybox-pop" rel="portfolio-1" href="{{ $gallery->image }}">
                                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </article>
                    </div>
                    @endforeach
                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector charity">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_1.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_1.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_2.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_2.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_3.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_3.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector charity">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_4.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_4.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_5.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_5.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_6.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_6.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_7.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_7.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">--}}
                    {{--<article class="">--}}
                        {{--<figure>--}}
                            {{--<img src="/themes/kidz/img/home/home_gallery/gallery_sm_8.jpg" alt="image" class="img-rounded">--}}
                            {{--<div class="overlay-background">--}}
                                {{--<div class="inner"></div>--}}
                            {{--</div>--}}
                            {{--<div class="overlay">--}}
                                {{--<a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_8.jpg">--}}
                                    {{--<i class="fa fa-search-plus" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</figure>--}}
                    {{--</article>--}}
                {{--</div>--}}
            </div>

            <div class="btnArea">
                <a href="/activity/gallery" class="btn btn-primary">View More Photo</a>
            </div>

        </div>
    </section>

    <!-- COUNT UP SECTION-->
    <section class="countUpSection">
        <div class="container">
            <div class="sectionTitleSmall">
                <h2>Do You Know?</h2>
                <p>1Master Statistic</p>
            </div>

            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <div class="text-center">
                        {{--<div class="counter">{{ $totalStudent }}</div>--}}
                        <div class="counter">{{ 0 }}</div>
                        <div class="counterInfo bg-color-1">Student</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="text-center">
                        <div class="counter">{{ $totalActivity }}</div>
                        <div class="counterInfo bg-color-2">Activity</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="text-center">
                        <div class="counter">{{ $totalCourse }}</div>
                        <div class="counterInfo bg-color-3">Course</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="text-center">
                        {{--<div class="counter">{{ $totalVisit }}</div>--}}
                        <div class="counter">{{ 0 }}</div>
                        <div class="counterInfo bg-color-4">Visit</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix newsSection" id="latestNews">
        <div class="container">
            <div class="sectionTitle text-center">
                <h2>
                    <span class="shape shape-left bg-color-4"></span>
                    <span>Activity</span>
                    <span class="shape shape-right bg-color-4"></span>
                </h2>
            </div>

            <div class="row">
                @foreach($activities as $activity)
                    <div class="col-sm-4 col-xs-12 block">
                        <div class="thumbnail thumbnailContent">
                            <a href="single-blog-left-sidebar.html"><img src="{{ $activity->image }}" alt="image" class="img-responsive"></a>
                            <div class="sticker-round bg-color-1">{{ $activity->date_start->format('d') }}<br>{{ $activity->date_start->format('M') }}</div>
                            <div class="caption border-color-1">
                                <h3><a href="single-blog-left-sidebar.html" class="color-1">{{ $activity->name }}</a></h3>
                                <ul class="list-inline">
                                    <li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Location : {{ $activity->venue }}</a></li>
                                    <li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>Time : {{ $activity->time }}</a></li>
                                </ul>
                                <p>{{ $activity->description }}</p>
                                <ul class="list-inline btn-yellow">
                                    <li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    @endforeach

                {{--<div class="col-sm-4 col-xs-12 block">--}}
                    {{--<div class="thumbnail thumbnailContent">--}}
                        {{--<a href="single-blog-left-sidebar.html"><img src="/themes/kidz/img/home/news/news-2.jpg" alt="image" class="img-responsive"></a>--}}
                        {{--<div class="sticker-round bg-color-2">10 <br>July</div>--}}
                        {{--<div class="caption border-color-2">--}}
                            {{--<h3><a href="single-blog-left-sidebar.html" class="color-2">Kelas Bersama Cikgu Rita</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Dataran Gemilang</a></li>--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 AM</a></li>--}}
                            {{--</ul>--}}
                            {{--<p>Cikgu Rita terkenal dengan chef yang pandai memasak kek. Mari belajar membuat kek karot.</p>--}}
                            {{--<ul class="list-inline btn-green">--}}
                                {{--<li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4 col-xs-12 block">--}}
                    {{--<div class="thumbnail thumbnailContent">--}}
                        {{--<a href="single-blog-left-sidebar.html"><img src="/themes/kidz/img/home/news/news-3.jpg" alt="image" class="img-responsive"></a>--}}
                        {{--<div class="sticker-round bg-color-3">10 <br>July</div>--}}
                        {{--<div class="caption border-color-3">--}}
                            {{--<h3><a href="single-blog-left-sidebar.html" class="color-3">Bootcamp Anjuran Kementerian Belia & Sukan</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Dataran Gemilang</a></li>--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 AM</a></li>--}}
                            {{--</ul>--}}
                            {{--<p>Bootcamp ini khas untuk be;ia-belia yang sedang mencari mentor bagi memanjat gunung. </p>--}}
                            {{--<ul class="list-inline btn-red">--}}
                                {{--<li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row">--}}
                {{--<div class="col-sm-4 col-xs-12 block">--}}
                    {{--<div class="thumbnail thumbnailContent">--}}
                        {{--<a href="single-blog-left-sidebar.html"><img src="/themes/kidz/img/home/news/news-1.jpg" alt="image" class="img-responsive"></a>--}}
                        {{--<div class="sticker-round bg-color-1">10 <br>July</div>--}}
                        {{--<div class="caption border-color-1">--}}
                            {{--<h3><a href="single-blog-left-sidebar.html" class="color-1">Mari Mewarna</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Dataran Gemilang</a></li>--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 AM</a></li>--}}
                            {{--</ul>--}}
                            {{--<p>Pertandingan warna bagi kanak berusia 1-2 tahun di Datran Gemilang Putrajaya. </p>--}}
                            {{--<ul class="list-inline btn-yellow">--}}
                                {{--<li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4 col-xs-12 block">--}}
                    {{--<div class="thumbnail thumbnailContent">--}}
                        {{--<a href="single-blog-left-sidebar.html"><img src="/themes/kidz/img/home/news/news-2.jpg" alt="image" class="img-responsive"></a>--}}
                        {{--<div class="sticker-round bg-color-2">10 <br>July</div>--}}
                        {{--<div class="caption border-color-2">--}}
                            {{--<h3><a href="single-blog-left-sidebar.html" class="color-2">Kelas Bersama Cikgu Rita</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Dataran Gemilang</a></li>--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 AM</a></li>--}}
                            {{--</ul>--}}
                            {{--<p>Cikgu Rita terkenal dengan chef yang pandai memasak kek. Mari belajar membuat kek karot.</p>--}}
                            {{--<ul class="list-inline btn-green">--}}
                                {{--<li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4 col-xs-12 block">--}}
                    {{--<div class="thumbnail thumbnailContent">--}}
                        {{--<a href="single-blog-left-sidebar.html"><img src="/themes/kidz/img/home/news/news-3.jpg" alt="image" class="img-responsive"></a>--}}
                        {{--<div class="sticker-round bg-color-3">10 <br>July</div>--}}
                        {{--<div class="caption border-color-3">--}}
                            {{--<h3><a href="single-blog-left-sidebar.html" class="color-3">Bootcamp Anjuran Kementerian Belia & Sukan</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-map" aria-hidden="true"></i>Dataran Gemilang</a></li>--}}
                                {{--<li><a href="single-blog-left-sidebar.html"><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 AM</a></li>--}}
                            {{--</ul>--}}
                            {{--<p>Bootcamp ini khas untuk be;ia-belia yang sedang mencari mentor bagi memanjat gunung. </p>--}}
                            {{--<ul class="list-inline btn-red">--}}
                                {{--<li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


            <div class="btnArea">
                <a href="/activity" class="btn btn-primary">View More Activity</a>
            </div>

        </div>
    </section>

    <!-- LIGHT SECTION -->
    {{--<section class="lightSection full-width clearfix homeContactSection">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-6 col-xs-12">--}}
                    {{--<div class="homeContactContent">--}}
                        {{--<h2>Our Address</h2>--}}
                        {{--<p>1Master by Ministry of Human Resource, Malaysia</p>--}}
                        {{--<address>--}}
                            {{--<p><i class="fa fa-map-marker bg-color-1" aria-hidden="true"></i>Persiaran Sultan Sallahuddin Abdul Aziz Shah, Presint 1, 62000 Putrajaya</p>--}}
                            {{--<p><i class="fa fa-envelope bg-color-2" aria-hidden="true"></i><a href="mailto:1master@mohr.gov.my">1master@mohr.gov.my</a></p>--}}
                            {{--<p><i class="fa fa-phone bg-color-4" aria-hidden="true"></i>03 8885 5000</p>--}}
                        {{--</address>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6 col-xs-12">--}}
                    {{--<div class="homeContactContent">--}}
                        {{--<form class="form-horizontal" role="form" method="POST">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">--}}
                                {{--<i class="fa fa-user"></i>--}}
                                {{--<input name="first_name" type="text" class="form-control border-color-1" id="first_name" placeholder="First name" required>--}}
                                {{--@include('partials.error_block', ['item' => 'first_name'])--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<i class="fa fa-envelope" aria-hidden="true"></i>--}}
                                {{--<input name="email" type="text" class="form-control border-color-2" id="email" placeholder="Email address">--}}
                                {{--@include('partials.error_block', ['item' => 'email'])--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">--}}
                                {{--<i class="fa fa-phone" aria-hidden="true"></i>--}}
                                {{--<input name="phone" type="text" class="form-control border-color-5" id="phone" placeholder="Phone">--}}
                                {{--@include('partials.error_block', ['item' => 'phone'])--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">--}}
                                {{--<i class="fa fa-book" aria-hidden="true"></i>--}}
                                {{--<input name="subject" type="text" class="form-control border-color-6" id="subject" placeholder="Subject">--}}
                                {{--@include('partials.error_block', ['item' => 'subject'])--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">--}}
                                {{--<i class="fa fa-comments" aria-hidden="true"></i>--}}
                                {{--<textarea class="form-control border-color-4" placeholder="Write message" name="message" id="message"></textarea>--}}
                                {{--@include('partials.error_block', ['item' => 'message'])--}}
                            {{--</div>--}}
                            {{--<button type="submit" class="btn btn-primary">Send Message</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

@endsection