@extends('layouts.frontend')

@section('content')
    {{-- Page Title with Background --}}
    @include('partials.page_title', ['title' => 'About Us'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix aboutSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6 col-xs-12">
                    <img src="/images/frontend/about/main.png" alt="image" class="img-responsive img-rounded">
                </div>
                <div class="col-sm-6 col-sm-pull-6 col-xs-12">
                    <div class="schoolInfo">
                        <h2>About 1Master</h2>
                        <p>1MALAYSIA SKILLS TRAINING & ENHANCEMENT FOR THE RAKYAT(1MASTER) is a program specially implemented under the National Blue Ocean Strategy (NBOS) to offer practical training in industry-demanded skills . </p>
                        <p>This program is coordinated by the Department of Skills Development (DSD) under the Ministry of Human Resources to train talented individuals to become skilled professionals to meet the ever growing demand by the various industries for knowledgeable and highly trained certified skilled personnel. </p>
                        <p class="color-3">This portal was designed to attract the younger generation and has a psychometric personality test to help match suitability for an occupation. This is one of the commitment shown by the Government to ensure our future workforce attain the necessary skills and productivity to attract high wages.</p>
                        <ul class="list-unstyled para-list">
                            <li><i class="fa fa-check" aria-hidden="true"></i> Student</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Working Adult</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Job Seeker</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Employer</li>
                        </ul>
                        <!--<a href="about_us.html" class="btn btn-primary">read more</a>-->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- COLOR SECTION -->
    <section class="colorSection full-width clearfix bg-color-4 servicesSection">
        <div class="container">
            <div class="sectionTitle text-center alt">
                <h2>
                    <span class="shape shape-left bg-color-3"></span>
                    <span>Our Services</span>
                    <span class="shape shape-right bg-color-3"></span>
                </h2>
            </div>

            <div class="row">
                <div class="col-sm-4 col-sm-offset-2 col-xs-12">
                    <div class="media servicesContent rightAlign">
                        <a class="media-left" href="/#student">
                            <i class="fa fa-cutlery bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Student</h3>
                            <p>Students and school leavers who are keen to pursue technical training can click on one of these choices.</p>
                        </div>
                    </div>
                    <div class="media servicesContent rightAlign">
                        <a class="media-left" href="/#jobseeker">
                            <i class="fa fa-heart bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Job Seeker</h3>
                            <p>If you are looking for a job, look no further find out what you want to be by clicking one of the icons.</p>
                        </div>
                    </div>
                    <div class="media servicesContent rightAlign">
                        <a class="media-left" href="/#working-adult">
                            <i class="fa fa-shield bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Working Adult</h3>
                            <p>Get better reimbursement by improving your technical skill and expertise. </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media servicesContent">
                        <a class="media-left" href="#">
                            <i class="fa fa-car bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Career Coaching</h3>
                            <p>Through psychometric test, your learning and career path could be determined accurately.</p>
                        </div>
                    </div>
                    <div class="media servicesContent">
                        <a class="media-left" href="http://www.jobsmalaysia.gov.my/">
                            <i class="fa fa-graduation-cap bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Apply for Job</h3>
                            <p>Efficiently search for thousands of available jobs via dedicated portal, JobsMalaysia</p>
                        </div>
                    </div>
                    <div class="media servicesContent">
                        <a class="media-left" href="/course/sponsorship">
                            <i class="fa fa-leaf bg-color-4" aria-hidden="true"></i>
                        </a>
                        <div class="media-body">
                            <h3 class="media-heading">Funding</h3>
                            <p>Do not let your financial constraint forbid your from persuing your dream. Get sponsorship from us!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- WHITE SECTION -->
    {{--<section class="whiteSection full-width clearfix">--}}
        {{--<div class="container">--}}
            {{--<div class="sectionTitle text-center">--}}
                {{--<h2>--}}
                    {{--<span class="shape shape-left bg-color-4"></span>--}}
                    {{--<span>Meet Our Teachers</span>--}}
                    {{--<span class="shape shape-right bg-color-4"></span>--}}
                {{--</h2>--}}
            {{--</div>--}}

            {{--<div class="row">--}}
                {{--<div class="col-sm-3 col-xs-12">--}}
                    {{--<div class="teamContent">--}}
                        {{--<div class="teamImage">--}}
                            {{--<img src="/themes/kidz/img/home/team/team-1.jpg" alt="img" class="img-circle img-responsive">--}}
                            {{--<div class="maskingContent">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="teamInfo teamTeacher">--}}
                            {{--<h3><a href="teachers-details.html">Amanda Stone</a></h3>--}}
                            {{--<p>English Teacher</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-3 col-xs-12">--}}
                    {{--<div class="teamContent">--}}
                        {{--<div class="teamImage">--}}
                            {{--<img src="/themes/kidz/img/home/team/team-2.jpg" alt="img" class="img-circle img-responsive">--}}
                            {{--<div class="maskingContent">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="teamInfo teamTeacher">--}}
                            {{--<h3><a href="teachers-details.html">Amanda Stone</a></h3>--}}
                            {{--<p>English Teacher</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-3 col-xs-12">--}}
                    {{--<div class="teamContent">--}}
                        {{--<div class="teamImage">--}}
                            {{--<img src="/themes/kidz/img/home/team/team-3.jpg" alt="img" class="img-circle img-responsive">--}}
                            {{--<div class="maskingContent">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="teamInfo teamTeacher">--}}
                            {{--<h3><a href="teachers-details.html">Amanda Stone</a></h3>--}}
                            {{--<p>English Teacher</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-3 col-xs-12">--}}
                    {{--<div class="teamContent">--}}
                        {{--<div class="teamImage">--}}
                            {{--<img src="/themes/kidz/img/home/team/team-4.jpg" alt="img" class="img-circle img-responsive">--}}
                            {{--<div class="maskingContent">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="teamInfo teamTeacher">--}}
                            {{--<h3><a href="teachers-details.html">Amanda Stone</a></h3>--}}
                            {{--<p>English Teacher</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
@endsection