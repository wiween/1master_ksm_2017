@extends('layouts.frontend')

@section('content')
    @include('partials.page_title', ['title' => 'Search'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
        <div class="container">
            <form class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}
            <div class="input-group searchArea">
                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                <input type="text" class="form-control" placeholder="Search something..." name="search">
                <button type="submit" class="input-group-addon button" id="basic-addon21">Search</button>
            </div>
            </form>
            <div class="resultInfo">
                <h3>Search result for : </h3>
            </div>

            <div class="row">
                <div class="container">
                    @if(isset($details))
                        <p> The Search results for your query <b> {{ $query }} </b> are :</p>
                        <h2>Sample User details</h2>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($details as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>

            <div class="pagerArea text-center">
                <ul class="pager">
                    <li class="prev"><a href="#"><i class="fa fa-arrow-left" aria-hidden="true"></i>Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">15</a></li>
                    <li class="next"><a href="#">Next<i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>
@endsection