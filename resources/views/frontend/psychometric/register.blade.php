@extends('layouts.frontend')

@section('content')
    @include('partials.page_title', ['title' => 'Register Psychometric Test'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default formPanel">
                        <div class="panel-heading bg-color-1 border-color-1">
                            <h3 class="panel-title">Create  an account</h3>
                        </div>
                        <div class="panel-body">
                            <form action="#" method="POST" role="form">
                                <div class="form-group formField">
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group formField">
                                    <input type="text" class="form-control" placeholder="User name">
                                </div>
                                <div class="form-group formField">
                                    <input type="text" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group formField">
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group formField">
                                    <input type="password" class="form-control" placeholder="Re-Password">
                                </div>
                                <div class="form-group formField">
                                    <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Register">
                                </div>
                                <div class="form-group formField">
                                    <p class="help-block">Allready have an account? <a href="#">Log in</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="panel panel-default formPanel">
                        <div class="panel-heading bg-color-1 border-color-1">
                            <h3 class="panel-title">Allready Registered?</h3>
                        </div>
                        <div class="panel-body">
                            <form action="#" method="POST" role="form">
                                <div class="form-group formField">
                                    <input type="text" class="form-control" placeholder="User name">
                                </div>
                                <div class="form-group formField">
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group formField">
                                    <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Log in">
                                </div>
                                <div class="form-group formField">
                                    <p class="help-block"><a href="#">Forgot password?</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection