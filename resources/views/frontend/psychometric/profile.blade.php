@extends('layouts.frontend')


@section('content')

    @include('partials.page_title',['title'=>'My Profile'])

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-3">
                {{-- PHOTO HERE--}}
                <img src="{{ $avatar }}" class="img img-thumbnail img-responsive"><br><br>
                <a href="/psychometric/edit-profile" class="btn btn-primary btn-block">Update My Profile</a>
            </div>
            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>IC Number</th>
                        <td>{{ $user->ic_number }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        {{-- <a href="mailto:arhamzul@gmail.com">Click Here to send mail</a> --}}
                        <td><a href="mailto:{{ $user->email }}">
                                {{ $user->email }}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>
                            <a href="tel:{{ $user->phone_number }}">
                                {{ $user->phone_number }}
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">
                            @if ($user->status == 'active')
                                <span class="label label-success">{{ $user->status }}</span>
                            @elseif($user->status == 'banned')
                                <span class="label label-warning">{{ $user->status }}</span>
                            @else
                                <span class="label label-default">{{ $user->status }}</span>
                            @endif
                        </td>
                    </tr>
                    @if ($enrollstatus == 'has_result_excel')
                        <tr>
                            <th class="col-md-3">Result Summary</th>
                            <td>
                                <table>
                                    <table class="table table-bordered table-striped">
                                        <th>Field</th>
                                        <th>Score</th>
                                        <th>Category</th>
                                        </tr>
                                        @foreach ($psychometrics as $psychometric)
                                            <tr @if ($highestScore == $psychometric->score) class="success" @endif>
                                            <td>{{ $psychometric->field }}</td>
                                                <td>{{ $psychometric->score }}</td>
                                                <td>{{ $psychometric->category }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    </td>
                                    </tr>
                                    @endif

                                </table>
                                <br>
                                <div>
                                    @if ($enrollstatus == 'never')
                                        <a href="/psychometric/enroll" class="btn btn-success btn-block">Take
                                            Psychometric Test</a>
                                    @endif
                                    @if ($enrollstatus == 'has_result' || $enrollstatus == 'has_result_excel')
                                        <a href="{{ $uploadpdf->filepdf }}" target="_blank" class="btn btn-info btn-block">
                                            <i class="fa fa-download"></i> Download Result</a>
                                    @endif
                                    @if ($enrollstatus == 'enrolling')
                                        <a href="#" class="btn btn-warning btn-block" disabled>In Progress</a>
                                    @endif

                                </div>
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </div>


@endsection