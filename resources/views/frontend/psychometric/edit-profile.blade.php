@extends('layouts.frontend')


@section('content')

    @include('partials.page_title',['title'=>'My Profile'])

    <div class="panel panel-default">
        <div class="panel-body">
            @include('partials.notification')
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">
                        Name
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" type="text" class="form-control" value="{{ old('name', $user->name) }}" required autofocus>
                        @include('partials.error_block', ['item' => 'name'])
                    </div>
                </div>

                {{-- IC Number --}}
                <div class="form-group{{ $errors->has('ic_number') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">
                        IC Number
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="ic_number" type="text" class="form-control" value="{{ old('ic_number', $user->ic_number) }}" required>
                        @include('partials.error_block', ['item' => 'ic_number'])
                    </div>
                </div>

                {{-- Email --}}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">
                        E-Mail Address
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="email" type="email" class="form-control" value="{{ old('email', $user->email) }}" required>
                        @include('partials.error_block', ['item' => 'email'])
                    </div>
                </div>

                {{-- Phone --}}
                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Phone Number</label>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('phone_number', $user->phone_number) }}" class="form-control" name="phone_number">
                        @include('partials.error_block', ['item' => 'phone_number'])
                    </div>
                </div>

                {{-- Avatar / Photo --}}
                <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Avatar
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="avatar">
                        @include('partials.error_block', ['item' => 'avatar'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
