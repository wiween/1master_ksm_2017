@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Photo Gallery'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix homeGallerySection">
        <div class="container">
            {{--<div class="row">--}}
            {{--<div class="col-xs-12">--}}
            {{--<div class="filter-container isotopeFilters">--}}
            {{--<ul class="list-inline filter">--}}
            {{--<li class="active"><a href="#" data-filter="*">View All</a></li>--}}
            {{--<li><a href="#" data-filter=".course">Course</a></li>--}}
            {{--<li><a href="#" data-filter=".workshop">Workshop</a></li>--}}
            {{--<li><a href="#" data-filter=".seminar">Seminar</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row isotopeContainer" id="container">--}}
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12 pull-right">
                    @if($galleries->isEmpty())
                        No Gallery Found
                    @else
                        @foreach ($galleries as $gallery)
                            <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector course">
                                <article class="">
                                    <figure>
                                        <img src="{{ $gallery->image }}" alt="image" class="img-rounded">
                                        {{ $gallery->remark }}
                                        <div class="overlay-background">
                                            <div class="inner"></div>
                                        </div>
                                        <div class="overlay">
                                            <a class="fancybox-pop" rel="portfolio-1" href="{{ $gallery->image }}">
                                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </figure>
                                </article>
                            </div>
                        @endforeach
                    @endif
                </div>

            <div class="col-md-3 col-sm-5 col-xs-12 pull-left">
                {{--Start Filter--}}
                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <fieldset>
                        <aside>
                            <div class="panel panel-default courseSidebar">
                                <div class="panel-heading bg-color-2 border-color-2">
                                    <h3 class="panel-title">Filter By</h3>
                                </div>
                                <div class="panel-body">
                                    <form method="post">
                                        <div class="lightDrop{{$errors->has('state') ? 'has-error' : ''}}">
                                            <select name="agency" class="select-drop">
                                                <option>All Available Agency</option>
                                                @foreach ($agencies as $agency)
                                                    <option @if (old('agency')== $agency->id) selected @endif
                                                    value="{{$agency->id}}">{{$agency->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @include('partials.error_block',['item'=> 'state'])
                                        </div>
                                        <div class="priceRange">
                                        <input class="btn btn-primary bg-color-2" type="submit" value="Filter">
                                        </div>
                                        {{--<button type="submit">List</button>--}}
                                    </form>



                                    {{--Venue--}}
                                    {{--<div class="lightDrop{{$errors->has('venue') ? 'has-error' : ''}}">--}}
                                    {{--<select name="venue" class="select-drop">--}}
                                    {{--<option>All Available Venues</option>--}}
                                    {{--@foreach ($venues as $venue)--}}
                                    {{--<option @if (old('venue')== $venue->venue) selected @endif value="{{$venue->venue}}">{{$venue->venue}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--@include('partials.error_block',['item'=> 'venue'])--}}
                                    {{--</div>--}}

                                    {{--<div class="priceRange">--}}
                                    {{--<!--<div class="price-slider-inner">--}}
                                    {{--<span class="amount-wrapper">--}}
                                    {{--Price:--}}
                                    {{--<input type="text" id="price-amount-1" readonly>--}}
                                    {{--<strong>-</strong>--}}
                                    {{--<input type="text" id="price-amount-2" readonly>--}}
                                    {{--</span>--}}
                                    {{--<div id="price-range"></div>--}}
                                    {{--</div>-->--}}
                                    {{--<input class="btn btn-primary bg-color-2" type="submit" value="Filter">--}}
                                    {{--<!-- <span class="priceLabel">Price: <strong>$12 - $30</strong></span> -->--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--End Filter--}}
                                    {{--<div class="coursesCounter">--}}
                                    {{--@foreach($countdowns as $countdown)--}}
                                    {{--<div class="counterInner">--}}
                                    {{--<h3 class="color-3">Next Course Is In</h3>--}}
                                    {{--<h3>{{$countdown->date_start->format('d M, Y')}}</h3>--}}
                                    {{--<h3>({{ $countdown->date_start->diffForHumans() }})</h3>--}}
                                    {{--<!--<div class="coursesCountStart clearfix">--}}
                                    {{--<div id="courseTimer" class="courseCountTimer">{{ $countdown->date_start->diffForHumans() }}</div>--}}
                                    {{--</div>-->--}}
                                    {{--<!--<a href="#" class="btn btn-primary">buy course</a>-->--}}
                                    {{--</div>--}}
                                    {{--@endforeach--}}
                                </div>
                            </div>
                        </aside>
                    </fieldset>
                </form>
            </div>
            </div>
            <div class="pagerArea text-center col-md-9 col-sm-7 col-xs-12 pull-right">
                <ul class="pagerArea">
                    {{ $galleries->links() }}
                </ul>
            </div>
            </div>
        </div>
    </section>

@endsection