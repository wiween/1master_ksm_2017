@extends('layouts.frontend')

@section('content')
    @include('partials.page_title', ['title' => 'FAQ'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
        <div class="container">
            <div class="row">
                {{--<div class="col-md-3 col-sm-4 col-xs-12">--}}
                    {{--<aside>--}}
                        {{--<div class="panel panel-default eventSidebar">--}}
                            {{--<div class="panel-heading bg-color-1 border-color-1">--}}
                                {{--<h3 class="panel-title">Ask Questions?</h3>--}}
                            {{--</div>--}}
                            {{--<div class="panel-body">--}}
                                {{--<div class="homeContactContent">--}}
                                    {{--<form class="form" role="form" method="POST">--}}
                                        {{--{{ csrf_field() }}--}}
                                        {{--<div class="row">--}}
                                        {{--<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">--}}
                                            {{--<i class="fa fa-user"></i>--}}
                                            {{--<input name="first_name" type="text" class="form-control border-color-1" id="first_name" placeholder="First name" required autofocus>--}}
                                            {{--@include('partials.error_block', ['item' => 'first_name'])--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                            {{--<i class="fa fa-envelope" aria-hidden="true"></i>--}}
                                            {{--<input name="email" type="text" class="form-control border-color-2" id="email" placeholder="Email address">--}}
                                            {{--@include('partials.error_block', ['item' => 'email'])--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">--}}
                                            {{--<i class="fa fa-comments" aria-hidden="true"></i>--}}
                                            {{--<textarea class="form-control border-color-4" placeholder="Write message" name="message" id="message"></textarea>--}}
                                            {{--@include('partials.error_block', ['item' => 'message'])--}}
                                        {{--</div>--}}
                                        {{--<button type="submit" class="btn btn-primary btn-Full">Send Message</button>--}}
                                        {{--</div>--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</aside>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-3 col-xs-12">--}}
                    {{--<img src="/images/frontend/faq/main.png" alt="image" class="img-responsive img-rounded">--}}
                {{--</div>--}}

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <img src="/images/frontend/faq/faqq.png" alt="image" class="img-responsive img-rounded">
                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="accordionCommon hello" id="faqAccordion">
                        <div class="panel-group" id="accordionFaq">
                            <div class="panel panel-default">
                                <a class="panel-heading accordion-toggle bg-color-4" data-toggle="collapse" data-parent="#accordionFaq" href="#collapse-aa">
                                    <span>What is 1Master program?</span>
                                    <span class="iconBlock"><i class="fa fa-chevron-down"></i></span>
                                </a>
                                {{--<div id="collapse-aa" class="panel-collapse collapse in">--}}
                                <div id="collapse-aa" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>1MALAYSIA SKILLS TRAINING & ENHANCEMENT FOR THE RAKYAT(1MASTER) is a program specially implemented under the National Blue Ocean Strategy (NBOS) to offer practical training in industry-demanded skills.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <a class="panel-heading accordion-toggle bg-color-4" data-toggle="collapse" data-parent="#accordionFaq" href="#collapse-bb">
                                    <span>How do I list multiple rooms?</span>
                                    <span class="iconBlock"><i class="fa fa-chevron-up"></i></span>
                                </a>
                                <div id="collapse-bb" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                                        <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <a class="panel-heading accordion-toggle bg-color-4" data-toggle="collapse" data-parent="#accordionFaq" href="#collapse-cc">
                                    <span>How do I list multiple rooms?</span>
                                    <span class="iconBlock"><i class="fa fa-chevron-up"></i></span>
                                </a>
                                <div id="collapse-cc" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                                        <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <a class="panel-heading accordion-toggle bg-color-4" data-toggle="collapse" data-parent="#accordionFaq" href="#collapse-dd">
                                    <span>How do I list multiple rooms?</span>
                                    <span class="iconBlock"><i class="fa fa-chevron-up"></i></span>
                                </a>
                                <div id="collapse-dd" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                                        <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <a class="panel-heading accordion-toggle bg-color-4" data-toggle="collapse" data-parent="#accordionFaq" href="#collapse-ee">
                                    <span>How do I list multiple rooms?</span>
                                    <span class="iconBlock"><i class="fa fa-chevron-up"></i></span>
                                </a>
                                <div id="collapse-ee" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                                        <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <br>
        <br>

        {{--<section>--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}

                    {{--<div class="col-md-3 col-sm-4 col-xs-12">--}}
                        {{--<aside>--}}
                            {{--<div class="panel panel-default eventSidebar">--}}
                                {{--<div class="panel-heading bg-color-1 border-color-1">--}}
                                    {{--<h3 class="panel-title">Filter By Category</h3>--}}
                                {{--</div>--}}
                                {{--<div class="panel-body">--}}
                                    {{--<div class="lightDrop">--}}
                                        {{--<select name="guiest_id4" id="guiest_id4" class="select-drop">--}}
                                            {{--<option value="0">Student</option>--}}
                                            {{--<option value="1">Employer</option>--}}
                                            {{--<option value="2">Working Adult</option>--}}
                                            {{--<option value="3">Job Seeker</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</aside>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-9 col-sm-8 col-xs-12">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12 table-responsive">--}}
                                {{--<div class="panel panel-default eventSidebar">--}}
                                    {{--<div class="panel-heading bg-color-1 border-color-6">--}}
                                        {{--<h3 class="panel-title">Q&A - from 1MASTER user</h3>--}}
                                    {{--</div>--}}
                                    {{--<table class="table">--}}
                                        {{--<tr>--}}
                                            {{--<th>#</th>--}}
                                            {{--<th>Date</th>--}}
                                            {{--<th>Question</th>--}}
                                            {{--<th>By</th>--}}
                                            {{--<th>Answer</th>--}}
                                            {{--<th>Category</th>--}}
                                        {{--</tr>--}}
                                        {{--@foreach ($faqses as $faq)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{ $loop->index + 1 }}</td>--}}
                                                {{--<td>--}}
                                                    {{--{{ $faq->created_at->format('d M, Y') }}--}}
                                                {{--</td>--}}
                                                {{--<td class="col-md-1">{{ $faq->message }}</td>--}}

                                                {{--<td>{{ $faq->first_name }}</td>--}}
                                                {{--<td>{{ $faq->message }}</td>--}}
                                                {{--<td>{{ $faq->category }}</td>--}}

                                            {{--</tr>--}}
                                        {{--@endforeach--}}

                                    {{--</table>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    </section>

@endsection