@extends('layouts.frontend')

@section('content')
    <!-- Page Title with Background -->
    @include('partials.page_title', ['title' => 'Contact Us'])

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix conactSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="media addressContent">
              <span class="media-left bg-color-1" href="#">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading">Address:</h3>
                            <p>Ministry of Human Resources, Block D3-D4 Complex D, 62530 Putrajaya Malaysia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media addressContent">
              <span class="media-left bg-color-2" href="#">
                <i class="fa fa-phone" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading">Phone:</h3>
                            <p><a href="tel:+603 8888 5000">+603 8888 5000</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="media addressContent">
              <span class="media-left bg-color-3" href="#">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
              </span>
                        <div class="media-body">
                            <h3 class="media-heading">Email:</h3>
                            <p><a href="mailto:1master@mohr.gov.my"> 1master[at]mohr[dot]gov[dot]my</a>  Or
                                <a href="mailto:ksm@mohr.gov.my"> ksm[at]mohr[dot]gov[dot]my
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="mapArea areaPadding">
                        <div id="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.573873537129!2d101.7030612142732!3d2.938045355234626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb61e4a33d509%3A0xe105c8e81c4061bd!2sBlok+D3%2C+Kementerian+Sumber+Manusia!5e0!3m2!1sen!2smy!4v1497577607724" width="1130" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @include('partials.notification')
                    <div class="homeContactContent">

                        <form class="form-horizontal" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <i class="fa fa-user"></i>
                                        <input name="first_name" type="text" class="form-control border-color-1" id="first_name" placeholder="First name" required autofocus>
                                        @include('partials.error_block', ['item' => 'first_name'])
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <input name="email" type="text" class="form-control border-color-2" id="email" placeholder="Email address">
                                        @include('partials.error_block', ['item' => 'email'])
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <input name="phone" type="text" class="form-control border-color-5" id="phone" placeholder="Phone">
                                        @include('partials.error_block', ['item' => 'phone'])
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                        <input name="subject" type="text" class="form-control border-color-6" id="subject" placeholder="Subject">
                                        @include('partials.error_block', ['item' => 'subject'])
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                        <i class="fa fa-comments" aria-hidden="true"></i>
                                        <textarea class="form-control border-color-4" placeholder="Write message" name="message" id="message"></textarea>
                                        @include('partials.error_block', ['item' => 'message'])
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="formBtnArea">
                                        <button type="submit" class="btn btn-primary">Send Message</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection