@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Edit Monthly Report
@endsection

@section('topButton')
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- State --}}
                {{--<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">--}}
                {{--<label for="state" class="col-md-4 control-label">--}}
                {{--State--}}
                {{--<span class="text-danger"> * </span>--}}
                {{--</label>--}}
                {{--<div class="col-md-6">--}}
                {{--<select name="state" class="form-control" onchange="loadCourse(this.value)">--}}
                {{--<option selected>Select one..</option>--}}
                {{--@foreach ($sessions as $session)--}}
                {{--<option @if (old('state') == $session->state) selected--}}
                {{--@endif value="{{ $session->course_id }}">{{ \App\State::getValue($session->state) }}</option>--}}
                {{--@endforeach--}}
                {{--</select>--}}
                {{--@include('partials.error_block', ['item' => 'state'])--}}
                {{--</div>--}}
                {{--</div>--}}

                {{-- course_name --}}
                <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                    <label for="course" class="col-md-4 control-label">
                        Course Name
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="course" class="form-control" id="courseSelectOption" onchange="loadSession(this.value)">
                            <option selected>Select one..</option>
                            @foreach ($courses as $course)
                                <option @if (old('course', $session->course_id) == $course->id) selected
                                        @endif value="{{ $course->id }}">{{ $course->name }} [{{\App\SubField::getValue($course->sub_field_id)}}]</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'course'])
                    </div>
                </div>

                {{-- session --}}
                <div class="form-group{{ $errors->has('session') ? ' has-error' : '' }}">
                    <label for="session" class="col-md-4 control-label">
                        Session
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="session" class="form-control" id="sessionSelectOption">
                            <option selected>Select one..</option>
                            @foreach ($sessions as $session)
                                <option @if (old('session', $session->id) == $session->id) selected
                                        @endif value="{{ $session->id }}">Session {{ $session->id }} [{{$session->year}}][{{$session->month}}][{{$session->venue}}]</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'session'])
                    </div>
                </div>

                {{-- Total Participant --}}
                <div class="form-group{{ $errors->has('total_participant') ? ' has-error' : '' }}">
                    <label for="total_participant" class="col-md-4 control-label">
                        Total Participant
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="number" class="form-control" placeholder="Total Participant" name="total_participant" value = "{{old('total_participant', $session->total_participant)}}">
                        </div>
                        @include('partials.error_block', ['item' => 'total_participant'])
                    </div>
                </div>

                {{-- Bil Peserta Jantina --}}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Number of Participants by Gender
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>MALE</label>
                                </div>
                                <div>
                                    <label>Age :</label>
                                    <div class="form-group{{ $errors->has('man_age1518') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="man_age1518">15-18</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="man_age1518" value="{{old('man_age1518', $session->man_age1518)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'man_age1518'])
                                    </div>

                                    <div class="form-group{{ $errors->has('man_age1925') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="man_age1925">19-25</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="man_age1925" value="{{old('man_age1925', $session->man_age1925)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'man_age1925'])
                                    </div>

                                    <div class="form-group{{ $errors->has('man_age2640') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="man_age2640">26-40</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="man_age2640" value="{{old('man_age2640', $session->man_age2640)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'man_age2640'])
                                    </div>

                                    <div class="form-group{{ $errors->has('man_age41') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="man_age41"> >41 </span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="man_age41" value="{{old('man_age41', $session->man_age41)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'man_age41'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>FEMALE</label>
                                </div>
                                <div class="form-group">
                                    <label>Age :</label>
                                    <div class="form-group{{ $errors->has('woman_age1518') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="woman_age1518">15-18</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="woman_age1518" value="{{old('woman_age1518', $session->woman_age1518)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'woman_age1518'])
                                    </div>

                                    <div class="form-group{{ $errors->has('woman_age1925') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="woman_age1925">19-25</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="woman_age1925" value="{{old('woman_age1925', $session->woman_age1925)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'woman_age1925'])
                                    </div>

                                    <div class="form-group{{ $errors->has('woman_age2640') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="woman_age2640">26-40</span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="woman_age2640" value="{{old('woman_age2640', $session->woman_age2640)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'woman_age2640'])
                                    </div>

                                    <div class="form-group{{ $errors->has('woman_age41') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="woman_age41"> >41 </span>
                                            <input type="number" class="form-control" placeholder="Total"
                                                   name="woman_age41" value="{{old('woman_age41', $session->woman_age41)}}">
                                        </div>
                                        @include('partials.error_block', ['item' => 'woman_age41'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Bil Peserta Kategori Peserta --}}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Number of Participants by Participant Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('students') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="students">Students</span>
                                        <input class="form-control" name="students" placeholder="Total" type="number" value = "{{old('students', $session->students)}}">
                                    </div>
                                    @include('partials.error_block', ['item' => 'students'])
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('school_leavers') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="school_leavers">School Leavers</span>
                                        <input class="form-control" name="school_leavers" placeholder="Total" type="number" value = "{{old('school_leavers', $session->school_leavers)}}">
                                    </div>
                                    @include('partials.error_block', ['item' => 'school_leavers'])
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('graduates') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="graduates">Graduates</span>
                                        <input class="form-control" name="graduates" placeholder="Total" type="number" value = "{{old('graduates', $session->graduates)}}">
                                    </div>
                                    @include('partials.error_block', ['item' => 'graduates'])
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('existing_employee') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="existing_employee">Existing Employees</span>
                                        <input class="form-control" name="existing_employee" placeholder="Total" type="number" value = "{{old('existing_employee', $session->existing_employee)}}">
                                    </div>
                                    @include('partials.error_block', ['item' => 'existing_employee'])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Bil Peserta Kaum Peserta --}}
                <div class="panel panel-primary">
                    <div class="panel-heading"> Number of Participants by Race </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('malay') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="malay">Malay</span>
                                        <input name="malay" type="number" class="form-control" placeholder = "Total" value = "{{old('malay', $session->malay)}}">
                                        @include('partials.error_block', ['item' => 'malay'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('chinese') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="chinese">Chinese</span>
                                        <input name="chinese" type="number" class="form-control" placeholder = "Total" value = "{{old('chinese', $session->chinese)}}">
                                        @include('partials.error_block', ['item' => 'chinese'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('indian') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="indian">Indian</span>
                                        <input name="indian" type="number" class="form-control" placeholder = "Total" value = "{{old('indian', $session->indian)}}">
                                        @include('partials.error_block', ['item' => 'indian'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('dayak') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="dayak">Dayak</span>
                                        <input name="dayak" type="number" class="form-control" placeholder = "Total" value = "{{old('dayak', $session->dayak)}}">
                                        @include('partials.error_block', ['item' => 'dayak'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('others') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="others">Others</span>
                                        <input name="others" type="number" class="form-control" placeholder = "Total" value = "{{old('others', $session->others)}}">
                                        @include('partials.error_block', ['item' => 'others'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Bil Syarikat --}}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Company
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('sme') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sme">SME</span>
                                        <input class="form-control" id="sme" name="sme" placeholder="Total" type="number" value="{{old('sme', $session->sme)}}">
                                        @include('partials.error_block', ['item' => 'sme'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('big_company') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="big_company">Big Company</span>
                                        <input class="form-control" id="big_company" name="big_company" placeholder="Total" type="number" value="{{old('big_company', $session->big_company)}}">
                                        @include('partials.error_block', ['item' => 'big_company'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-9">
                        <button type="submit" class="btn btn-primary">
                            Edit Monthly Report Record
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/monthlyreport/ajax/get-dept/'+ministryId);
        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/monthlyreport/ajax/get-agency/'+agencyId);
        }

        function loadCourse(courseId)
        {
            console.log(courseId);
            $('#courseSelectOption').load('/admin/monthlyreport/ajax/get-course/'+courseId);
        }

        function loadSession(sessionId)
        {
            console.log(sessionId);
            $('#sessionSelectOption').load('/admin/monthlyreport/ajax/get-session/'+sessionId);
        }
    </script>
@endsection