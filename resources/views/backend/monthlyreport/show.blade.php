@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Details of Monthly Report
@endsection

@section('topButton')
    <a href="/admin/monthlyreport" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Monthly Reports</span>
    </a>
    <a href="/admin/monthlyreport/edit/{{$monthlyReport->id}}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i>
        <span>Edit Monthly Report</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <a href="#">
                        <img class="img-responsive img-thumbnail" src="{{ \App\Course::getValue1($monthlyReport->course_id) }}" alt="image">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Monthly Report Informations</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped table-hover ">
                                <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th class="text-capitalize">{{ \App\Course::getValue($monthlyReport->course_id) }} [{{\App\Organization::getValue1(\App\Course::getValue2($monthlyReport->course_id))}}]</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td>Session</td>
                                    <td>Session {{ $monthlyReport->id }} [{{$monthlyReport->year}}][{{$monthlyReport->month}}][{{$monthlyReport->venue}}]</td>
                                </tr>
                                <tr class="success">
                                    <td>Participants by Gender</td>
                                    <td>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Age 15-18: <strong>{{ $monthlyReport->man_age1518 }}</strong></td>
                                                <td>Age 15-18: <strong>{{ $monthlyReport->woman_age1518 }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Age 19-25: <strong>{{ $monthlyReport->man_age1925 }}</strong></td>
                                                <td>Age 19-25: <strong>{{ $monthlyReport->woman_age1925 }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Age 26-40: <strong>{{ $monthlyReport->man_age2640 }}</strong></td>
                                                <td>Age 26-40: <strong>{{ $monthlyReport->woman_age2640 }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Age >41: <strong>{{ $monthlyReport->man_age41 }}</strong></td>
                                                <td>Age >41: <strong>{{ $monthlyReport->woman_age41 }}</strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="warning">
                                    <td>Participants by Participant Category</td>
                                    <td>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Participant Category</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Students: <strong>{{ $monthlyReport->students }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>School Leavers: <strong>{{ $monthlyReport->school_leavers }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Graduates: <strong>{{ $monthlyReport->graduates }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Existing Employees: <strong>{{ $monthlyReport->existing_employee }}</strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="danger">
                                    <td>Participants by Race</td>
                                    <td>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Participant Race</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Malay: <strong>{{ $monthlyReport->malay }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Chinese: <strong>{{ $monthlyReport->chinese }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Indian: <strong>{{ $monthlyReport->indian }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Dayak: <strong>{{ $monthlyReport->dayak }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Others: <strong>{{ $monthlyReport->others }}</strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>Total Participants</td>
                                    <td><strong>{{$monthlyReport->total_participant}}</strong></td>
                                </tr>
                                <tr class="info">
                                    <td>SME</td>
                                    <td>{{$monthlyReport->sme}}</td>
                                </tr>
                                <tr class="success">
                                    <td>Big Company</td>
                                    <td>{{$monthlyReport->big_company}}</td>
                                </tr>
                                <tr class="warning">
                                    <td>Created At</td>
                                    <td>{{$monthlyReport->created_at->format('d M, Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Updated At</td>
                                    <td>{{$monthlyReport->updated_at->format('d M, Y')}}</td>
                                </tr>
                                </tbody>
                            </table>

                            {{--Submit Button--}}
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-7">
                                    <a href="/admin/monthlyreport/edit/{{$monthlyReport->id}}" type="submit" class="btn btn-primary" role="button">Edit Monthly Report Record</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection