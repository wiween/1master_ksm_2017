@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Upload Excel Data - Monthly Report
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>List Upload</span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-primary"></i>
        <span>New</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
            {{-- Avatar / Photo --}}
            <div class="form-group{{ $errors->has('filepdf') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">
                    File Excel
                    <span class="text-danger"> * </span>
                </label>
                <div class="col-md-6">
                    <input type="file" class="form-control" name="filepdf" required>
                    @include('partials.error_block', ['item' => 'filepdf'])
                </div>
            </div>
                {{-- Course --}}
                <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Course
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="course_id" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($courses as $course)
                                <option @if (old('course_id')) selected @endif
                                value="{{ $course->id }}">{{ $course->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>
                {{-- Session --}}
                <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Session
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="course_id" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($courses as $course)
                                <option @if (old('course_id')) selected @endif
                                value="{{ $course->id }}">{{ $course->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>


                {{-- Year --}}
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Year
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="year" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($years as $year)
                                <option @if (old('year', \Carbon\Carbon::now()->year == $year)) selected @endif
                                value="{{ $year }}">{{ $year }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>
                {{-- month --}}
                <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                    <label for="month" class="col-md-4 control-label">
                        Month
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="month" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($months as $month)
                                <option @if (old('month', $currentMonth) == $month) selected @endif
                                value="{{ $month }}">{{ $month }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'month'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                           Upload File
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/target/ajax/get-dept/'+ministryId);
        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/target/ajax/get-agency/'+agencyId);
        }
    </script>
@endsection