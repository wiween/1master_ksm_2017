@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Monthly Report
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-sunflower"></i>
        <span>New Monthly Report</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Course Name</th>
                            <th>Session</th>
                            <th>Total Participant</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($monthlyReports as $monthlyReport)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td class="text-capitalize"><a href="/admin/monthlyreport/show/{{$monthlyReport->id}}">{{ \App\Course::getValue($monthlyReport->course_id) }} [{{\App\Organization::getValue1(\App\Course::getValue2($monthlyReport->course_id))}}]</a></td>
                                <td>Session {{ $monthlyReport->id }} [{{$monthlyReport->year}}][{{$monthlyReport->month}}][{{$monthlyReport->venue}}]</td>
                                <td>{{ $monthlyReport->total_participant }}</td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/monthlyreport/show/{{ $monthlyReport->id }}"><i class="icon-display"></i> View</a></li>
                                                <li><a href="/admin/monthlyreport/edit/{{ $monthlyReport->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/monthlyreport/destroy/{{ $monthlyReport->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection