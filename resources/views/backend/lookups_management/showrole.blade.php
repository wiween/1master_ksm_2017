@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Detail of {{ $role->name }}
@endsection

@section('topButton')
    <a href="/admin/lookups_management/role" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All role</span>
    </a>
    <a href="/admin/lookups_management/editrole/{{ $role->id }}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i>
        <span>Edit</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{ $role->name }}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">
                            @if ($role->status == 'active')
                                <span class="label label-success">{{ $role->status }}</span>
                            @elseif($role->status == 'banned')
                                <span class="label label-warning">{{ $role->status }}</span>
                            @else
                                <span class="label label-default">{{ $role->status }}</span>
                            @endif
                        </td>
                    </tr>


                </table>
                <br>
                <a href="/admin/lookups_management/showrole/{{ $role->id }}" class="btn btn-primary btn-block">Update role</a>

            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection











