@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    User
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity </span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-sunflower"></i>
        <span>New Course</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
           
           
           </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection