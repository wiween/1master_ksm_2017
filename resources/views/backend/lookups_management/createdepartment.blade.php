@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Department
@endsection

@section('topButton')
    <a href="/admin/lookups/ministry" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>List Department</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{-- parent --}}
                <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                    <label for="value" class="col-md-2 control-label">
                        Department Name
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-10">
                        <input name="value" type="text" class="form-control" value="{{ old('value') }}" required autofocus>
                        @include('partials.error_block', ['item' => 'value'])
                    </div>
                </div>
                <div class="form-group{{ $errors->has('key') ? ' has-error' : '' }}">
                    <label for="key" class="col-md-2 control-label">
                        Department Code
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-10">
                        <input name="key" type="text" class="form-control" value="{{ old('key') }}" required>
                        @include('partials.error_block', ['item' => 'key'])
                    </div>
                </div>
                {{--Ministry--}}
                <div class="form-group{{$errors->has('ministry') ? 'has-error' : ''}}">
                    <label for="ministry" class="col-lg-2 control-label">Organizer<span class="text-danger"> *</span></label>
                    <div class="col-lg-10">
                        <select class="form-control" name="ministry">
                            <option selected>Select ministry...</option>
                            @foreach ($ministries as $ministry)
                                <option @if (old('ministry')== $ministry->name) selected @endif value="{{$ministry->name}}">{{$ministry->name}}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block',['item'=> 'ministry'])
                    </div>
                </div>
                {{--Status--}}
                <div class="form-group{{$errors->has('status') ? 'has-error' : ''}}">
                    <label for="status" class="col-lg-2 control-label">Status<span class="text-danger"> *</span></label>
                    <div class="col-lg-10">
                        <select class="form-control" name="status">
                            <option selected>Select one...</option>
                            @foreach ($statuses as $status)
                                <option @if (old('status')== $status->value) selected @endif value="{{$status->key}}">{{$status->value}}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block',['item'=> 'status'])
                    </div>
                </div>




                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Create Department
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer_script')
@endsection