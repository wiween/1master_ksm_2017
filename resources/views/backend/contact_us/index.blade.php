@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Contact Us Feedback
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity </span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-sunflower"></i>
        <span>New Course</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Subject</th>
                            <!--<th>Message</th>-->
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($contactuses as $contactus)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td><a href="/admin/contact-us/show/{{ $contactus->id }}">{{ $contactus->first_name }}</a> </td>
                                <td><a href="mailto:{{ $contactus->email }}">{{ $contactus->email }}</a></td>
                                <td><a href="tel:{{ $contactus->phone }}">{{ $contactus->phone }}</a></td>
                                <td>{{ $contactus->subject }}</td>
                                <!--<td class="text-capitalize">{{ $contactus->message }}</td>-->
                                <td>
                                    @if ($contactus->status == 'open')
                                        <span class="label label-success">{{ $contactus->status }}</span>
                                    @elseif($contactus->status == 'close')
                                        <span class="label label-warning">{{ $contactus->status }}</span>
                                    @else
                                        <span class="label label-default">{{ $contactus->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/contact-us/set/open/{{ $contactus->id }}"><i class="icon-flag3 text-success"></i> Set to Open</a></li>
                                                <li><a href="/admin/contact-us/set/close/{{ $contactus->id }}"><i class="icon-flag7 text-warning"></i> Set to Close</a></li>
                                                <li><a href="/admin/contact-us/destroy/{{ $contactus->id }}"><i class="icon-trash text-danger-600"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection