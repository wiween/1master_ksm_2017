@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Show Contact Us Detail
@endsection

@section('topButton')
    @if (Auth::user()->role == 'super_admin')
        <a href="/admin/contact-us" class="btn btn-link btn-float has-text">
            <i class="icon-list-numbered text-primary"></i>
            <span>All Feedback</span>
        </a>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-3">
                {{-- PHOTO HERE--}}
            </div>
            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped table-bordered">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{$contactus->first_name }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-3">Email</th>
                        <td><a href="mailto:{{ $contactus->email }}">{{$contactus->email }}</a></td>
                    </tr>

                    <tr>
                        <th class="col-md-3">Phone</th>
                        <td><a href="tel:{{ $contactus->phone }}">{{ $contactus->phone }}</a></td>
                    </tr>

                    <tr>
                        <th class="col-md-3">Subject</th>
                        <td>{{ $contactus->subject }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-3">Message</th>
                        <td>{{ $contactus->message }}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">
                            @if ($contactus->status == 'open')
                                <span class="label label-success">{{ $contactus->status }}</span>
                            @elseif($contactus->status == 'close')
                                <span class="label label-warning">{{ $contactus->status }}</span>
                            @else
                                <span class="label label-default">{{ $contactus->status }}</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>
                            {{ $contactus->created_at->format('d M, Y') }}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>
                            {{ $contactus->updated_at->format('d M, Y') }}
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


@endsection

@section('footer_script')
@endsection