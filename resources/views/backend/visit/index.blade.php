@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Visit Report
@endsection

@section('topButton')
    <a href="/admin/visit/create" class="btn btn-link btn-float has-text">
        <i class="fa fa-bookmark-o text-primary"></i>
        <span>Add Visit</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Ministry</th>
                            <th>Visit</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($visits as $visit)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $visit->year }} </td>
                                <td>{{ $visit->month }}</td>
                                <td>{{ $visit->organization->name }}</td>
                                <td>{{ $visit->hit }}</td>

                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">

                                                <li><a href="/admin/visit/edit/{{ $visit->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/visit/destroy/{{ $visit->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection