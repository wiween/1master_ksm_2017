@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Edit Visit
@endsection

@section('topButton')
    <a href="/admin/dashboard" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity </span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}

                {{-- year --}}
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Year
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="year" type="text" class="form-control" value="{{ old('year', $visit->year) }}">
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>

                {{-- Month --}}
                <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                    <label for="month" class="col-md-4 control-label">
                        Month
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="month" type="text" class="form-control" value="{{ old('month', $visit->month) }}">
                        @include('partials.error_block', ['item' => 'month'])
                    </div>
                </div>

                {{-- ministry--}}
                <div class="form-group{{ $errors->has('ministry') ? ' has-error' : '' }}">
                    <label for="ministry" class="col-md-4 control-label">
                        Ministry
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        {{ \App\Organization::getMinistry($visit->organization_id)->name }}
                    </div>
                </div>

                {{-- department--}}
                <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                    <label for="department" class="col-md-4 control-label">
                        Department
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        {{ \App\Organization::getDepartment($visit->organization_id)->name }}
                    </div>
                </div>

                  {{-- visit --}}
                <div class="form-group{{ $errors->has('hit') ? ' has-error' : '' }}">
                    <label for="hit" class="col-md-4 control-label">
                        Total Visit
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="hit" type="text" class="form-control" value="{{ old('hit', $visit->hit) }}">
                        @include('partials.error_block', ['item' => 'hit'])
                    </div>
                </div>


                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update Visit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection