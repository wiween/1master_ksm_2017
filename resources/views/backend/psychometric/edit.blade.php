@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Update Psychometric Result
@endsection

@section('topButton')
    <a href="/admin/psychometric/form" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Result</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-3">
                        Name
                    </label>
                    <div class="col-md-6">
                        <td>{{ $psychometric->user->name }}</td>
                    </div>
                </div>

                {{-- IC Number --}}
                <div class="form-group{{ $errors->has('ic_number') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-3">
                        IC Number
                    </label>
                    <div class="col-md-6">
                        <td>{{ $psychometric->user->ic_number }}</td>
                    </div>
                </div>

                {{-- Email --}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-3">
                        Email
                    </label>
                    <div class="col-md-6">
                        <td>{{ $psychometric->user->email }}</td>
                    </div>
                </div>

                {{-- Field --}}
                <div class="form-group{{ $errors->has('field') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label">
                        Field
                    </label>
                    <div class="col-md-4">
                        <select name="field" class="form-control">
                            @foreach ($psychoList as $psy)
                            <option @if (old('field', $psychometric->field) == $psy) selected @endif value="{{ $psy }}">
                                {{ $psy }}
                            </option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'field'])
                    </div>
                </div>

                {{-- Category--}}
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label">
                        Category
                    </label>
                    <div class="col-md-4">
                        <select name="category" class="form-control">
                            @foreach ($psychoCategory as $cat)
                                <option @if (old('field', $psychometric->category) == $cat) selected @endif value="{{ $cat }}">
                                    {{ $cat }}
                                </option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'category'])
                    </div>
                </div>

                {{-- score --}}
                <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label">Score</label>
                    <div class="col-md-4">
                        <input type="text" value="{{ old('score', $psychometric->score) }}" class="form-control" name="score">
                        @include('partials.error_block', ['item' => 'score'])
                    </div>
                </div>
                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">
                            Update Record
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection