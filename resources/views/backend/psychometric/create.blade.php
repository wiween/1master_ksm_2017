@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Psychometric Result Form
@endsection

@section('topButton')

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

               {{-- IC --}}
                <div class="form-group{{ $errors->has('ic_number') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        IC Number
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="ic_number" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($users as $user)
                                <option @if (old('ic_number') == $user->ic_number) selected @endif value="{{ $user->id }}">{{ $user->ic_number }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'ic_number'])
                    </div>
                </div>


               {{-- Field --}}
                <div class="form-group{{ $errors->has('field') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                       Field
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="field" class="form-control">
                            <option selected>Select one..</option>
                            <option value="admin">Admin</option>
                            <option value="engineering">Engineering</option>
                            <option value="finance">Finance</option>
                            <option value="legal">Legal</option>
                            {{--@foreach ($statuses as $status)--}}
                                {{--<option @if (old('status') == $status->key) selected @endif value="{{ $status->key }}">{{ $status->value }}</option>--}}
                            {{--@endforeach--}}
                        </select>
                        @include('partials.error_block', ['item' => 'field'])
                    </div>
                </div>

                {{-- Category--}}
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Category
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="category" class="form-control">
                            <option selected>Select one..</option>
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low">Low</option>
                            {{--@foreach ($roles as $role)--}}
                                {{--<option @if (old('role') == $role->name) selected @endif value="{{ $role->name }}">{{ $role->nick }}</option>--}}
                            {{--@endforeach--}}
                        </select>
                        @include('partials.error_block', ['item' => 'category'])
                    </div>
                </div>

                {{-- score --}}
                <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Score</label>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('score') }}" class="form-control" name="score">
                        @include('partials.error_block', ['item' => 'score'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Create New User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection