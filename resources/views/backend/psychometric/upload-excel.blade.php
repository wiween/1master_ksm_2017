@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Upload Excel File - Psychometric Result
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-primary"></i>
        <span>Download</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
            {{-- fileexcel--}}
            <div class="form-group{{ $errors->has('fileexcel') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">
                    File Excel
                    <span class="text-danger"> * </span>
                </label>
                <div class="col-md-6">
                    <input type="file" class="form-control" name="fileexcel" required>
                    @include('partials.error_block', ['item' => 'fileexcel'])
                </div>
            </div>
               {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                           Upload File
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer_script')
@endsection