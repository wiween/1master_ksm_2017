@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    List Uploaded PDF File
@endsection

@section('topButton')
    <a href="/admin/psychometric/pdf/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New upload </span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>File</th>
                            <th>Email</th>
                            <th>Uploaded By</th>
                            <th>Date Uploaded</th>
                        </tr>
                        @foreach ($uploads as $upload)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td><a href="{{ $upload->filepdf }}" target="_blank">{{ $upload->filepdf }}</a></td>
                                <td>{{ $upload->user_id }}</td>
                                <td>{{ $upload->uploaded_by }}</td>
                                <td>{{ $upload->created_at->format('d M Y') }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection