@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Upload PDF File - Psychometric Result
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>List Upload</span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-primary"></i>
        <span>New Psychometric</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
            {{-- ic no --}}
            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">
                    Email
                    <span class="text-danger"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="user_id" class="form-control">
                        <option selected>Select one..</option>
                        @foreach ($users as $user)
                            <option @if (old('user') == $user->id) selected
                                    @endif value="{{ $user->id }}">{{ $user->email }}</option>
                        @endforeach
                    </select>
                    @include('partials.error_block', ['item' => 'user_id'])
                </div>
            </div>
                {{-- pdffile --}}
                <div class="form-group{{ $errors->has('filepdf') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        File PDF
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="filepdf" required>
                        @include('partials.error_block', ['item' => 'filepdf'])
                    </div>
                </div>
                {{-- Remark --}}
                <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Remark</label>
                    <div class="col-md-6">
                        <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                           Upload File
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer_script')
@endsection