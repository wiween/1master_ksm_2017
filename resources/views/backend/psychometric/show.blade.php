@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Psychometric Result of {{ $psychometric->user->name }}
@endsection

@section('topButton')
    <a href="/admin/psychometric/form" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Result</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-3">
                {{-- PHOTO HERE--}}
                <img src="{{ $psychometric->user->avatar }}" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{ $psychometric->user->name }}</td>
                    </tr>
                    <tr>
                        <th>IC Number</th>
                        <td>{{ $psychometric->user->ic_number }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $psychometric->user->email }}</td>
                    </tr>
                    <tr>
                        <th>Field</th>
                        <td>
                            {{ $psychometric->field }}
                        </td>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <td>
                            {{ $psychometric->category }}
                        </td>
                    </tr>
                    <tr>
                        <th>Score</th>
                        <td>
                            {{ $psychometric->score }}
                        </td>
                    </tr>
                </table>
                <br>
                <a href="/admin/psychometric/form/edit/{{ $psychometric->id }}" class="btn btn-primary btn-block">Edit Psychometric Record</a>

            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection













