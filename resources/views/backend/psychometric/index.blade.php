@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Psychometric
@endsection

@section('topButton')
    <a href="/admin/psychometric/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Result</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>IC Number</th>
                            <th>Phone</th>
                            <th>Field</th>
                            <th>Category</th>
                            <th>Score</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($psychometrics as $psychometric)
                          <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td><a href="/admin/psychometric/show/{{ $psychometric->id }}">{{ $psychometric->user->name }}</a></td>
                                <td>{{ $psychometric->user->email }}</td>
                                <td>{{ $psychometric->user->ic_number }}</td>
                                <td>{{ $psychometric->user->phone_number }}</td>
                                <td>{{ $psychometric->field }}</td>
                                <td>{{ $psychometric->category }}</td>
                                <td>{{ $psychometric->score }}</td>
                                <td>
                                    @if ($psychometric->status == 'active')
                                        <span class="label label-success">{{ $psychometric->status }}</span>
                                    @elseif($psychometric->status == 'banned')
                                        <span class="label label-warning">{{ $psychometric->status }}</span>
                                    @else
                                        <span class="label label-default">{{ $psychometric->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/psychometric/form/show/{{ $psychometric->id }}"><i class="icon-display"></i> View</a></li>
                                                <li><a href="/admin/psychometric/form/edit/{{ $psychometric->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/psychometric/form/destroy/{{ $psychometric->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_script')
@endsection