@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    All Photos
@endsection

@section('topButton')
    <a href="/admin/gallery/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>Add Photo</span>
    </a>

@endsection


@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Agency</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($galleries as $gallery)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td class="col-md-1">
                                <img class="img-responsive img-thumbnail" src="{{ $gallery->image }}">
                            </td>
                            {{--<td>{{ $gallery->ministry->name }} </td>--}}
                            {{--<td>{{ $gallery->department->name }} </td>--}}
                            <td>{{ $gallery->organization->name }}</td>
                            <td>{{ $gallery->remark }}</td>
                            <td>
                                @if ($gallery->status == 'active')
                                    <span class="label label-success">{{ $gallery->status }}</span>
                                @elseif($gallery->status == 'banned')
                                    <span class="label label-warning">{{ $gallery->status }}</span>
                                @else
                                    <span class="label label-default">{{ $gallery->status }}</span>
                                @endif
                            </td>
                            <td>
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="/admin/gallery/edit/{{ $gallery->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                            <li><a href="/admin/gallery/destroy/{{ $gallery->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            <li><a href="gallery/set/active/{{ $gallery->id }}"><i class="icon-flag3 text-success"></i> Set to Active</a></li>
                                            <li><a href="gallery/set/inactive/{{ $gallery->id }}"><i class="icon-flag7 text-warning"></i> Set to Inactive</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </table>
              </div>
            </div>
        </div>
    </div>
@endsection

