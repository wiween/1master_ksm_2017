@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Update Photo
@endsection

@section('topButton')

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8">
                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        {{-- Avatar / Photo --}}
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">
                                Photo
                                <span class="text-danger"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="file" class="form-control" name="image">
                                @include('partials.error_block', ['item' => 'image'])
                            </div>
                        </div>

                        {{-- agency --}}
                        <div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">
                            <label for="agency" class="col-md-4">
                                Agency
                                <span class="text-danger"> * </span>
                            </label>
                            <div class="col-md-8">
                                <select name="agency" class="form-control" id="agencySelectOption">
                                    <option selected>Select one..</option>
                                    @foreach ($agencies as $agency)
                                        <option @if (old('agency', $gallery->organization_id) == $agency->id) selected
                                                @endif value="{{ $agency->id }}">{{ $agency->name }}</option>
                                    @endforeach
                                </select>
                                @include('partials.error_block', ['item' => 'agency'])
                            </div>
                        </div>

                        {{-- Status --}}
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label class="col-md-4">
                                Status
                                <span class="text-danger"> * </span>
                            </label>
                            <div class="col-md-8">
                                <select name="status" class="form-control">
                                    <option selected>Select one..</option>
                                    @foreach ($statuses as $status)
                                        <option @if (old('status', $gallery->status) == $status->key) selected @endif value="{{ $status->key }}">
                                            {{ $status->value }}
                                        </option>
                                    @endforeach
                                </select>

                                @include('partials.error_block', ['item' => 'status'])
                            </div>
                        </div>


                        {{-- Remark --}}
                        <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                            <label class="col-md-4">Remark</label>
                            <div class="col-md-8">
                                <textarea name="remark" class="form-control" rows="3">{{ old('remark', $gallery->remark) }}</textarea>
                            </div>
                            @include('partials.error_block', ['item' => 'remark'])
                        </div>

                        {{-- Submit Button --}}
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Gallery
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="col-md-4">
                    <img class="img-responsive img-thumbnail" src="{{ $gallery->image }}">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
//        function loadDept(ministryId)
//        {
//            $('#deptSelectOption').load('/admin/gallery/ajax/get-dept/'+ministryId);
//        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/gallery/ajax/get-agency/'+agencyId);
        }
    </script>
@endsection
