@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Add New Photo
@endsection

@section('topButton')
    <a href="/admin/gallery" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>All Photos</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Avatar / Photo --}}
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Photo [ size: 270 x 220 | type: jpg, png, gif ]
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="image" required>
                        @include('partials.error_block', ['item' => 'image'])
                    </div>
                </div>

                {{--ministry --}}
                {{--<div class="form-group{{ $errors->has('ministry') ? ' has-error' : '' }}">--}}
                    {{--<label for="department" class="col-md-4 control-label">--}}
                        {{--Ministry--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="ministry" class="form-control" onchange="loadDept(this.value)">--}}
                            {{--<option value="" selected>Select one..</option>--}}
                            {{--@foreach ($ministries as $ministry)--}}
                                {{--<option @if (old('ministry') == $ministry->id) selected--}}
                                        {{--@endif value="{{ $ministry->id }}">{{ $ministry->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'ministry'])--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{-- dept --}}
                {{--<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">--}}
                    {{--<label for="department" class="col-md-4 control-label">--}}
                        {{--Department--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="department" class="form-control" id="deptSelectOption" onchange="loadAgency(this.value)">--}}
                            {{--<option value="" selected>Select one..</option>--}}
                            {{--@foreach ($departments as $department)--}}
                                {{--<option @if (old('department') == $department->id) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'department'])--}}
                    {{--</div>--}}
                {{--</div>--}}

                 {{--agency --}}
                <div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">
                    <label for="agency" class="col-md-4 control-label">
                        Agency
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="agency" class="form-control" id="agencySelectOption">
                            <option selected>Select one..</option>
                            @foreach ($agencies as $agency)
                                <option @if (old('agency') == $agency->id) selected
                                        @endif value="{{ $agency->id }}">{{ $agency->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'agency'])
                    </div>
                </div>


                {{-- Status --}}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Status
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="status" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($statuses as $status)
                                <option @if (old('status') == $status->key) selected @endif value="{{ $status->key }}">{{ $status->value }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'status'])
                    </div>
                </div>

                {{-- Remark --}}
                <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Title</label>
                    <div class="col-md-6">
                        <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
//        function loadDept(ministryId)
//        {
//            $('#deptSelectOption').load('/admin/gallery/ajax/get-dept/'+ministryId);
//        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/gallery/ajax/get-agency/'+agencyId);
        }
    </script>
@endsection
