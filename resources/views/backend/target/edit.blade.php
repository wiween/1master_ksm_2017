@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Edit Target
@endsection

@section('topButton')
    <a href="/admin/target/create" class="btn btn-link btn-float has-text">
        <i class="icon-target text-primary"></i>
        <span>New Target</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}


                {{-- year --}}
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Year
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="year" type="text" class="form-control" value="{{ old('year', $target->year) }}">
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>

                 {{-- ministry--}}
                <div class="form-group{{ $errors->has('ministry') ? ' has-error' : '' }}">
                    <label for="ministry" class="col-md-4 control-label">
                        Ministry
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        {{ \App\Organization::getMinistry($target->organization_id)->name }}
                    </div>
                </div>

                 {{-- department--}}
                <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                    <label for="department" class="col-md-4 control-label">
                        Department
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                   {{ \App\Organization::getDepartment($target->organization_id)->name }}
                    </div>
                </div>


                 {{-- agency--}}
                {{--<div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">--}}
                    {{--<label for="agency" class="col-md-4 control-label">--}}
                        {{--Agency--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="agency" class="form-control">--}}
                            {{--<option selected>Select one..</option>--}}
                            {{--@foreach ($agencies as $agency)--}}
                                {{--<option @if (old('agency', $target->organization_id) == $agency->id) selected--}}
                                        {{--@endif value="{{ $agency->id }}">--}}
                                    {{--{{ $agency->name }}--}}
                                {{--</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'agency'])--}}
                    {{--</div>--}}
                {{--</div>--}}

                      {{-- visit --}}
                <div class="form-group{{ $errors->has('visit') ? ' has-error' : '' }}">
                    <label for="visit" class="col-md-4 control-label">
                       Target Visit
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="visit" type="text" class="form-control" value="{{ old('visit', $target->visit) }}">
                        @include('partials.error_block', ['item' => 'visit'])
                    </div>
                </div>

                {{-- participant --}}
                <div class="form-group{{ $errors->has('participant') ? ' has-error' : '' }}">
                    <label for="participant" class="col-md-4 control-label">
                        Target Participant
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="participant" type="text" class="form-control" value="{{ old('participant', $target->participant) }}">
                        @include('partials.error_block', ['item' => 'participant'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update Target
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection