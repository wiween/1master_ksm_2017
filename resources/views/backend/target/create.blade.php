@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Create Target
@endsection

@section('topButton')
    <a href="/admin/dashboard" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity </span>
    </a>

    <a href="/admin/target/create" class="btn btn-link btn-float has-text">
        <i class="icon-target2 text-sunflower"></i>
        <span>New Target</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}

                {{-- Year --}}
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Year
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="year" class="form-control">

                            <option value="" selected>Select one</option>
                            @foreach( $years as $year )
                                <option @if (old('year', \Carbon\Carbon::now()->year == $year)) selected @endif
                                values="{{ $year }}"> {{ $year }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>


                {{--ministry --}}
                <div class="form-group{{ $errors->has('ministry') ? ' has-error' : '' }}">
                    <label for="ministry" class="col-md-4 control-label">
                        Ministry
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="ministry" class="form-control" onchange="loadDept(this.value)">
                            <option value="" selected>Select one..</option>
                            @foreach ($ministries as $ministry)
                                <option @if (old('ministry') == $ministry->id) selected
                                        @endif value="{{ $ministry->id }}">{{ $ministry->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'ministry'])
                    </div>
                </div>

                {{-- dept --}}
                <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                    <label for="department" class="col-md-4 control-label">
                       Department
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        {{--<select name="department" class="form-control" onchange="loadAgency(this.value)">--}}
                            <select name="department" class="form-control">
                            <option value="" selected>Select one..</option>
                            @foreach ($departments as $department)
                                <option @if (old('department') == $department->id) selected
                                        @endif value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'department'])
                    </div>
                </div>

                {{-- agency --}}
                {{--<div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">--}}
                    {{--<label for="agency" class="col-md-4 control-label">--}}
                        {{--Agency--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="agency" class="form-control" id="agencySelectOption">--}}
                            {{--<option value="" selected>Select one..</option>--}}
                            {{--@foreach ($agencies as $agency)--}}
                                {{--<option @if (old('agency') == $agency->id) selected @endif value="{{ $agency->id }}">{{ $agency->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'agency'])--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{-- visit --}}
                <div class="form-group{{ $errors->has('visit') ? ' has-error' : '' }}">
                    <label for="visit" class="col-md-4 control-label">
                        Target Visit
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input value="{{ old('visit') }}" type="number" class="form-control" name="visit" required>
                        @include('partials.error_block', ['item' => 'visit'])
                    </div>
                </div>

                {{-- participant--}}
                <div class="form-group{{ $errors->has('participant') ? ' has-error' : '' }}">
                    <label for="participant" class="col-md-4 control-label">
                        Target Participant
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input value="{{ old('participant') }}" type="number" class="form-control" name="participant" required>
                        @include('partials.error_block', ['item' => 'participant'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Create New Target
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/target/ajax/get-dept/'+ministryId);
        }

//        function loadAgency(agencyId)
//        {
//            console.log(agencyId);
//            $('#agencySelectOption').load('/admin/target/ajax/get-agency/'+agencyId);
//        }
    </script>
@endsection