@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Target
@endsection

@section('topButton')
    <a href="/admin/target/create" class="btn btn-link btn-float has-text">
        <i class="icon-target2 text-primary"></i>
        <span>New Target</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Year</th>
                            <th>Ministry</th>
                            <th>Department</th>
                            {{--<th>Agency</th>--}}
                            <th>Target Visit</th>
                            <th>Target Participant</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($targets as $target)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $target->year }}</td>
                                <td>{{ \App\Organization::getMinistry($target->organization_id)->name }} </td>
                                {{--<td>{{ \App\Organization::getDepartment($target->organization_id)->name }} </td>--}}
                                <td>{{ $target->organization->name }}</td>
                                <td align="center">{{ $target->visit }}</td>
                                <td align="center" class="text-capitalize">{{ $target->participant }}</td>
                                <!--<td>
                                    @if ($target->status == 'open')
                                        <span class="label label-success">{{ $target->status }}</span>
                                    @elseif($target->status == 'close')
                                        <span class="label label-warning">{{ $target->status }}</span>
                                    @else
                                        <span class="label label-default">{{ $target->status }}</span>
                                    @endif
                                </td>-->
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <!--<li><a href="/admin/target/show/{{ $target->id }}"><i class="icon-display"></i> View</a></li>-->
                                                <li><a href="/admin/target/edit/{{ $target->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/target/destroy/{{ $target->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection