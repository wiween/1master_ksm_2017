@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Edit Activity
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity </span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-sunflower"></i>
        <span>New Course</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">
                        Name
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" type="text" class="form-control" value="{{ old('name', $activity->name) }}" required autofocus>
                        @include('partials.error_block', ['item' => 'name'])
                    </div>
                </div>


                {{-- Date Start --}}
                <div class="form-group{{ $errors->has('date_start') ? ' has-error' : '' }}">
                    <label for="date_start" class="col-md-4 control-label">
                        Date Start
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="date_start" type="date" class="form-control" value="{{ old('date_start',  $activity->date_start)->format('Y-m-d') }}" required>
                        @include('partials.error_block', ['item' => 'date_start'])
                    </div>
                </div>

                {{-- Date End --}}
                <div class="form-group{{ $errors->has('date_end') ? ' has-error' : '' }}">
                    <label for="date_end" class="col-md-4 control-label">
                        Date End
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="date_end" type="date" class="form-control" value="{{ old('date_end',  $activity->date_end->format('Y-m-d')) }}" required>
                        @include('partials.error_block', ['item' => 'date_end'])
                    </div>
                </div>

                {{-- Time --}}
                <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                    <label for="time" class="col-md-4 control-label">
                        Time
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="time" type="time" class="form-control" value="{{ old('time',  $activity->time) }}" required>
                        @include('partials.error_block', ['item' => 'time'])
                    </div>
                </div>

                {{-- ministry --}}
                <div class="form-group{{ $errors->has('ministry') ? ' has-error' : '' }}">
                    <label for="department" class="col-md-4 control-label">
                        Ministry
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="ministry" class="form-control" onchange="loadDept(this.value)">
                            <option value="" selected>Select one..</option>
                            @foreach ($ministries as $ministry)
                                <option @if (old('ministry',$activity->organization_id) == $ministry->id) selected
                                        @endif value="{{ $ministry->id }}">{{ $ministry->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'ministry'])
                    </div>
                </div>

                {{-- dept --}}
                {{--<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">--}}
                    {{--<label for="department" class="col-md-4 control-label">--}}
                        {{--Department--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="department" class="form-control" id="deptSelectOption" onchange="loadAgency(this.value)">--}}
                            {{--<option value="" selected>Select one..</option>--}}
                            {{--@foreach ($departments as $department)--}}
                                {{--<option @if (old('department') == $department->id) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'department'])--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{-- agency/venue --}}
                {{--<div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">--}}
                    {{--<label for="agency" class="col-md-4 control-label">--}}
                        {{--Venue--}}
                        {{--<span class="text-danger"> * </span>--}}
                    {{--</label>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<select name="agency" class="form-control" id="agencySelectOption">--}}
                            {{--<option selected>Select one..</option>--}}
                            {{--@foreach ($agencies as $agency)--}}
                                {{--<option @if (old('agency') == $agency->id) selected @endif value="{{ $agency->id }}">{{ $agency->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@include('partials.error_block', ['item' => 'agency'])--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{-- Venue --}}
                <div class="form-group{{ $errors->has('venue') ? ' has-error' : '' }}">
                    <label for="venue" class="col-md-4 control-label">
                        Venue
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="venue" type="text" class="form-control" value="{{ old('venue',  $activity->venue) }}" required>
                        @include('partials.error_block', ['item' => 'venue'])
                    </div>
                </div>

                {{-- Dirasmikan oleh --}}
                <div class="form-group{{ $errors->has('officiated_by') ? ' has-error' : '' }}">
                    <label for="officiated_by" class="col-md-4 control-label">
                        Officiated By
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="officiated_by" type="text" class="form-control" value="{{ old('officiated_by', $activity->officiated_by) }}">
                        @include('partials.error_block', ['item' => 'officiated_by'])
                    </div>
                </div>

                {{-- Participants--}}
                <div class="form-group{{ $errors->has('participant') ? ' has-error' : '' }}">
                    <label for="participant" class="col-md-4 control-label">
                        Participant
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="participant" type="number" class="form-control" value="{{ old('participant',$activity->participant)  }}" required>
                        @include('partials.error_block', ['item' => 'participant'])
                    </div>
                </div>

                {{-- Avatar / Photo --}}
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                      Photo
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="image" required>
                        @include('partials.error_block', ['item' => 'image'])
                    </div>
                </div>

                {{-- Audience --}}
                <div class="form-group{{ $errors->has('audience') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Targeted Participant
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="audience" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($audiences as $audience)
                                <option @if (old('audience',$activity->audience) == $audience->id) selected @endif value="{{ $audience->id}}">{{ $audience->value }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'audience'])
                    </div>
                </div>

                {{-- Status --}}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Status
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="status" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($statuses as $status)
                                <option @if (old('status',$activity->status) == $status->value) selected @endif value="{{ $status->key }}">{{ $status->value }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'status'])
                    </div>
                </div>

                {{-- Description --}}
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Description</label>
                    <div class="col-md-6">
                        <textarea name="description" class="form-control" rows="3">{{ old('description', $activity->description) }}</textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                {{-- Remark --}}
                <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Remark</label>
                    <div class="col-md-6">
                        <textarea name="remark" class="form-control" rows="3">{{ old('remark', $activity->remark) }}</textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update Activity
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('footer_script')
@endsection