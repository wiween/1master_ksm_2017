@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Detail of {{ $activity->name }}
@endsection

@section('topButton')
    <a href="/admin/activity" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Activities</span>
    </a>
    <a href="/admin/activity/edit/{{ $activity->id }}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i>
        <span>Edit</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-6">
                {{-- PHOTO HERE--}}
                <img src="{{ $activity->image }}" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{ $activity->name }}</td>
                    </tr>
                    <tr>
                        <th>Date Start</th>
                        <td>{{ $activity->date_start->format('d M, Y') }}</td>
                    </tr>
                    <tr>
                        <th>Date End</th>
                        <td>{{ $activity->date_end->format('d M, Y') }}</td>
                    </tr>
                    <tr>
                        <th>Time</th>
                        <td>{{ $activity->time }}</td>
                    </tr>
                    <tr>
                        <th>Ministry</th>
                        <td>{{ $activity->Organization->name }}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<th>Department</th>--}}
                        {{--<td>{{ $activity->ministry }}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <th>Venue</th>
                        <td>{{ $activity->venue }}</td>
                        {{--<td> {{$activity = Organization::where('type','agency')->where('status', 'active')->get();}} </td>--}}
                    </tr>
                    <tr>
                        <th>Officiated By</th>
                        <td>{{ $activity->officiated_by }}</td>
                    </tr>
                    <tr>
                        <th>Targeted Participant</th>
                        <td>{{  \App\Lookup::getValue($activity->audience) }}</td>
                    </tr>

                    <tr>
                        <th>Participant</th>
                        <td>{{ $activity->participant }}</td>
                    </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td class="text-capitalize">
                                                @if ($activity->status == 'Published')
                                                    <span class="label label-success">{{ $activity->status }}</span>
                                                @elseif($activity->status == 'Unpublished')
                                                    <span class="label label-warning">{{ $activity->status }}</span>
                                                @else
                                                    <span class="label label-default">{{ $activity->status }}</span>
                                                @endif
                                            </td>
                                        </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{ $activity->description }}</td>
                    </tr>

                    <tr>
                        <th>Remark</th>
                        <td>{{ $activity->remark }}</td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>
                            {{ $activity->created_at->format('d M, Y') }}
                        </td>
                    </tr>
                     <tr>
                        <th>Updated At</th>
                        <td>
                            {{ $activity->updated_at->format('d M, Y') }}
                        </td>
                    </tr>
                </table>
                <br>
                <a href="/admin/activity/edit/{{ $activity->id }}" class="btn btn-primary btn-block">Edit Activity Record</a>

            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection