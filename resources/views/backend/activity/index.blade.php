@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Activity
@endsection

@section('topButton')
    <a href="/admin/activity/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Date_Start</th>
                            <th>Date_End</th>
                            <th>Time</th>
                            <th>Venue</th>
                            <th>Targeted Participant</th>
                            <th>Participant</th>
                            <th>Ministry</th>
                            {{--<th>Dirasmi_Oleh</th>
                            <th>Description</th>--}}
                            <th>Status</th>
                            <th>Action</th>



                        </tr>
                        @foreach ($activities as $activity)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td class="col-md-1">
                                    <img class="img-responsive img-thumbnail" src="{{ $activity->image }}">
                                </td>
                                <td><a href="/activity/show/{{ $activity->id }}">{{ $activity->name }}</a> </td>
                                <td>{{ $activity->date_start->format('d M, Y') }}</td>
                                <td>{{ $activity->date_end->format('d M, Y') }}</td>
                                <td>{{ $activity->time }}</td>
                                <td>{{ $activity->venue }}</td>
{{--                                <td>{{ \App\Lookup::where('id', $activity->audience)->value('value') }}</td>--}}
                                <td>{{ \App\Lookup::getValue($activity->audience) }}</td>
                                <td>{{ $activity->participant }}</td>
                                <td>{{ $activity->organization->name }}</td>
    {{--<td>{{ $activity->dirasmi_oleh }}</td>
    <td>{{ $activity->description }}</td>--}}


    <td>

        @if ($activity->status == 'Published')
            <span class="label label-success">{{ $activity->status }}</span>
        @elseif($activity->status == 'Unpublished')
            <span class="label label-warning">{{ $activity->status }}</span>
        @else
            <span class="label label-default">{{ $activity->status }}</span>
        @endif
    </td>
    <td>
        <ul class="icons-list">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-menu9"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="activity/show/{{ $activity->id }}"><i class="icon-display"></i> View</a></li>
                    <li><a href="activity/edit/{{ $activity->id }}"><i class="icon-pencil"></i> Edit</a></li>
                    <li><a href="activity/set/Published/{{ $activity->id }}"><i class="icon-flag3 text-success"></i> Set to Published</a></li>
                    <li><a href="activity/set/Unpublished/{{ $activity->id }}"><i class="icon-flag7 text-warning"></i> Set to Unpublished</a></li>
                    <li><a href="activity/destroy/{{ $activity->id }}"><i class="icon-trash"></i> Delete</a></li>
                </ul>
            </li>
        </ul>
    </td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
</div>
@endsection

@section('footer_script')
@endsection


