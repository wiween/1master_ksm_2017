@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Create New Session
@endsection

@section('topButton')
    <a href="/admin/session" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>All Sessions</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}

                {{-- course_name --}}
                <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                    <label for="course" class="col-md-4 control-label">
                        Course Name
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="course" class="form-control" id="courseSelectOption">
                            <option selected>Select one..</option>
                            @foreach ($courses as $course)
                                <option @if (old('course') == $course->id) selected
                                        @endif value="{{ $course->id }}">{{ $course->name }} [{{$course->organization->name}}]</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'course'])
                    </div>
                </div>

                {{-- YearS --}}
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label for="year" class="col-md-4 control-label">
                        Year
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="year" class="form-control" >
                            <option value="" selected>Select one..</option>
                            @foreach ($years as $year)
                                <option @if (old('year', \Carbon\Carbon::now()->year) == $year) selected
                                        @endif value="{{ $year }}">{{ $year }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'year'])
                    </div>
                </div>

                {{-- month --}}
                <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                    <label for="month" class="col-md-4 control-label">
                        Month
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="month" class="form-control" >
                            <option value="" selected>Select one..</option>
                            @foreach ($months as $month)
                                <option @if (old('month', $currentMonth) == $month) selected
                                        @endif value="{{ $month }}">{{ $month }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'month'])
                    </div>
                </div>

                {{-- State --}}
                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    <label for="state" class="col-md-4 control-label">
                        State
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="state" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($states as $state)
                                <option @if (old('state') == $state->id) selected
                                        @endif value="{{ $state->id }}">{{ $state->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'state'])
                    </div>
                </div>

                {{-- Venue --}}
                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    <label for="venue" class="col-md-4 control-label">
                        Venue
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input class="form-control" name="venue" value="{{old('venue')}}" type="text" >
                        @include('partials.error_block',['item'=> 'venue'])
                        <span class="help-block">Example: Dewan Serbaguna Sibu.</span>
                    </div>
                </div>

                {{-- Fee --}}
                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    <label for="fee" class="col-md-4 control-label">
                        Fee
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input class="form-control" name="fee" value="{{old('fee')}}" type="text" >
                        @include('partials.error_block',['item'=> 'fee'])
                        <span class="help-block">Example: 1200.00 without the RM</span>
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Create New
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/monthlyreport/ajax/get-dept/'+ministryId);
        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/monthlyreport/ajax/get-agency/'+agencyId);
        }

        function loadCourse(courseId)
        {
            console.log(courseId);
            $('#courseSelectOption').load('/admin/monthlyreport/ajax/get-course/'+courseId);
        }
    </script>
@endsection