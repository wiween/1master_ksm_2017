@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Upload Excel Data - Monthly Report
@endsection

@section('topButton')
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>List Upload</span>
    </a>
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-primary"></i>
        <span>New</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
            {{-- Avatar / Photo --}}
            <div class="form-group{{ $errors->has('filepdf') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">
                    File Excel
                    <span class="text-danger"> * </span>
                </label>
                <div class="col-md-6">
                    <input type="file" class="form-control" name="filepdf" required>
                    @include('partials.error_block', ['item' => 'filepdf'])
                </div>
            </div>
                {{-- dept --}}
                <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                    <label for="department" class="col-md-4 control-label">
                        Department
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="department" class="form-control" id="deptSelectOption" onchange="loadAgency(this.value)">
                            <option value="" selected>Select one..</option>
                            @foreach ($departments as $department)
                                <option @if (old('department') == $department->id) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'department'])
                    </div>
                </div>

                {{-- agency --}}
                <div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">
                    <label for="agency" class="col-md-4 control-label">
                        Agency
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="agency" class="form-control" id="agencySelectOption">
                            <option selected>Select one..</option>
                            @foreach ($agencies as $agency)
                                <option @if (old('agency') == $agency->id) selected @endif value="{{ $agency->id }}">{{ $agency->name }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'agency'])
                    </div>
                </div>

                {{-- Remark --}}
                <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Remark</label>
                    <div class="col-md-6">
                        <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                           Upload File
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/target/ajax/get-dept/'+ministryId);
        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/target/ajax/get-agency/'+agencyId);
        }
    </script>
@endsection