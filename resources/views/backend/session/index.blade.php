@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    All Course Session
@endsection

@section('topButton')
    <a href="/admin/monthlyreport" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Monthly Report</span>
    </a>
    <a href="/admin/session/create" class="btn btn-link btn-float has-text">
        <i class="fa fa-id-card-o text-sunflower"></i>
        <span>New Session</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Course Name</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>State</th>
                            <th>Venue</th>
                            <th>Fee</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($sessions as $session)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td class="text-capitalize"><a href="/admin/session/show/{{$session->id}}">{{ \App\Course::getValue($session->course_id) }} [{{\App\Organization::getValue1(\App\Course::getValue2($session->course_id))}}]</a></td>
                                <td>{{ $session->year }}</td>
                                <td>{{ $session->month }}</td>
                                <td>{{ \App\State::getValue($session->state) }}</td>
                                <td class="text-capitalize">{{ $session->venue }}</td>
                                <td class="text-capitalize">RM{{ $session->fee }}</td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/session/show/{{ $session->id }}"><i class="icon-display"></i> View</a></li>
                                                <li><a href="/admin/session/edit/{{ $session->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/session/destroy/{{ $session->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection