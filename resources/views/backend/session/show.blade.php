@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Details of Session
@endsection

@section('topButton')
    <a href="/admin/session" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Sessions</span>
    </a>
    <a href="/admin/course/edit/{{$session->id}}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i>
        <span>Edit Session</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <a href="#">
                        <img class="img-responsive img-thumbnail" src="{{ \App\Course::getValue1($session->course_id) }}" alt="image">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Session Informations</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped table-hover ">
                                <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th class="text-capitalize">{{ \App\Course::getValue($session->course_id) }} [{{\App\Organization::getValue1(\App\Course::getValue2($session->course_id))}}]</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td>Year</td>
                                    <td>{{$session->year}}</td>
                                </tr>
                                <tr class="success">
                                    <td>Month</td>
                                    <td>{{$session->month}}</td>
                                </tr>
                                <tr class="warning">
                                    <td>State</td>
                                    <td>{{\App\State::getValue($session->state)}}</td>
                                </tr>
                                <tr class="danger">
                                    <td>Venue</td>
                                    <td>{{$session->venue}}</td>
                                </tr>
                                <tr class="active">
                                    <td>Fee</td>
                                    <td>RM{{$session->fee}}</td>
                                </tr>
                                <tr class="info">
                                    <td>Created At</td>
                                    <td>{{$session->created_at->format('d M, Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Updated At</td>
                                    <td>{{$session->updated_at->format('d M, Y')}}</td>
                                </tr>
                                </tbody>
                            </table>

                            {{--Submit Button--}}
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-7">
                                    <a href="/admin/session/edit/{{$session->id}}" type="submit" class="btn btn-primary" role="button">Edit Course Session Record</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
@endsection