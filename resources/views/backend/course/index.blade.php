@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Course
@endsection

@section('topButton')
    <a href="/admin/course/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Course</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Agency</th>
                            <th>Duration</th>
                            <th>Course Field</th>
                            <th># of Session</th>
                            <th>Action</th>
                        </tr>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td class="col-md-1">
                                    <img class="img-responsive img-thumbnail" src="{{$course->image}}">
                                </td>
                                <td class="text-capitalize"><a href="/admin/course/show/{{$course->id}}">{{$course->name}}</a></td>
                                <td>{{$course->organization->name}}</td>
                                <td class="text-capitalize">{{$course->duration}}</td>
                                <td>{{ \App\Field::getValue(\App\SubField::getValue1($course->sub_field_id))}}</td>
                                <td>
                                    @if ($course->session->count() == 0)
                                        <label class="label label-danger">None</label>
                                    @else
                                        <label class="label label-success">{{$course->session->count()}} Registered</label>
                                    @endif
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/course/show/{{$course->id}}"><i class="icon-display text-info"></i> View</a></li>
                                                <li><a href="/admin/course/edit/{{$course->id}}"><i class="icon-pencil text-primary"></i> Edit</a></li>
                                                <li><a href="/admin/course/destroy/{{$course->id}}"><i class="icon-trash text-danger"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>


                </div>

            </div>
        </div>
    </div>

@endsection