@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Confirm Record Deletion
@endsection

@section('topButton')

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <fieldset>
                        <legend>Enter your password to proceed with deletion.</legend>

                        {{--Name--}}
                        <div class="form-group{{$errors->has('password') ? 'has-error' : ''}}">
                            <label for="name" class="col-lg-2 control-label">Password<span class="text-danger"> *</span></label>
                            <div class="col-lg-10">
                                <input class="form-control" name="password" value="{{old('password')}}" type="text" >
                                @include('partials.error_block',['item'=> 'password'])
                            </div>
                        </div>

                        {{--Submit Button--}}
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')

@endsection