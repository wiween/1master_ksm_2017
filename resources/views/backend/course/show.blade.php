@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Details of {{ $course->name }}
@endsection

@section('topButton')
    <a href="/admin/course" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Courses</span>
    </a>
    <a href="/admin/course/edit/{{$course->id}}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i>
        <span>Edit Course</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <a href="#">
                        <img class="img-responsive img-thumbnail" src="{{$course->image}}" alt="image">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $course->name }} Informations</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped table-hover ">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th class="text-capitalize">{{$course->name}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td>Course Sub Field</td>
                                    <td>{{ \App\SubField::getValue($course->sub_field_id)}}</td>
                                </tr>
                                <tr class="success">
                                    <td>Agency</td>
                                    <td>{{$course->organization->name}}</td>
                                </tr>
                                <tr class="warning">
                                    <td>Course Introduction</td>
                                    <td>{{$course->introduction}}</td>
                                </tr>
                                <tr class="danger">
                                    <td>Potential Job</td>
                                    <td>{{$course->job}}</td>
                                </tr>
                                <tr class="active">
                                    <td>Potential Salary</td>
                                    <td>{{$course->salary}}</td>
                                </tr>
                                <tr class="info">
                                    <td>Course Objective</td>
                                    <td>{{$course->syllabus}}</td>
                                </tr>
                                {{--<tr class="active">--}}
                                    {{--<td>Course Fee</td>--}}
                                    {{--@if ($course->fee <> 'free' && $course->fee <> 'Free' && $course->fee <> 'FREE')--}}
                                        {{--<td>RM{{$course->fee}}</td>--}}
                                    {{--@else--}}
                                        {{--<td class="text-capitalize">{{$course->fee}}</td>--}}
                                    {{--@endif--}}
                                {{--</tr>--}}
                                <tr class="success">
                                    <td>Targeted Participant</td>
                                    <td class="text-capitalize">
                                        @if ($course->audience1 <> '' && $course->audience2 == '' && $course->audience3 == '' && $course->audience4 == '')
                                            {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}
                                        @elseif ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 == '' && $course->audience4 == '')
                                            {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}
                                        @elseif ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 <> '' && $course->audience4 == '')
                                            {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience3))}}
                                        @else ($course->audience1 <> '' && $course->audience2 <> '' && $course->audience3 <> '' && $course->audience4 <> '')
                                            {{str_replace('_', ' ', \App\Lookup::getValue($course->audience1))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience2))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience3))}}, {{str_replace('_', ' ', \App\Lookup::getValue($course->audience4))}}
                                        @endif
                                    </td>
                                </tr>
                                <tr class="warning">
                                    <td>Contact Person</td>
                                    <td><textarea class="form-control" rows="3" name="contact" disabled="">{{old('introduction', $course->contact)}}</textarea>
                                        @include('partials.error_block',['item'=> 'contact'])</td>
                                </tr>
                                <tr class="danger">
                                    <td>Status</td>
                                    <td>
                                        @if ($course->status == '4')
                                            <span class="label label-success">{{\App\Lookup::getValue($course->status)}}</span>
                                        @elseif($course->status == '5')
                                            <span class="label label-warning">{{\App\Lookup::getValue($course->status)}}</span>
                                        @elseif($course->status == '6')
                                            <span class="label label-danger">{{\App\Lookup::getValue($course->status)}}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>Created At</td>
                                    <td>{{$course->created_at->format('d M, Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Updated At</td>
                                    <td>{{$course->updated_at->format('d M, Y')}}</td>
                                </tr>
                                </tbody>
                            </table>

                            {{--Submit Button--}}
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-8">
                                    <a href="/admin/course/edit/{{$course->id}}" type="submit" class="btn btn-primary" role="button">Edit Course Record</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection