@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Create New Course
@endsection

@section('topButton')

@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">

            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <fieldset>
                    <legend>Create Course Form</legend>

                    {{--Name--}}
                    <div class="form-group{{$errors->has('name') ? 'has-error' : ''}}">
                        <label for="name" class="col-lg-2 control-label">Name<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <input class="form-control" name="name" value="{{old('name')}}" type="text" >
                            @include('partials.error_block',['item'=> 'name'])
                        </div>
                    </div>

                    {{--Course Field--}}
                    <div class="form-group{{$errors->has('field') ? 'has-error' : ''}}">
                        <label for="field" class="col-lg-2 control-label">Course Field<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="field" onchange="loadSubField(this.value)">
                                <option value="" selected>Select field...</option>
                                @foreach ($fields as $field)
                                    <option @if (old('field')== $field->id) selected @endif value="{{$field->id}}">{{$field->name}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'field'])
                        </div>
                    </div>

                    {{--Course Sub Field--}}
                    <div class="form-group{{$errors->has('sub_field') ? 'has-error' : ''}}">
                        <label for="sub_field" class="col-lg-2 control-label">Course Sub Field<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="sub_field" id="sub_fieldSelectOption">
                                <option value="" selected>Select sub field...</option>
                                @foreach ($sub_fields as $sub_field)
                                    <option @if (old('sub_field')== $sub_field->id) selected @endif value="{{$sub_field->id}}">{{$sub_field->name}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'sub_field'])
                        </div>
                    </div>

                    {{--Ministry--}}
                    <div class="form-group{{$errors->has('ministry') ? 'has-error' : ''}}">
                        <label for="ministry" class="col-lg-2 control-label">Organizer<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="ministry" onchange="loadDept(this.value)">
                                <option value="" selected>Select ministry...</option>
                                @foreach ($ministries as $ministry)
                                    <option @if (old('ministry')== $ministry->id) selected @endif value="{{$ministry->id}}">{{$ministry->name}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'ministry'])
                        </div>
                    </div>

                    {{-- department --}}
                    <div class="form-group{{$errors->has('department') ? 'has-error' : ''}}">
                        <label for="department" class="col-lg-2 control-label">Department<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="department" id="deptSelectOption" onchange="loadAgency(this.value)">
                                <option value="" selected>Select department...</option>
                                @foreach ($departments as $department)
                                    <option @if (old('department')== $department->id) selected @endif value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'department'])
                        </div>
                    </div>

                    {{--Agency--}}
                    <div class="form-group{{$errors->has('venue') ? 'has-error' : ''}}">
                        <label for="agency" class="col-lg-2 control-label">Agency<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="agency" id="agencySelectOption">
                                <option value="" selected>Select agency...</option>
                                @foreach ($agencies as $agency)
                                    <option @if (old('agency') == $agency->id) selected @endif value="{{ $agency->id }}">{{ $agency->name }}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'agency'])
                        </div>
                    </div>

                    {{--Course Introduction--}}
                    <div class="form-group{{$errors->has('introduction') ? 'has-error' : ''}}">
                        <label for="introduction" class="col-lg-2 control-label">Course Introduction</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" rows="3" name="introduction"></textarea>
                            @include('partials.error_block',['item'=> 'introduction'])
                            <span class="help-block">A little bit of introduction for the course.</span>
                        </div>
                    </div>

                    {{--Potential Job--}}
                    <div class="form-group{{$errors->has('job') ? 'has-error' : ''}}">
                        <label for="job" class="col-lg-2 control-label">Potential Job</label>
                        <div class="col-lg-10">
                            <input class="form-control" name="job" value="{{old('job')}}" type="text" >
                            @include('partials.error_block',['item'=> 'job'])
                            <span class="help-block">If more than one potential job, put coma(Example: Teacher, Tutor).</span>
                        </div>
                    </div>

                    {{--Potential Salary--}}
                    <div class="form-group{{$errors->has('salary') ? 'has-error' : ''}}">
                        <label for="salary" class="col-lg-2 control-label">Potential Salary</label>
                        <div class="col-lg-10">
                            <input class="form-control" name="salary" value="{{old('salary')}}" type="text" >
                            @include('partials.error_block',['item'=> 'salary'])
                            <span class="help-block">Example: RM1200-RM1400.</span>
                        </div>
                    </div>

                    {{--Course Learning--}}
                    <div class="form-group{{$errors->has('syllabus') ? 'has-error' : ''}}">
                        <label for="syllabus" class="col-lg-2 control-label">Learning Syllabus</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" rows="3" name="syllabus"></textarea>
                            @include('partials.error_block',['item'=> 'syllabus'])
                            <span class="help-block">What will the participant learn.</span>
                        </div>
                    </div>

                    {{--Contact Person--}}
                    <div class="form-group{{$errors->has('contact') ? 'has-error' : ''}}">
                        <label for="contact" class="col-lg-2 control-label">Contact Person</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" rows="3" name="contact"></textarea>
                            @include('partials.error_block',['item'=> 'contact'])
                            <span class="help-block">Contact person for the course.</span>
                        </div>
                    </div>

                    {{--Course Duration--}}
                    <div class="form-group{{$errors->has('duration') ? 'has-error' : ''}}">
                        <label for="duration" class="col-lg-2 control-label">Course Duration<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <input class="form-control" name="duration" value="{{old('duration')}}" type="text" >
                            @include('partials.error_block',['item'=> 'duration'])
                            <span class="help-block">Example: 6 jam.</span>
                        </div>
                    </div>

                    {{--Audience 1--}}
                    <div class="form-group{{$errors->has('audience1') ? 'has-error' : ''}}">
                        <label for="audience1" class="col-lg-2 control-label">Targeted Client 1<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="audience1">
                                <option value="" selected>Select client...</option>
                                @foreach ($audiences as $audience)
                                    <option @if (old('audience1')== $audience->id) selected @endif value="{{$audience->id}}">{{$audience->value}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'audience1'])
                        </div>
                    </div>

                    {{--Audience 2--}}
                    <div class="form-group{{$errors->has('audience2') ? 'has-error' : ''}}">
                        <label for="audience2" class="col-lg-2 control-label">Targeted Client 2</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="audience2">
                                <option value="" selected>Select client...</option>
                                @foreach ($audiences as $audience)
                                    <option @if (old('audience2')== $audience->id) selected @endif value="{{$audience->id}}">{{$audience->value}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'audience2'])
                            <span class="help-block">Leave it as it is if no additional client.</span>
                        </div>
                    </div>

                    {{--Audience 3--}}
                    <div class="form-group{{$errors->has('audience3') ? 'has-error' : ''}}">
                        <label for="audience3" class="col-lg-2 control-label">Targeted Client 3</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="audience3">
                                <option value="" selected>Select client...</option>
                                @foreach ($audiences as $audience)
                                    <option @if (old('audience3')== $audience->id) selected @endif value="{{$audience->id}}">{{$audience->value}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'audience3'])
                            <span class="help-block">Leave it as it is if no additional client.</span>
                        </div>
                    </div>

                    {{--Audience 4--}}
                    <div class="form-group{{$errors->has('audience4') ? 'has-error' : ''}}">
                        <label for="audience4" class="col-lg-2 control-label">Targeted Client 4</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="audience4">
                                <option value="" selected>Select client...</option>
                                @foreach ($audiences as $audience)
                                    <option @if (old('audience4')== $audience->id) selected @endif value="{{$audience->id}}">{{$audience->value}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'audience4'])
                            <span class="help-block">Leave it as it is if no additional client.</span>
                        </div>
                    </div>

                    {{--Status--}}
                    <div class="form-group{{$errors->has('status') ? 'has-error' : ''}}">
                        <label for="status" class="col-lg-2 control-label">Status<span class="text-danger"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" name="status">
                                <option value="" selected>Select one...</option>
                                @foreach ($statuses as $status)
                                    <option @if (old('status')== $status->id) selected @endif value="{{$status->id}}">{{$status->value}}</option>
                                @endforeach
                            </select>
                            @include('partials.error_block',['item'=> 'status'])
                        </div>
                    </div>

                    {{--Imej--}}
                    <div class="form-group{{$errors->has('image') ? 'has-error' : ''}}">
                        <label for="image" class="col-lg-2 control-label">Image</label>
                        <div class="col-lg-10">
                            <input class="form-control" name="image" value="{{old('image')}}" type="file" >
                            @include('partials.error_block',['item'=> 'image'])
                        </div>
                    </div>

                    {{--Submit Button--}}
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="reset" class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
    </div>
</div>

@endsection

@section('footer_script')
    <script>
        function loadDept(ministryId)
        {
            $('#deptSelectOption').load('/admin/course/ajax/get-dept/'+ministryId);
        }

        function loadAgency(agencyId)
        {
            console.log(agencyId);
            $('#agencySelectOption').load('/admin/course/ajax/get-agency/'+agencyId);
        }

        function loadSubField(subFieldId)
        {
            console.log(subFieldId);
            $('#sub_fieldSelectOption').load('/admin/course/ajax/get-sub-field/'+subFieldId);
        }
    </script>
@endsection