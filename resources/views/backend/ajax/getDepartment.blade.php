@foreach ($departments as $department)
    <option @if (old('department') == $department->id) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>
@endforeach