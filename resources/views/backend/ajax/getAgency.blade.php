@foreach ($agencies as $agency)
    <option @if (old('agency') == $agency->id) selected @endif value="{{ $agency->id }}">{{ $agency->name }}</option>
@endforeach