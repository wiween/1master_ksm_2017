@foreach ($sessions as $session)
    <option @if (old('session') == $session->id) selected
            @endif value="{{ $session->id }}">Session {{ $session->id }} [{{$session->year}}][{{$session->month}}][{{$session->venue}}]</option>
@endforeach