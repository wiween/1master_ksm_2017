@foreach ($courses as $course)
    <option @if (old('course') == $course->id) selected
            @endif value="{{ $course->id }}">{{ $course->name }} [{{\App\SubField::getValue($course->sub_field_id)}}]</option>
@endforeach