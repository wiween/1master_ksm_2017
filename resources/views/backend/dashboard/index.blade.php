@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Dashboard
@endsection

@section('topButton')
    @if (Auth::user()->access_power >= 2000)
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Activity</span>
    </a>
    @endif
    @if (Auth::user()->access_power >= 1000)
    <a href="#" class="btn btn-link btn-float has-text">
        <i class="icon-grid text-primary"></i>
        <span>New Course</span>
    </a>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if (Auth::user()->access_power >= 1000)
            <div class="row">
                    <div class="col-md-3 col-xs-6 thumbnail">
                        <img class="img-responsive img-rounded" src="/images/dashboard/audit-min.jpg">
                        <a href="/admin/course" class="btn btn-info btn-block" style="margin-top: 4px">COURSE</a>
                    </div>

                    <div class="col-md-3 col-xs-6 thumbnail">
                        <img class="img-responsive img-rounded" src="/images/dashboard/audit-min.jpg">
                        <a href="/admin/session" class="btn btn-info btn-block" style="margin-top: 4px">SESSION</a>
                    </div>
                    <div class="col-md-3 col-xs-6 thumbnail">
                        <img class="img-responsive img-rounded" src="/images/dashboard/statistic-min.jpg">
                        <a href="/admin/monthlyreport" class="btn btn-info btn-block" style="margin-top: 4px">MONTHLY REPORT</a>
                    </div>
                    <div class="col-md-3 col-xs-6 thumbnail">
                        <img class="img-responsive img-rounded" src="/images/dashboard/statistic-min.jpg">
                        <a href="/admin/report/chart" class="btn btn-info btn-block" style="margin-top: 4px">REPORTING</a>
                    </div>
            </div>
                @endif
                @if (Auth::user()->access_power >= 2000)
                    <div class="row">
                        <!--admin ministry -->
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/photo-min.jpg">
                            <a href="/admin/gallery" class="btn btn-info btn-block" style="margin-top: 4px">PHOTO GALLERY</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/activity-min.jpg">
                            <a href="/admin/activity" class="btn btn-info btn-block" style="margin-top: 4px">ACTIVITY</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/target-min.jpg">
                            <a href="/admin/target" class="btn btn-info btn-block" style="margin-top: 4px">TARGET</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/target-min.jpg">
                            <a href="/admin/visit" class="btn btn-info btn-block" style="margin-top: 4px">VISIT</a>
                        </div>
                    </div>
                @endif
                @if (Auth::user()->access_power >= 5000)
                    <div class="row">
                        <!--admin -->
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/contactus-min.jpg">
                            <a href="/admin/contact-us" class="btn btn-info btn-block" style="margin-top: 4px">CONTACT US</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/faq-min.jpg">
                            <a href="#" class="btn btn-info btn-block" style="margin-top: 4px">FAQ</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/faq-min.jpg">
                            <a href="/admin/slider" class="btn btn-info btn-block" style="margin-top: 4px">SLIDER</a>
                        </div>
                    </div>
                @endif
                @if (Auth::user()->access_power == 200 || Auth::user()->access_power >= 5000 )
                <!--examiner-->
                    <div class="row">
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/upload-min.jpg">
                            <a href="/admin/psychometric/pdf" class="btn btn-info btn-block" style="margin-top: 4px">UPLOAD PDF</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/upload-min.jpg">
                            <a href="/admin/psychometric/upload-excel" class="btn btn-info btn-block" style="margin-top: 4px">UPLOAD EXCEL DATA</a>
                        </div>

                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/exam-min.jpg">
                            <a href="/admin/psychometric/form" class="btn btn-info btn-block" style="margin-top: 4px">EXAM</a>
                        </div>
                    </div>
                @endif
                @if (Auth::user()->access_power == 10000)
                    <div class="row">
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/usermgmt-min.jpg">
                            <a href="/admin/audit-trail" class="btn btn-info btn-block" style="margin-top: 4px">AUDIT TRAILS</a>
                        </div>

                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/lookups-min.jpg">
                            <a href="/admin/lookup-mgmt" class="btn btn-info btn-block" style="margin-top: 4px">LOOKUPS
                                MANAGEMENT</a>
                        </div>
                        <div class="col-md-3 col-xs-6 thumbnail">
                            <img class="img-responsive img-rounded" src="/images/dashboard/user-min.jpg">
                            <a href="/admin/user" class="btn btn-info btn-block" style="margin-top: 4px">USER MGT</a>
                        </div>
                    </div>

                @endif


            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection
