@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
   Slider
@endsection

@section('topButton')
    <a href="/admin/slider" class="btn btn-link btn-float has-text">
        <i class="icon-list2 text-primary"></i>
        <span>All Sliders</span>
    </a>

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Avatar / Photo --}}
                <div class="form-group{{ $errors->has('sliderphoto') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Photo [ size:  type: jpg, png, gif ]
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="sliderphoto" required>
                        @include('partials.error_block', ['item' => 'sliderphoto'])
                    </div>
                </div>

                {{-- Title --}}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-4 control-label">
                        Title
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="title" type="text" class="form-control" value="{{ old('title') }}" required>
                        @include('partials.error_block', ['item' => 'title'])
                    </div>
                </div>

                {{-- Subtitle --}}
                <div class="form-group{{ $errors->has('subtitle') ? ' has-error' : '' }}">
                    <label for="subtitle" class="col-md-4 control-label">
                        Subtitle
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="subtitle" type="text" class="form-control" value="{{ old('subtitle') }}" required>
                        @include('partials.error_block', ['item' => 'subtitle'])
                    </div>
                </div>

                {{-- Status --}}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Status
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="status" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($statuses as $status)
                                <option @if (old('status') == $status->key) selected @endif value="{{ $status->key }}">{{ $status->value }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'status'])
                    </div>
                </div>

                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection