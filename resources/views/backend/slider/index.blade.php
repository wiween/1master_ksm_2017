@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    All Sliders
@endsection

@section('topButton')
    <a href="/admin/slider/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New slider</span>
    </a>

@endsection


@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Title</th>
                            <th>Subtitle</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($sliders as $slider)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td class="col-md-1">
                                   <a href="/admin/slider/show/{{ $slider->id }}">
                                       <img class="img-responsive img-thumbnail" src="{{ $slider->sliderphoto }}">
                                   </a>
                                </td>
                                <td>{{ $slider->title }}</td>
                                <td>{{ $slider->subtitle }}</td>
                                <td>
                                    @if ($slider->status == 'active')
                                        <span class="label label-success">{{ $slider->status }}</span>
                                    @elseif($slider->status == 'banned')
                                        <span class="label label-warning">{{ $slider->status }}</span>
                                    @else
                                        <span class="label label-default">{{ $slider->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/slider/show/{{ $slider->id }}"><i class="icon-display"></i>Show</a></li>
                                                <li><a href="/admin/slider/edit/{{ $slider->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/slider/destroy/{{ $slider->id }}"><i class="icon-trash"></i> Delete</a></li>
                                                <li><a href="slider/set/active/{{ $slider->id }}"><i class="icon-flag3 text-success"></i> Set to Active</a></li>
                                                <li><a href="slider/set/inactive/{{ $slider->id }}"><i class="icon-flag7 text-warning"></i> Set to Inactive</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

