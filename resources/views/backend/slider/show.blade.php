@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Show Slider
@endsection

@section('topButton')
    <a href="/admin/slider/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New slider</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Avatar / Photo --}}
                <div class="form-group{{ $errors->has('sliderphoto') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Photo
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <img class="img-responsive img-thumbnail" src="{{ $slider->sliderphoto }}">
                    </div>
                </div>
                {{--Title--}}
                <div class="form-group{{$errors->has('title') ? 'has-error' : ''}}">
                    <label for="title" class="col-lg-4 control-label">
                        Title<span class="text-danger"> *</span></label>
                    <div class="col-lg-6">
                        {{ $slider->title }}
                    </div>
                </div>
                {{--Subtitle--}}
                <div class="form-group{{$errors->has('subtitle') ? 'has-error' : ''}}">
                    <label for="subtitle" class="col-lg-4 control-label">
                        Subtitle<span class="text-danger"> *</span></label>
                    <div class="col-lg-6">
                        {{ $slider->subtitle }}
                    </div>
                </div>

                {{-- Status --}}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Status
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        {{ $slider->status }}
                    </div>
                </div>


                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <a href="/admin/slider/edit/{{ $slider->id }}" class="btn btn-primary btn-block">
                            Edit Slider Record</a>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection