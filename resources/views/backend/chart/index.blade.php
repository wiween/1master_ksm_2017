@extends('layouts.backend')

@section('header_script')
    {!! Charts::styles() !!}
@endsection

@section('mainTitle')
    Chart of All Users
@endsection

@section('topButton')
    <a href="/admin/user" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Users</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Total by Participant Types
                            <div class="pull-right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-book fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $totalStudents }}</div>
                                    <div>Student</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-male fa-5x"></i>
                                </div>

                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $totalSchoolLeavers }}</div>
                                    <div>School Leavers</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-graduation-cap fa-5x"></i>
                                </div>

                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $totalGraduates }}</div>
                                    <div>Graduate</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>

                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $totalExistingEmployees }}</div>
                                    <div>Existing Employee</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->

            <div class="app text-center">
                {!! $chartSession->html() !!}
                {{--{!! $chart->html() !!}--}}
                {{--{!! $chart2->html() !!}--}}
                {{--{!! $pieChartField->html() !!}--}}
            </div>
            <div class="col-lg-6 col-md-6">
                {!! $pieChartAgeMan->html() !!}
                {!! $pieChartAgeWoman->html() !!}
            </div>

            <!-- End Of Main Application -->

            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Total by Company Involved
                        <div class="pull-right">

                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-bank fa-5x"></i>
                            </div>

                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $totalSME }}</div>
                                <div>SME</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                <!-- /.panel-heading -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-building fa-5x"></i>
                            </div>

                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $totalBigCompany }}</div>
                                <div>Big Company</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i> Table : Session [Ethnic]
                        </div>
                </div>
                <div>
                <table class="table table-bordered table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Malay</th>
                        <th>Chinese</th>
                        <th>Indian</th>
                        <th>Dayak</th>
                        <th>Others</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <td>{{ $totalMalay }}</td>
                        <td>{{ $totalChinese }}</td>
                        <td>{{ $totalIndian }}</td>
                        <td>{{ $totalDayak }}</td>
                        <td>{{ $totalOthers }}</td>
                        <td>{{ $totalEthnic }}</td>
                    </tr>

                    </tr>
                </table>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('footer_script')
    {!! Charts::scripts() !!}
    {!! $chartSession->script() !!}
    {{--{!! $chart->script() !!}--}}
    {{--{!! $chart2->script() !!}--}}
    {{--{!! $pieChartField->script() !!}-->--}}
    {!! $pieChartAgeMan->script() !!}
    {!! $pieChartAgeWoman->script() !!}
@endsection
