@extends('layouts.backend')

@section('header_script')
    {!! Charts::styles() !!}
@endsection

@section('mainTitle')
    Chart of All Users
@endsection

@section('topButton')
    <a href="/admin/user" class="btn btn-link btn-float has-text">
        <i class="icon-list-numbered text-primary"></i>
        <span>All Users</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="app text-center">
                {!! $chartUsers->html() !!}
                {!! $chart->html() !!}
            </div>
            <!-- End Of Main Application -->

        </div>
    </div>
@endsection

@section('footer_script')
    {!! Charts::scripts() !!}
    {!! $chartUsers->script() !!}
    {!! $chart->script() !!}
@endsection











