@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Detail of Message
@endsection

@section('topButton')
    @if (Auth::user()->role == 'super_admin')
        <a href="/admin/faq" class="btn btn-link btn-float has-text">
            <i class="icon-list-numbered text-primary"></i>
            <span>All Feedback</span>
        </a>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-3">
                {{-- PHOTO HERE--}}
            </div>
            <div class="col-md-6">
                {{-- TABLE HERE --}}
                <table class="table table-striped table-bordered">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <td>{{$faqses->first_name }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-3">Email</th>
                        <td><a href="mailto:{{ $faqses->email }}">{{$faqses->email }}</a></td>
                    </tr>
                    <tr>
                        <th class="col-md-3">Message</th>
                        <td>{{ $faqses->message }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">
                            @if ($faqses->status == 'open')
                                <span class="label label-success">{{ $faqses->status }}</span>
                            @elseif($faqses->status == 'close')
                                <span class="label label-warning">{{ $faqses->status }}</span>
                            @else
                                <span class="label label-default">{{ $faqses->status }}</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>
                            {{ $faqses->created_at->format('d M, Y') }}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>
                            {{ $faqses->updated_at->format('d M, Y') }}
                        </td>
                    </tr>
                </table>
                <br>
                <a href="/admin/faq/edit/{{ $faqses->id }}" class="btn btn-primary btn-block">Edit Message</a>

            </div>
        </div>
    </div>


@endsection

@section('footer_script')
@endsection