@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')

    Update Message/Question

@endsection

@section('topButton')

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="first_name" class="col-md-4 control-label">
                        Name :
                    </label>
                    <div class="col-md-6">
                        {{  $faqses->first_name }}
                    </div>
                </div>

                {{-- Email --}}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">
                        E-Mail Address :
                    </label>
                    <div class="col-md-6">
                        {{  $faqses->email }}
                    </div>
                </div>

                {{-- Message --}}
                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Message/Question :</label>
                    <div class="col-md-6">
                        {{ $faqses->message }}
                    </div>
                </div>

                {{-- Answer --}}
                <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Answer :
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <textarea name="answer" class="form-control" rows="3">{{ old('answer', $faqses->answer) }}</textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'answer'])
                </div>

                {{-- Status --}}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Set Status :
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="status" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($statuses as $status)
                                <option @if (old('status', $status->status) == $status->key) selected @endif value="{{ $status->key }}">
                                    {{ $status->value }}
                                </option>
                            @endforeach
                        </select>

                        @include('partials.error_block', ['item' => 'status'])
                    </div>
                </div>

                {{-- Category --}}
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">
                        Set Category :
                        <span class="text-danger"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select name="category" class="form-control">
                            <option selected>Select one..</option>
                            @foreach ($categories as $category)
                                <option @if (old('category', $category->name) == $category->name) selected @endif value="{{ $category->id }}">
                                    {{ $category->name }}
                                </option>
                            @endforeach
                        </select>

                        @include('partials.error_block', ['item' => 'category'])
                    </div>
                </div>



                {{-- Submit Button --}}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update & Reply
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection
