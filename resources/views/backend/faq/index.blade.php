@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    FAQ Feedback
@endsection

@section('topButton')

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($faqses as $faq)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td><a href="/admin/faq/show/{{ $faq->id }}">{{ $faq->first_name }}</a> </td>
                                <td><a href="mailto:{{ $faq->email }}">{{ $faq->email }}</a></td>
                                <td>{{ $faq->message }}</td>
                                <td>
                                    {{ $faq->created_at }}
                                </td>
                                <td>
                                    @if ($faq->status == 'publish')
                                        <span class="label label-success">{{ $faq->status }}</span>
                                    @elseif($faq->status == 'unpublish')
                                        <span class="label label-warning">{{ $faq->status }}</span>
                                    @else
                                        <span class="label label-default">{{ $faq->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="/admin/faq/edit/{{ $faq->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a href="/admin/faq/set/publish/{{ $faq->id }}"><i class="icon-display"></i> Publish</a></li>
                                                <li><a href="/admin/faq/set/unpublish/{{ $faq->id }}"><i class="icon-blocked"></i> Unpublish</a></li>
                                                <li><a href="/admin/faq/destroy/{{ $faq->id }}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('footer_script')
@endsection